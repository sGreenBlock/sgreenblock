# sGreenBlock the blockchain game

Have you ever thought of playing around with the concept of blockchain and
cryptocurrency in a straightforward way? sGreenBlock is a Blockchain Wallet
based on a real blockchain developed for a special purpose: Learning and Fun.
Currently it is at a Beta state. Master branch is at a stable state, but
currently in active development and testing phase. 

sGreenBlock implements a full **Blockchain** with a **Crypto Currency** and
**Smart Contracts**. **Asymmetric encryption** is used like in real world
applications to secure the contracts like transactions. With the implemented
Peer-to-Peer functionality, the Blockchain can be shared as a **Distributed
Ledger**. Just like real world Bitcoin or Ethereum. The money is generated like
in real world BitCoin Currency by **proof of work** with **mining** for
Hashcodes.

To use this tool also in educational programming, the backend is implemented in
plain Java without any additional libraries. The frontend is implemented on the well
known learning platform Greenfoot supported by the King's College London and ORACLE.

![Preview](images/20190517-sgreenblock-demo-01.gif)

## Installation

1. [Download Greenfoot](https://www.greenfoot.org/download) and install it
2. Download the latest tar archive file from the [releases page](https://gitlab.com/sGreenBlock/sgreenblock/releases)
3. Extract the archive with a tool like [7-zip](https://www.7-zip.org/)
4. Go into the extracted archive, open folder `sGreenBlock` and execute the file
   project.greenfoot by double klicking it.
5. If you have a **firewall** running, it will ask you, if it is ok that Java
   communicates through it. This is needed for communication with other Wallets, if
   you want Peer-to-Peer communication between Wallets.

### Peer-to-Peer Communication

Peer-to-Peer Communication is an essential part of a Blockchain network. It is
the only way to keep everything decentralized.

To do this, sGreenBlock must be allowed to use an incoming Port (default
43210). Your Firewall must be configured to allow this, if you want to use
Peer-to-Peer communication.

If you also want to allow connections from the internet, you will have to
configure port forwarding in your router. Forward Port 43210 to the IP of your
computer running sGreenBlock

### Syncing the BlockChain

Because sometimes it is not possible to configure the Firewall for Peer-to-Peer
(e.g. public facilities), sGreenBlock can also be used with a file sync provider like
Google Drive. This way you need no open port, but keep in mind that for real
world this would mean that the blockchain gets centralized.

Refer to "[Installation and
Setup](https://gitlab.com/sGreenBlock/sgreenblock-light/blob/master/README.md#installation-and-setup)"
of sGreenBlock Light to set up syncing. Setup is very similar. But keep care that contents of
`keys` folder and `local_data` folder **are not synced**.

## Features

* Smart Contracts
* Multisig Contracts
* Transactions with the possibility for Transaction fees
* Aggressive UTXO (Unspent Transactions) management with persisting by frontend
* Mining for Hashcodes with automatic adaption of difficulty level to average mining time
* Asymmetric Encryption
* Graphical inspection of blocks, contracts, transactions and UTXO list
* Peer-to-Peer connection of nodes and alternative syncing of folder (if you have firewall issues) 

## Syncing features

sGreenBlock can do real Peer-to-Peer communication. You have to specify one ore
more starting addresses in the file `sGreenBlock/local_data/ips.snodes`
(`IP:Port` line by line). After successful connection, the Wallet requests all
addresses the other node knows. It also collects any address which connects to
it. To keep network communication efficient, the Wallet rates the connection
quality and prefers nodes with high quality. To allow Peer-to-Peer sharing, you
will have to allow the application to communicate through your firewall. 

Peer-to-Peer communication can be prevented by institute firewalls blocking any
communication betwenn peers. To make it possible to use this game also in this
circumstances, **Peer2Peer sharing** is simulated by sharing the blockchain and
contracts over a sync provider like Google Backup & Sync or Dropbox. This way
behavior is much the same and you have no problems if no one is online to get
the newest chain. Keep in mind that this way the anonymity suffers, because the
share provider can link your work in the blockchain to your account. Also other
attendees can make conclusions in a limited way. 

## Smart Contracts

To implement some gaming experience, it will be possible to use hard coded Smart
Contracts on the BlockChain Lengths. This is also an example for one of the main
use cases of Smart Contract, called **Prediction Markets**.

To use them, others confirm this contracts and hold the bet against the guess made
in them. The winner will earn the money stored in the contract.

Java inheritance is used to make it possible to implement new Contracts.

## License

    sGreenBlock the blockchain game
    Copyright (C) 2019  Stefan Stolz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
