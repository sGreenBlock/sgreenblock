import java.util.logging.Logger;

import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)


public class ActorAccountBalanceMonitor extends Actor
{
	private Transaction transaction;
	private String transactionId;
	private WalletUI walletUI;
	
	private static final Logger LOGGER = Logger.getLogger(Wallet.class.getName());
    
    public ActorAccountBalanceMonitor(String strToDraw, Transaction transaction, WalletUI walletUI) {
    	this.transaction = transaction;
    	this.transactionId = transaction.getHash();
    	this.walletUI = walletUI;
		drawAccountInfo(strToDraw);
	}
    
    public ActorAccountBalanceMonitor(String strToDraw, WalletUI walletUI) {
    	this.walletUI = walletUI;
		drawAccountInfo(strToDraw);
	}
    
    public ActorAccountBalanceMonitor(String strToDraw, String transactionId, WalletUI walletUI) {
    	
    	this.transactionId = transactionId;	
    	this.walletUI = walletUI;
		drawAccountInfo(strToDraw);
    }
    

	public void act() 
    {
		if(Greenfoot.mouseClicked(this)) {
			LOGGER.warning(String.format("Show Contract requested!"));
			if(this.transaction == null && this.transactionId != null) {
				LOGGER.warning(String.format("Searching by id 0x%s!",transactionId));
				Contract contract = walletUI.getWallet().getContractById(transactionId);
				if(contract instanceof Transaction) {
		    		Transaction transaction = (Transaction) contract;
		    		this.transaction = transaction;	    		
		    	}   
			}
			
			if(this.transaction != null) {
				WorldContractMonitor contractMonitor = new WorldContractMonitor(getWorld(),walletUI,transaction);
	        	Greenfoot.setWorld(contractMonitor);
			}
			else {
				LOGGER.warning(String.format("Can not show contract 0x%.6s!",transactionId));
			}
        }
    }    
    
    public void drawAccountInfo(String strToDraw){
        GreenfootImage img = new GreenfootImage(700, 20);
        img.setColor(new Color(0,77,64));
        img.fill();
        img.setColor(new Color(255,255,255));
        img.setFont(new Font("Monospaced", false, false , 12));
        img.drawString(strToDraw, 10, -3);
        setImage(img);
    }
    
    public Transaction getTransaction() {
    	return this.transaction;
    }
}
