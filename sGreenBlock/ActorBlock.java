import greenfoot.*;  
import java.util.Date;
import java.text.SimpleDateFormat;
import java.lang.Math;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;

public class ActorBlock extends Actor
{
	
	Block block;
	WalletUI walletUI;
	private static final Logger LOGGER = Logger.getLogger(Transaction.class.getName());	
    
    public ActorBlock(Block block, WalletUI walletUI) {
    	this.block = block;
    	this.walletUI = walletUI;
        drawBlockImage();
    }
    
    public void act()
    {
    	if(Greenfoot.mouseClicked(this)) {
        	WorldBlockMonitor blockMonitor = new WorldBlockMonitor(getWorld(),walletUI,block.getMerkleRoot());
        	Greenfoot.setWorld(blockMonitor);
        }
    }

    public void drawBlockImage(){        
        GreenfootImage thisImage = new GreenfootImage("block_60.png");
        //thisImage.setColor(new Color(0,0,0));
        //thisImage.setFont(new Font("Monospaced", false, false , 11));
        thisImage.drawString(String.format("Merkle 0x%.6s", block.getMerkleRoot()),3,15);
        thisImage.drawString(String.format("mined %s",new SimpleDateFormat("HH:mm:ss").format(new Date(block.getMinedAtTimeStamp()))),3,25);
        long hr = (block.getMiningTime()/1000)/3600;
        long rem = (block.getMiningTime()/1000)%3600;
        long mn = rem/60;
        long sec = rem%60;
        String timeString = String.format("%3d:%02d:%02d",hr,mn,sec);
        String infoString = String.format("difficulty %3d",block.getDifficulty());        
        thisImage.drawString(infoString,3,35);
        infoString = String.format("dur. %s",timeString);        
        thisImage.drawString(infoString,3,45);
        //thisImage.drawString("Block Hash:",2,25);
        //thisImage.drawString(String.format("%.8s",this.hash),2,35);
        //GreenfootImage wonImage = new GreenfootImage("Coin_30_trans.png");
        //thisImage.drawImage(wonImage,thisImage.getWidth()-35,thisImage.getHeight()-20);
        this.setImage(thisImage);
    }
    
}
