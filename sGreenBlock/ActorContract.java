import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.logging.Logger;
import java.text.SimpleDateFormat;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.security.PublicKey;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.ObjectInputStream;
import java.io.File;
import java.io.FileInputStream;

public class ActorContract extends Actor
{
    Contract contract;
    WalletUI walletUI;
    
    private static final Logger LOGGER = Logger.getLogger(Transaction.class.getName());
    
    public ActorContract(Contract contract, WalletUI walletUI){
        this.contract = contract;
        this.walletUI = walletUI;
    }
    
    public void act()
    {
    	if(Greenfoot.mouseClicked(this)) {
        	WorldContractMonitor contractMonitor = new WorldContractMonitor(getWorld(),walletUI,contract);
        	Greenfoot.setWorld(contractMonitor);
        }
    }

	public void drawContractImage(boolean accepted, boolean won){
		drawContractImageContract(accepted, won);
    }
	
	public void drawContractImageContract(boolean accepted, boolean won){
        String imageFileName = "contract_not_accepted.png";
        if(accepted)
            imageFileName = "contract.png";
        GreenfootImage thisImage = new GreenfootImage(imageFileName);
        //thisImage.setColor(new Color(0,0,0));
        //thisImage.setFont(new Font("Monospaced", false, false , 11));
        //thisImage.drawString(this.hash,4,15);        
        //GreenfootImage wonImage = new GreenfootImage("Coin_30.png");
        //if(won)
        //    thisImage.drawImage(wonImage,7,38);
        this.setImage(thisImage);
    }

	public Contract getContract() {
		return contract;
	}
	
}
