import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

import java.security.PublicKey;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;

public class ActorGuessBlockSize extends ActorSmartContract
{
    private GuessBlockSize guessBlockSize;
    WalletUI walletUI;
    private static final Logger LOGGER = Logger.getLogger(Transaction.class.getName());
    
    public ActorGuessBlockSize(GuessBlockSize guessBlockSize, WalletUI walletUI){
        super(guessBlockSize, walletUI);
        this.guessBlockSize = guessBlockSize;
        this.walletUI = walletUI;
        
    }   
    
    @Override    
    public void drawContractImage(boolean accepted, boolean won){
    	super.drawContractImage(accepted, won);        
        GreenfootImage thisImage = getImage();
        //thisImage.setColor(new Color(0,0,0));
        //thisImage.setFont(new Font("Monospaced", false, false , 11));
        thisImage.drawString(String.format("i:%.6s", guessBlockSize.getHash()),4,15);
        thisImage.drawString("T:"+new SimpleDateFormat("HH:mm:ss").format(new Date(guessBlockSize.getTimeStamp()+guessBlockSize.getEstimatedSeconds() * 1000)),4,25);
        String from = "null";
        if(guessBlockSize.getSenderPubKey() != null) {
        	from = Hash_it.getHash(guessBlockSize.getSenderPubKey());
        }
        if(guessBlockSize.getFromTransactionHash() != null) {
        	from = Hash_it.getHash(guessBlockSize.getFromTransactionHash());
        }
        thisImage.drawString(String.format("F:%."+Wallet.USER_HASH_LENGTH+"s", from),4,35);
        thisImage.drawString(""+guessBlockSize.getEstimatedBlocks()+" Blocks",4,45);
        this.setImage(thisImage);
    }

	public GuessBlockSize getGuessBlockSize() {
		return guessBlockSize;
	}    
	
}
