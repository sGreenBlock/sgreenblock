import java.util.Date;

import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)


public class ActorHashesN extends Actor
{
	private int time;
	public static final int UPDATE_TIME = 10;
	private boolean reset = false;
	
	public ActorHashesN() {
		drawAccountInfo(false);
	}

	public void act() 
    {
		time = (++time) % UPDATE_TIME;
		
    	if(MiningProgress.isMining() && time == 0) {
    		drawAccountInfo(false);
    		reset = true;
    	}
    	if(!MiningProgress.isMining() && reset) {
    		drawAccountInfo(true);
    		reset = false;
    	}
		
    }    
    
    public void drawAccountInfo(boolean reset){
    	long currentTime = new Date().getTime();
    	long hashesPerSecond = 0;
    	if(MiningProgress.getStartTime() != null && MiningProgress.getHashesCounter() > 0) {
    		long secondsLast = (currentTime - MiningProgress.getStartTime()) / 1000;
    		if(secondsLast > 0){
    		    hashesPerSecond = (MiningProgress.getHashesCounter()/(secondsLast));
    		}    		
    	}
    	String strToDraw = "";
    	
    	if(MiningProgress.getContractsToMine()==0 && reset) {
    		strToDraw = String.format("no Contracts%nfound",MiningProgress.getHashesCounter(),MiningProgress.getCurrentHash(),hashesPerSecond);
    	}
    	else if(MiningProgress.getContractsToMine()==0) {
    		strToDraw = String.format("searching%nContracts ..",MiningProgress.getHashesCounter(),MiningProgress.getCurrentHash(),hashesPerSecond);
    	}
    	else {
    		strToDraw = String.format("# %,10d%n0x%.10s%n%,8d H/s",MiningProgress.getHashesCounter(),MiningProgress.getCurrentHash(),hashesPerSecond);
    	}
    	
    	
        GreenfootImage img = new GreenfootImage(100, 60);
        img.setColor(new Color(0,77,64));
        img.fill();
        img.setColor(new Color(255,255,255));
        img.setFont(new Font("Monospaced", false, false , 12));
        img.drawString(strToDraw, 10, 15);
        setImage(img);
    }
}
