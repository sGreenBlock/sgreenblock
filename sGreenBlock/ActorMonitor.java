import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)


public class ActorMonitor extends Actor
{
	
	
    
    public ActorMonitor(String strToDraw) {
		drawAccountInfo(strToDraw);
	}

	public void act() 
    {
        
    }    
    
    public void drawAccountInfo(String strToDraw){
        GreenfootImage img = new GreenfootImage(700, 500);
        img.setColor(new Color(0,77,64));
        img.fill();
        img.setColor(new Color(255,255,255));
        img.setFont(new Font("Monospaced", false, false , 12));
        img.drawString(strToDraw, 10, 20);
        setImage(img);
    }
}
