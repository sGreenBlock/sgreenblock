import java.net.ServerSocket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class ActorMonitorNodes extends Actor
{
	public static final int UPDATE_BLOCKCHAIN_INTERVALL = 300;
	
	WalletUI walletUI;
	
	private int time;
	
	private Integer availableNodes = null;
	
    public ActorMonitorNodes(WalletUI walletUI) {
    	this.walletUI = walletUI;
    	drawWalletImage();
	}
    
    public void act() 
    {
    	time = (++time) % UPDATE_BLOCKCHAIN_INTERVALL;
    	if(time == 0) {
    		drawWalletImage();
    	}    	
    	if(Greenfoot.mouseClicked(this)){
    		WorldNodesMonitor contractMonitor = new WorldNodesMonitor(getWorld(),walletUI);
        	Greenfoot.setWorld(contractMonitor);
    	}
    }
    
    private void drawWalletImage(){   
    	Integer availableNodes = Wallet.calcAvailableNodes();
    	ServerSocket serverSocket = P2PSingleton.getServerSocketInstance(walletUI.getWallet());
    	int status = 0;
    	if(serverSocket != null && serverSocket.isBound()) {
    		status++;
    	}
    	if(availableNodes != this.availableNodes) {
    		if(availableNodes > 0) {
    			status++;
    		}
    		GreenfootImage thisImage;
        	if(status > 1) {
        		thisImage = new GreenfootImage("info_nodes_green.png");   
        	}
        	else if(status > 0) {
        		thisImage = new GreenfootImage("info_nodes_orange.png");   
        	}
        	else {
        		thisImage = new GreenfootImage("info_nodes_red.png");
        	}
            
            thisImage.setColor(new Color(255,255,255));
            thisImage.setFont(new Font("Monospaced", false, false , 11));
            thisImage.drawString(String.format("%3d",availableNodes),3,17);   
            this.setImage(thisImage);
    	}    	
    }    
}
