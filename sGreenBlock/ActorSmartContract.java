import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

import java.security.PublicKey;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;

public class ActorSmartContract extends ActorContract
{
    private GuessBlockSize guessBlockSize;
    WalletUI walletUI;
    private static final Logger LOGGER = Logger.getLogger(Transaction.class.getName());
    
    public ActorSmartContract(GuessBlockSize guessBlockSize, WalletUI walletUI){
        super(guessBlockSize, walletUI);
        this.guessBlockSize = guessBlockSize;
        this.walletUI = walletUI;
        
    }   
    
    @Override    
    public void drawContractImage(boolean accepted, boolean won){
    	super.drawContractImageContract(accepted, won);
    	GreenfootImage thisImage = getImage();
    	//thisImage.setColor(new Color(0,0,0));
        //thisImage.setFont(new Font("Monospaced", false, false , 11));
    	String strToPrint = String.format("MSig: %d/%d", guessBlockSize.getNumberApproverSigns(), guessBlockSize.getNumberApproversNeeded());
    	thisImage.drawString(strToPrint,4,65);      
        this.setImage(thisImage);
    }

	public GuessBlockSize getGuessBlockSize() {
		return guessBlockSize;
	}    
	
}
