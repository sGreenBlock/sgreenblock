import greenfoot.*;  
import java.security.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;


public class ActorTransaction extends ActorContract
{  
    
	private Transaction transaction;
	private WalletUI walletUI;
	private static final Logger LOGGER = Logger.getLogger(ActorTransaction.class.getName());
    
    public ActorTransaction(Transaction transaction, WalletUI walletUI){
        super(transaction, walletUI);
        this.transaction = transaction;
        this.walletUI = walletUI;
    }
    
    @Override    
    public void drawContractImage(boolean accepted, boolean won){
        super.drawContractImage(accepted,won);
        
        GreenfootImage thisImage = getImage();   
        GreenfootImage wonImage = new GreenfootImage("Coin_30_trans.png");
        thisImage.drawImage(wonImage,7,38);
        //thisImage.setColor(new Color(0,0,0));
        //thisImage.setFont(new Font("Monospaced", false, false , 11));
        thisImage.drawString(String.format("i:%.6s", transaction.getHash()),4,15);
        thisImage.drawString("T:"+new SimpleDateFormat("HH:mm:ss").format(new Date(transaction.getTimeStamp())),4,25);
        String from = "null";
        if(this.getTransaction().getSenderPubKey() != null) {
        	from = Hash_it.getHash(this.getTransaction().getSenderPubKey());
        }
        if(this.getTransaction().getFromTransactionHash() != null) {
        	from = Hash_it.getHash(this.getTransaction().getFromTransactionHash());
        }
        thisImage.drawString(String.format("F:%."+Wallet.USER_HASH_LENGTH+"s", from),4,35);
        String to = "null";
        if(transaction.getRecipientPubKey() != null) {
        	to = Hash_it.getHash(transaction.getRecipientPubKey());
        }
        if(transaction.getToTransactionHash() != null) {
        	to = transaction.getToTransactionHash();
        }
        thisImage.drawString(String.format("-> %."+Wallet.USER_HASH_LENGTH+"s", to),4,45);
        thisImage.drawString(String.format("V:%s", transaction.getValue()),4,55);
        
        
        this.setImage(thisImage);        
    }

	public Transaction getTransaction() {
		return transaction;
	}
    
}
