import java.security.PublicKey;
import java.util.ArrayList;

import greenfoot.*;  


public class ActorTransactionMultiSig extends ActorTransaction
{
	private TransactionMultiSig transactionMultiSig;
	WalletUI walletUI;
	
    public ActorTransactionMultiSig(TransactionMultiSig transactionMultiSig, WalletUI walletUI) {
		super(transactionMultiSig, walletUI);
		this.walletUI = walletUI;
		this.transactionMultiSig = transactionMultiSig;
	}

    @Override    
    public void drawContractImage(boolean accepted, boolean won){
    	super.drawContractImage(accepted, won);
    	drawContractImageMutliSig();
    }
    
    public void drawContractImageMutliSig() {
    	GreenfootImage thisImage = getImage();   
    	//thisImage.setColor(new Color(0,0,0));
        //thisImage.setFont(new Font("Monospaced", false, false , 11));
    	String strToPrint = String.format("MSig: %d/%d", transactionMultiSig.getNumberApproverSigns(), transactionMultiSig.getNumberApproversNeeded());
    	thisImage.drawString(strToPrint,4,65);
    }

	public TransactionMultiSig getTransactionMultiSig() {
		return transactionMultiSig;
	}
    
}
