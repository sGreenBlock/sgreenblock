import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.io.*;
import java.security.*;
import java.security.spec.X509EncodedKeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.util.Base64;
import java.util.logging.Logger;

public class AsymKrypt
{
    // instance variables - replace the example below with your own
    private int x;
    PrivateKey priv;
    PublicKey pub;
    private static final String KEY_FOLDER_NAME = "keys";
    public static final String KEY_NAME = "userkey";
    private static final String KEY_PUB_SUFF = ".pub";
    private static final String KEY_PRIV_SUFF = ".priv";
    private String keyName;
    
    private static final Logger LOGGER = Logger.getLogger(Wallet.class.getName());
    
    public AsymKrypt(String keyName, boolean generateIfNotExists)
    {
    	
    	this.keyName = (keyName != null)? keyName: KEY_NAME;
        
        if(isKeyExisting(this.keyName)){
        	LOGGER.config(String.format("Keyfile %s is existing, using it",this.keyName));
            this.pub = readPublicKey(this.keyName);
            this.priv = readPrivateKey(this.keyName);
        }
        
        else if(!generateIfNotExists){
        	throw new IllegalArgumentException(String.format("There is no Keyfile '%s'", keyName));
        }
        
        else{     
            System.out.println("Generating Keys...");
            LOGGER.config(String.format("No Key found. Generating Key in File %s",this.keyName));
            generateKeys();
            savePrivateKey(this.keyName);
            savePublicKey(this.keyName);
        }
    }
    
    private static String getKeyNameWithPath(String keyName, boolean isPrivateKey){
    	String returnKeyPath = null;
    	String folderWithSeparator = KEY_FOLDER_NAME + File.separatorChar;
    	returnKeyPath = folderWithSeparator + keyName + ((isPrivateKey)? KEY_PRIV_SUFF: KEY_PUB_SUFF);
    	return returnKeyPath;
    }
    
    public static String getStringFromKey(Key key) {
		return Base64.getEncoder().encodeToString(key.getEncoded());
	}
    
    public static boolean verify(byte[] sigToVerify, byte[] data, PublicKey pub){
        boolean returnBool = false;
        try{
            Signature sig = Signature.getInstance("SHA1withDSA", "SUN");
            sig.initVerify(pub);
            sig.update(data);
            returnBool = sig.verify(sigToVerify);
        } catch (NoSuchProviderException | NoSuchAlgorithmException | InvalidKeyException | SignatureException e) {
            System.err.println("Caught exception " + e.toString());
            e.printStackTrace();
        }
        return returnBool;
    }
    
    public void generateKeys(){
        try {  
            KeyPairGenerator keyGen = KeyPairGenerator.getInstance("DSA", "SUN");
            SecureRandom random = SecureRandom.getInstance("SHA1PRNG", "SUN");
            keyGen.initialize(1024, random);
            
            KeyPair pair = keyGen.generateKeyPair();
            priv = pair.getPrivate();
            pub = pair.getPublic();
        } catch (Exception e) {
            System.err.println("Caught exception " + e.toString());
            e.printStackTrace();
        }
    }

    public byte[] signData(String data)
    {
        byte[] returnStr = null;
        try {              
            Signature dsa = Signature.getInstance("SHA1withDSA", "SUN");
            dsa.initSign(priv);
            byte[] dataByte = data.getBytes();
            dsa.update(dataByte);
            
            byte[] realSig = dsa.sign();
            returnStr =realSig;

        } catch (Exception e) {
            System.err.println("Caught exception " + e.toString());
            e.printStackTrace();
        }
        
        return returnStr;
    }
    
    public static boolean isKeyExisting(String keyName){
        boolean isKeyExisting = false;
        File f = new File(getKeyNameWithPath(keyName, true));
        if(f.exists() && !f.isDirectory()) { 
            isKeyExisting = true;
        }
        return isKeyExisting;
    }
    
    public static byte[] readFromFile(String filename){        
        byte[] encKey = null;
        try {   
            FileInputStream keyfis = new FileInputStream(filename);
            encKey = new byte[keyfis.available()];  
            keyfis.read(encKey);        
            keyfis.close();
        } catch (Exception e) {
            System.out.println(String.format("Can not read file. Is file '%s' existing?", filename));
            e.printStackTrace();
        }
        return encKey;
    }
    
    public static PublicKey readPublicKey(String keyPubName){    
        PublicKey pub = null;
    	byte[] encKey = readFromFile(getKeyNameWithPath(keyPubName, false));        
        try{
            X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(encKey);
            KeyFactory keyFactory = KeyFactory.getInstance("DSA", "SUN");
            pub = keyFactory.generatePublic(pubKeySpec);
        } catch (Exception e) {
            System.out.println(String.format("Can not read key '%s'.", keyPubName));
            e.printStackTrace();
        }
        return pub;        
    }
    
    public byte[] getPublicKeyEncoded(){
        return pub.getEncoded();
    }
    
    public PublicKey getPublicKey(){
        return pub;
    }
    
    public PrivateKey getPrivateKey(){
        return priv;
    }
    
    public static PrivateKey readPrivateKey(String keyPrivName){
        PrivateKey priv = null;
    	byte[] encKey = readFromFile(getKeyNameWithPath(keyPrivName, true));        
        try{
            PKCS8EncodedKeySpec privKeySpec = new PKCS8EncodedKeySpec (encKey);
            KeyFactory keyFactory = KeyFactory.getInstance("DSA");
            priv = keyFactory.generatePrivate(privKeySpec);
        } catch (Exception e) {
            System.err.println("Caught exception when reading private key: " + e.toString());
            e.printStackTrace();
            System.exit(1);
        }
        return priv;
    }
    
    public void savePublicKey(String keyPubName){
        /* save the public key in a file */
        byte[] key = pub.getEncoded();
        FileOutputStream keyfos = null;
        try{
            keyfos = new FileOutputStream(getKeyNameWithPath(keyPubName, false));
            keyfos.write(key);
            keyfos.close();
        }
        catch (FileNotFoundException ex) {
            System.out.println("Can not write to disk: "+ex);
            ex.printStackTrace();
        }
        catch (IOException ex) {
            System.out.println("IO error: "+ex);
        }finally {
            if(keyfos != null){
                //keyfos.close();
            } 
        }
    }
    
    public void savePrivateKey(String keyPrivName){
        /* save the public key in a file */
        byte[] key = priv.getEncoded();
        FileOutputStream keyfos = null;
        try{
            keyfos = new FileOutputStream(getKeyNameWithPath(keyPrivName, true));
            keyfos.write(key);
            keyfos.close();
        }
        catch (FileNotFoundException ex) {
            System.out.println("Can not write to disk: "+ex);
            ex.printStackTrace();
        }
        catch (IOException ex) {
            System.out.println("IO error: "+ex);
            ex.printStackTrace();
        }finally {
            if(keyfos != null){
                //keyfos.close();
            } 
        }
    }

	public String getKeyName() {
		return keyName;
	}    
    
}
