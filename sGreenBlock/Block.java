import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.Date;
import java.text.SimpleDateFormat;
import java.lang.Math;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;

public class Block implements java.io.Serializable
{
    public String blockHash;
    private String merkleRoot;   
	public String previousHash;
    public String contractHash;
    private String transactionData; //our data will be a simple message.
    private long timeStamp; //as number of milliseconds since 1/1/1970.
    private long nonce=Long.MIN_VALUE;
    private long miningTime = 0;
    private long minedAtTimeStamp = 0;
    private int difficulty = 0;
    private LinkedHashMap<String,Contract> contractList;
    
    private static final Logger LOGGER = Logger.getLogger(Block.class.getName());

    //Block Constructor.    
    
    public Block(String transactionData,String previousHash, LinkedHashMap<String,Contract> contractList) {
        
        this.transactionData = transactionData;
        this.previousHash = previousHash;
        this.timeStamp = new Date().getTime();
        this.contractList = contractList;        
        this.contractHash = Hash_it.getHash(toStringContracts());
        this.blockHash = calculateBlockHash();
        this.merkleRoot = calculateMerkleRoot();
    }
    
    public boolean checkContracts(BlockChain blockChain){
    	boolean returnIfValid = true;
    	
    	for(Map.Entry<String, Contract> entry : contractList.entrySet()) {
    		boolean isOk = false;
            String hashContract = entry.getKey();
            Contract contract = entry.getValue();
            // don't run checks if rewardTransaction
            
        	LOGGER.fine(String.format("Checking Contract %s",hashContract));
        	if(contract.checkContract(blockChain)){            		
        		isOk = true;
        	}
            
            if(!isOk){
            	LOGGER.warning(String.format("Error checking Contract %s",hashContract));
            	returnIfValid = false;
            }
        }
    	return returnIfValid;
    }
    
    public boolean checkContracts(BlockChain blockChain,Transaction rewardTransaction){
    	boolean returnIfValid = true;
    	for(Map.Entry<String, Contract> entry : contractList.entrySet()) {
    		boolean isOk = false;
            String hashContract = entry.getKey();
            Contract contract = entry.getValue();
            // don't run checks if rewardTransaction
            if(hashContract.equals(rewardTransaction.getHash())){
            	LOGGER.fine(String.format("Skipping Contract Reward Transaction %s",rewardTransaction.getHash()));
            	isOk = true;
            }
            else{
            	LOGGER.fine(String.format("Checking Contract %s",hashContract));
            	if(contract.checkContract(blockChain)){            		
            		isOk = true;
            	}
            }
            if(!isOk){
            	LOGGER.warning(String.format("Error checking Contract %s",hashContract));
            	returnIfValid = false;
            }
        }
    	return returnIfValid;
    }

    /**
     * Calculates a pseudo Merkle Root by making a Hash over all Contract Hashes.
     * Calculating a real Merkle Root does not bring any benefit but makes code
     * more complicated. If you are interested in programming a real Merkle Tree
     * refer to this ressources:
     * * https://www.youtube.com/watch?v=gUwXCt1qkBU&t=193s
     * * http://www.righto.com/2014/02/bitcoin-mining-hard-way-algorithms.html
     */
    public String calculateMerkleRoot(){
        String stringToHash = getAllContractHashes("");
        return Hash_it.getHash(stringToHash);
    }
    
    /**
     * The Block Hash consists of:
     * 1. The Bitcoin version number. // not implemented here
     * 2. The previous block hash.
     * 3. The Merkle Root of all the transactions selected to be in that block.
     * 4. The timestamp.
     * 5. The difficulty target.
     * 6. The Nonce.
     * https://en.bitcoin.it/wiki/Block_hashing_algorithm
     */
    public String calculateBlockHash(){
        String stringToHash = previousHash + merkleRoot + timeStamp + difficulty + nonce;
        return Hash_it.getHash(stringToHash);
    }
    
    public String toStringContracts(){
        String returnStr = "";
        returnStr += String.format("%n%n## Contracts of Block%n");
        returnStr += getAllContractHashes("%n* ");
        return returnStr;
    }
    
    public String getAllContractHashes(String separator){
        String returnStr = "";
        for(Map.Entry<String, Contract> entry : contractList.entrySet()) {
            String key = entry.getKey();
            returnStr += String.format(separator+"%s",key);
        }
        return returnStr;
    }
    
    public String toString(){        
        String returnStr = "";
        returnStr += String.format("%n# Block 0x%.6s %n",getMerkleRoot());
        returnStr += String.format("%n* %-15s: %s","merkle hash",merkleRoot);
        returnStr += String.format("%n* %-15s: %s","block hash",blockHash);
        returnStr += String.format("%n* %-15s: %s","previous block",previousHash);
        returnStr += String.format("%n* %-15s: %s (%s)","timestamp",new SimpleDateFormat("HH:mm:ss").format(new Date(timeStamp)), timeStamp);
        returnStr += String.format("%n* %-15s: %s (%s)","mined at",new SimpleDateFormat("HH:mm:ss").format(new Date(minedAtTimeStamp)), minedAtTimeStamp);
        returnStr += String.format("%n* %-15s: %s","mining time",miningTime);
        returnStr += String.format("%n* %-15s: %s","difficulty",difficulty);
        returnStr += String.format("%n* %-15s: %s","nonce",nonce); 
        returnStr += toStringContracts();
        return returnStr;
    }

    public void setTransactionData(String transactionData){
        this.transactionData = transactionData;
    }

    public Block mineBlock(BlockChain blockchain,int difficulty, Transaction rewardTransaction) {     
    	
    	LOGGER.config(String.format("Start Mining"));
    	MiningProgress.setMining(true);
    	
    	for(Map.Entry<String, Contract> entry : getContractList().entrySet()){
    		LOGGER.fine(String.format("* %s",entry.getKey()));
    	}
        
        LOGGER.fine(String.format("Mining difficulty: \t" + difficulty));    
       
        this.difficulty = difficulty;
        
        int hexZeros = difficulty/16;
        LOGGER.fine(String.format("Hex Zeros: \t" + hexZeros));
        int remaining = difficulty-(hexZeros*16);
        //System.out.println("remaining: \t" + remaining);
        //int lastHex = (int)(15/(Math.pow(2,remaining)));
        int lastHex = 15 - remaining;
        LOGGER.fine(String.format("Hex max: \t" + lastHex));
        Block returnBlock = null;
        long startMiningTime = new Date().getTime();
        
        //String target = new String(new char[hexZeros]).replace('\0', '0'); //Create a string with difficulty * "0"
        //target = target+Integer.toHexString(lastHex);
        long counter = 0;
        MiningProgress.setStartTime(new Date().getTime());
        while(nonce < Long.MAX_VALUE) {
            if(counter % 500000 == 0){
                LOGGER.fine(String.format("%10d Hashes tried",counter));
                if(blockchain.updateBlockChain()){                    
                    //blockchain.redrawBlocksAndContracts();
                	// break, because someone was faster in mining the new Block
                    break;
                }
            }
            if(counter % 1 == 0) {
            	MiningProgress.setHashesCounter(counter);
            }            
            nonce = nonce +1;
            long now = new Date().getTime();
            minedAtTimeStamp = now;
            miningTime = now - startMiningTime;
            blockHash = calculateBlockHash();
            MiningProgress.setCurrentHash(blockHash);
            counter++;
            String inspectStr = blockHash.substring( 0, hexZeros+1);
            long inspectLong = Long.parseLong(inspectStr,16);
            if(inspectLong <= lastHex){ 
            	MiningProgress.setCurrentHash(blockHash);
                LOGGER.fine(String.format("%10d tries to find hash",counter));
                LOGGER.fine(String.format(this.toString()));
                returnBlock = this;
                break;
            }
            else{
                //System.out.println("target : " + target + ", hash: " + hash);
            }
        }
        if(returnBlock == null){
            LOGGER.warning(String.format("No Block found!"));
            nonce = Long.MIN_VALUE;
        }
        LOGGER.fine(String.format("Mining over!"));
        MiningProgress.setMining(false);
        return returnBlock;
    }
    
    public long getTimeStamp(){
        return timeStamp;
    }
    
    public long getMiningTime(){
        return miningTime;
    }
    public long minedAtTimeStamp(){
        return minedAtTimeStamp;
    }
    
    public int getDifficulty(){
        return difficulty;
    }
    
    public LinkedHashMap<String,Contract> getContractList(){
        return contractList;
    }
    
    public String getMerkleRoot() {
		return merkleRoot;
	}

	public long getMinedAtTimeStamp() {
		return minedAtTimeStamp;
	}

	
    
    
}
