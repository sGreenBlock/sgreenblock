import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.ArrayList;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.PublicKey;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.ObjectInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.util.List;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;

import java.util.Date;

public class BlockChain implements java.io.Serializable
{
	// lock is used for writing same files by different threads
	private ReentrantLock lock;
	// lock is used for concurrently accessing getter and setter of this object by other threads
	private Object lockGetSet = new Object();
	
    private volatile ArrayList<Block> blockChain; 
    public static ConcurrentHashMap<String,UTXO> UTXOs = new ConcurrentHashMap<String,UTXO>();
    public static ConcurrentHashMap<String,UTXO> UTXOsTmp = new ConcurrentHashMap<String,UTXO>();
    //private LinkedHashMap<String,Contract> myContractList; 
    private LinkedHashMap<String,Contract> chainContractList;
        
    private Wallet wallet;
    
    private boolean initRun;
    
    public Wallet getWallet() {
		return wallet;
	}

	private static final Logger LOGGER = Logger.getLogger(BlockChain.class.getName());
    
    public BlockChain(Wallet wallet, ReentrantLock lock){
    	this.wallet = wallet;
    	
    	this.lock = lock;
    	
        blockChain = new ArrayList<>();
        chainContractList = new LinkedHashMap<>();     
        
        initRun = true;
    }
    
    public String calculateBlockChainHash() {
    	return calculateBlockChainHash(this.getBlockChain());
    }
    
    // Blockchain Hash equals Hash of last Block
    public String calculateBlockChainHash(ArrayList<Block> blockChain) {  
    	String returnHash = null;
    	synchronized (lockGetSet) {		    	
	    	if(this.getBlockChain().size() > 0)
	    	{
	    		returnHash = this.getBlockChain().get(this.getBlockChain().size()-1).calculateMerkleRoot();
	    	}
    	}
    	return returnHash;
    }
    
    public boolean updateBlockChain() {
    	boolean isChanged = false;
    	LOGGER.fine(String.format("Updating Blockchain"));
    	boolean isInit = this.initRun;    	
    	ArrayList<Block> newestBlockChain = readLongestOldestBlockChainFromDisk(isInit);
    	if(newestBlockChain != null && newestBlockChain.size() > 0){
    		LOGGER.fine(String.format("Found new Blockchain 0x%.6s updating",calculateBlockChainHash(newestBlockChain)));
    		isChanged = true;
    		ArrayList<Block> oldBlockChain = this.getBlockChain();
    		this.setBlockChain(newestBlockChain);
    		boolean isUTXOsAlreadySet = false;
    		if(isInit) {
    			if(this.calculateBlockChainHash() != null){
	    			LOGGER.fine(String.format("Is init run, so searching for existing UTXO list 0x%.6s",this.calculateBlockChainHash()));
	    			ConcurrentHashMap<String,UTXO> foundUTXOs = readUTXOsFromDisk(Wallet.LOCAL_STORAGE, this.calculateBlockChainHash(),false);
	    			if(foundUTXOs != null) {
	    				LOGGER.fine(String.format("Found existing UTXOs to go on"));
	    				BlockChain.UTXOs = foundUTXOs;
	    				isUTXOsAlreadySet = true;
	    			}
    			}
    		}
    		for(int i = 0; i < newestBlockChain.size(); i++){
    			String newHash = newestBlockChain.get(i).blockHash;
    			String oldHash = "";
    			if(i < oldBlockChain.size()){
    				oldHash = oldBlockChain.get(i).blockHash;
    			}
    			
    			if(!isUTXOsAlreadySet && !newHash.equals(oldHash)){
    				if(i < oldBlockChain.size()) {
    					LOGGER.fine(String.format("Removing UTXOs for Block 0x%.6s",oldBlockChain.get(i).getMerkleRoot()));
    					removeUTXOs(oldBlockChain.get(i).getContractList());
    				}
    				LOGGER.fine(String.format("Updating UTXOs for Block 0x%.6s",newestBlockChain.get(i).getMerkleRoot()));
    				updateUTXOs(newestBlockChain.get(i));
    			}
    		}
    		
    		readChainsFromDisk(Wallet.LOCAL_STORAGE,true);
    		// Keep care that longest Chain is avaiable on P2P _and_ LOCAL
    		writeChainToDisk(null,Wallet.LOCAL_STORAGE);
    		readChainsFromDisk(Wallet.P2P_FOLDER,true);
    		writeChainToDisk(null,Wallet.P2P_FOLDER);
    		readUTXOsFromDisk(Wallet.LOCAL_STORAGE, this.calculateBlockChainHash(),true);
    	}
    	this.updateUTXOsNotInChain();
    	this.initRun = false;
    	return isChanged;
	}
       
	private void updateUTXOs(Block block) {
		LOGGER.config(String.format("Update UTXO List for Block 0x%.6s", block.getMerkleRoot()));		
		
		HashMap<String,Contract> contractList = block.getContractList();
		HashMap<String,TransactionOutput> blockUTXOs = new HashMap<>();
		LOGGER.fine(String.format("Adding to UTXO Transactions of Block: %s",block.blockHash));
		// add all TransactionOutputs from Block to UTXO List
		for(Map.Entry<String,Contract> entry : contractList.entrySet()){	
			LOGGER.fine(String.format("Checking if 0x%.6s is Transaction",entry.getValue().getHash()));
			
			if(entry.getValue() instanceof Transaction){
				LOGGER.fine(String.format("0x%.6s is Transaction. Walking through Outputs",entry.getValue().getHash()));
									
				Transaction transaction = (Transaction)entry.getValue();
				if(!transaction.locked(this)) {
					
					for(TransactionOutput transactionOutput : transaction.getTransactionOutputs()){
						LOGGER.fine(String.format("Found Output %s",transactionOutput.id));
						if(this.UTXOs.get(transactionOutput.id) == null){							
							if(transactionOutput.reciepient != null) {
								BlockChain.UTXOs.put(transactionOutput.id, new UTXO(transactionOutput.reciepient, transactionOutput.value, transaction.getHash(), transactionOutput,transaction.getTimeStamp()));
								LOGGER.fine(String.format("Transaction %.6s: Adding UTXO %.6s, Recipient %.6s, Value %.2f", transaction.getHash(), transactionOutput.id,Hash_it.getHash(transactionOutput.reciepient),transactionOutput.value));
							}							
						}
					}
				}					
				
				else {
					LOGGER.fine(String.format("0x%.6s is Transaction, but not fullfilled",entry.getValue().getHash()));
				}
			}
		}
		
		
		// remove all TransactionInputs from UTXO List
		LOGGER.fine(String.format("Removing from UTXO TransactionInputs of Block: %s",block.blockHash));
		removeUsedUTXOs(contractList);
		
		writeUTXOToDisk(Wallet.LOCAL_STORAGE);
		
	}
	
	private void updateUTXOsNotInChain() {
		BlockChain.UTXOsTmp.clear();
		LOGGER.fine(String.format("Getting Transactions not in Chain"));
		LinkedHashMap<String,Contract> notAcceptedContracts = getNotAcceptedContracts();
		LOGGER.fine(String.format("Removing invalid Transactions not in Chain"));
		ArrayList<String> contractsToRemove = new ArrayList<String>();
		for(Map.Entry<String,Contract> entry : notAcceptedContracts.entrySet()){	
			if(!entry.getValue().checkContract(this)) {
				LOGGER.fine(String.format("Removing invalid Transaction %.6s",entry.getKey()));
				contractsToRemove.add(entry.getKey());
			}
		}
		for(String contractToRemove : contractsToRemove) {
			notAcceptedContracts.remove(contractToRemove);
		}
		LinkedHashMap<String,Contract> notAcceptedValidContracts = notAcceptedContracts;
		LOGGER.fine(String.format("Adding to UTXO Transactions not in Chain"));
		for(Map.Entry<String,Contract> entry : notAcceptedValidContracts.entrySet()){	
			LOGGER.fine(String.format("Checking if Transaction 0x%.6s is Transaction",entry.getValue().getHash()));
			if(entry.getValue() instanceof Transaction){
				
				LOGGER.fine(String.format("Contract 0x%.6s is Transaction. Walking through Outputs",entry.getValue().getHash()));
				Transaction transaction = (Transaction)entry.getValue();
				for(TransactionOutput transactionOutput : transaction.getTransactionOutputs()){
					LOGGER.fine(String.format("Found Output %s",transactionOutput.id));
					if(BlockChain.UTXOsTmp.get(transactionOutput.id) == null && transactionOutput.reciepient != null){
						BlockChain.UTXOsTmp.put(transactionOutput.id, new UTXO(transactionOutput.reciepient, transactionOutput.value, transaction.getHash(), transactionOutput,transaction.getTimeStamp()));
						LOGGER.fine(String.format("Transaction %.6s: Adding UTXO %.6s, Recipient %.6s, Value %.2f", transaction.getHash(), transactionOutput.id,Hash_it.getHash(transactionOutput.reciepient),transactionOutput.value));
					}
				}
				
			}
		}
		LOGGER.fine(String.format("Removing from UTXO Transactions not in Chain"));
		notAcceptedContracts = getNotAcceptedContracts();
		removeUsedUTXOs(notAcceptedContracts);
	}
	
	private void removeUTXOs(HashMap<String,Contract> contractList) {
		LOGGER.fine(String.format("Removing all Outputs of Contract List from UTXOs "));
		for(Map.Entry<String,Contract> entry : contractList.entrySet()){
			LOGGER.fine(String.format("Checking if Contract 0x%.6s is Transaction",entry.getValue().getHash()));
			if(entry.getValue() instanceof Transaction){
				LOGGER.fine(String.format("Contract 0x%.6s is Transaction. Walking through Inputs",entry.getValue().getHash()));
				Transaction transaction = (Transaction)entry.getValue();
				if(transaction.getTransactionOutputs() != null){
					for(TransactionOutput transactionOutput : transaction.getTransactionOutputs()){
						if(BlockChain.UTXOs.get(transactionOutput.id) != null && transactionOutput.reciepient != null){
							BlockChain.UTXOs.remove(transactionOutput.id);
							LOGGER.config(String.format("Transaction %.6s: Removing UTXO %.6s, Recipient %.6s, Value %.2f", transaction.getHash(), transactionOutput.id,Hash_it.getHash(transactionOutput.reciepient),transactionOutput.value));
						}
					}
				}
				else{
					LOGGER.finer(String.format("Contract 0x%.6s has no Inputs",entry.getValue().getHash()));
				}
				
			}
		}
	}
	
	private void removeUsedUTXOs(HashMap<String,Contract> contractList) {
    	LOGGER.fine(String.format("Removing TransactionInputs from UTXOs "));
		for(Map.Entry<String,Contract> entry : contractList.entrySet()){
			LOGGER.fine(String.format("Checking if Contract 0x%.6s is Transaction",entry.getValue().getHash()));
			if(entry.getValue() instanceof Transaction){
				LOGGER.fine(String.format("Contract 0x%.6s is Transaction. Walking through Inputs",entry.getValue().getHash()));
				Transaction transaction = (Transaction)entry.getValue();
				if(transaction.getTransactionInputs() != null){
					for(TransactionInput transactionInput : transaction.getTransactionInputs()){
						if(BlockChain.UTXOs.get(transactionInput.transactionOutputId) != null && transactionInput.transactionOutput.reciepient != null){
							BlockChain.UTXOs.remove(transactionInput.transactionOutputId);
							LOGGER.config(String.format("Transaction %.6s: Removing UTXO %.6s, Recipient %.6s, Value %.2f", transaction.getHash(), transactionInput.transactionOutput.id,Hash_it.getHash(transactionInput.transactionOutput.reciepient),transactionInput.transactionOutput.value));
						}
					}
				}
				else{
					LOGGER.finer(String.format("Contract 0x%.6s has no Inputs",entry.getValue().getHash()));
				}
				
			}
		}
    }
    
    public ArrayList<TransactionInput> gatherUTXos(float value) {
    	float inputSum = 0;
    	ArrayList<TransactionInput> txis = new ArrayList<>();
    	LOGGER.fine(String.format("%-15s: %s","Searching UTXOs for User",wallet.getUserKeyHash()));
    	for(Map.Entry<String,UTXO> entry : BlockChain.UTXOs.entrySet()){  
    		UTXO utxo = entry.getValue();
    		String txoPublicKeyHash = Hash_it.getHash(utxo.reciepient.getEncoded());
    		
    		if(txoPublicKeyHash.equals(wallet.getUserKeyHash())){
    			LOGGER.fine(String.format("%-15s: %s","Adding UTXO: ",utxo.txo.id));
    			TransactionInput txi = new TransactionInput(utxo.txo.id);
    			txis.add(txi);
    			inputSum += utxo.value;
    			LOGGER.fine(String.format("%-15s: %s","Sum Inputs",inputSum));
    		}
    		if(inputSum >= value){
    			break;
    		}
    	}
    	if(inputSum >= value) {
    		return txis;
    	}
    	else {
    		return null;
    	}
    }
    
    public void newTransaction(Contract contract) {
    	LOGGER.fine(String.format("Adding Contract: %s",contract.getHash()));
        Wallet.writeContractToDisk(Wallet.P2P_FOLDER,contract);
        Wallet.writeContractToDisk(Wallet.LOCAL_STORAGE,contract);
        
        if(contract instanceof Transaction)
        {
        	LOGGER.fine(String.format("Contract: %s is Transaction, so updating UTXOs",contract.getHash()));
        }
        
        updateBlockChain();
	}

	public void newContract(Contract contract) {
    	LOGGER.fine(String.format("Adding Transaction: 0x%s",contract.getHash()));
        Wallet.writeContractToDisk(Wallet.P2P_FOLDER,contract);
        Wallet.writeContractToDisk(Wallet.LOCAL_STORAGE,contract);    
        
        updateBlockChain();
	}
    
    public boolean mineNewBlock(){  
    	boolean returnBool = true;
        LOGGER.config(String.format("Adding Block"));
        
        while(true){
        	
        	LOGGER.fine(String.format("Checking chain"));
        	if(!isChainValid(this.getBlockChain(), true, true)) {
	        	LOGGER.warning(String.format("Chain is not valid, so stopping!"));
	        	returnBool = false;
	        	MiningProgress.setMining(false);
    			break;
	        }   
        	
        	LOGGER.fine(String.format("Getting Contracts not in chain"));
            LinkedHashMap<String,Contract> notAcceptedValidContracts = getNotAcceptedContracts();
            
            LOGGER.fine(String.format("Checking Contracts for validity"));
            ArrayList<String> contractsToRemove = new ArrayList<>();
            for(Map.Entry<String,Contract> entry : notAcceptedValidContracts.entrySet()){
            	Contract contract = entry.getValue();
            	if(contractsToRemove.contains(entry.getKey())) continue;
            	if(!contract.checkContract(this)) {            		
            		contractsToRemove.add(entry.getKey());
            	}
            	else if(contract instanceof Transaction) {
            		Transaction tx = (Transaction) contract;
            		// remove Transaction which use same TXIs
            		for(TransactionInput txi : tx.getTransactionInputs()) {
            			for(Map.Entry<String,Contract> cEntry : notAcceptedValidContracts.entrySet()){
            				if(!cEntry.getKey().equals(entry.getKey())) {
            					if(cEntry.getValue() instanceof Transaction) {
                					Transaction tx2 = (Transaction) cEntry.getValue();
                					for(TransactionInput txi2 : tx2.getTransactionInputs()) {
                    					if(txi.transactionOutputId.equals(txi2.transactionOutputId)) {
                    						contractsToRemove.add(cEntry.getKey());
                    					}
                    				}
                				}     
            				}            				       				
                		}
            		}            		
            	}            	
            }
            LOGGER.fine(String.format("Removing all non valid Contracts"));
            for(String key : contractsToRemove) {
            	notAcceptedValidContracts.remove(key);
            }
            
            LOGGER.fine(String.format("Getting left off of all valid Contracts"));
            float leftOff = 0.F;
            for(Map.Entry<String,Contract> entry : notAcceptedValidContracts.entrySet()){
            	Contract contract = entry.getValue();
            	if(contract instanceof Transaction) {
            		Transaction tx = (Transaction) contract;
            		float sumTxiValues = 0.F;
            		for(TransactionInput txi : tx.getTransactionInputs()) {
            			sumTxiValues += txi.transactionOutput.value;
            		}
            		float sumTxoValues = 0.F;
            		for(TransactionOutput txo : tx.getTransactionOutputs()) {
            			sumTxoValues += txo.value;
            		}
            		// keep care to not include coins for not known recipient in leftOff
            		// in normal transactions input is most time higher than tx.value, so
            		// rest ist transferred back in txo to sender
            		leftOff = (sumTxoValues < tx.getValue()) ? sumTxiValues - tx.getValue():sumTxiValues - sumTxoValues;
            	}
            }    
            
            LOGGER.fine(String.format("Leftoff of all valid Contracts: %f",leftOff));            
            float mineReward =  Wallet.MINE_REWARD + leftOff;
            
            LOGGER.fine(String.format("Creating Reward Transaction with reward: %f",mineReward));
            Transaction rewardTransaction = new Transaction(wallet.getWalletKey().getPublicKey(), wallet.getWalletKey().getPublicKey(), mineReward, 0.F, null);
    		if(rewardTransaction.processTransaction(0.F)){
    			rewardTransaction.generateSignature(wallet.getWalletKey());
    			LOGGER.fine(String.format("Checking Signature"));
        		if(rewardTransaction.verifiySignature()){
        			LOGGER.fine(String.format("Signature ok"));  
        		}
        		else{
        			LOGGER.warning(String.format("Signature failed!"));
        			returnBool = false;
        			MiningProgress.setMining(false);
        			break;
        		}
    		}
    		else{
    			LOGGER.warning(String.format("Not possible to generate Reward Transaction. Abort Mining."));
    			returnBool = false;
    			MiningProgress.setMining(false);
    			break;
    		}
            
            LOGGER.fine(String.format("Adding Reward Transaction 0x%s",rewardTransaction.getHash()));
            notAcceptedValidContracts.put(rewardTransaction.getHash(), rewardTransaction);     
            
            String perviousHash = "0";
            int lastDifficulty = 0;
            if(getBlockChain().size() > 0){
                Block previousBlock = getBlockChain().get(getBlockChain().size()-1);
                perviousHash = previousBlock.blockHash;
                lastDifficulty = previousBlock.getDifficulty(); 
            }
        	
            // >1 because reward is always there
            MiningProgress.setContractsToMine(notAcceptedValidContracts.size()-1);
            if(!(notAcceptedValidContracts.size() > 1) && this.getBlockChain().size() != 0){
                LOGGER.warning(String.format("No Contracts available to mine... Stopping!"));
                MiningProgress.setMining(false);
                break;
            }
            else{
            	/**
                 * add block for reward
                 */
            	LOGGER.fine(String.format("Generating Reward Transaction 0x%.6s on disk",rewardTransaction.getHash()));
                newTransaction(rewardTransaction);   
                
                LOGGER.fine(String.format("Found Contracts to mine... Starting!"));
            }
            
            Integer difficulty = this.getDifficultyOfBlock(this.getBlockChain(),0);
            
            if(difficulty < Wallet.STARTING_DIFFICULTY){
                difficulty = Wallet.STARTING_DIFFICULTY;                
            }
            
            
        	Block newBlock = new Block("created from BlockChain class", perviousHash, notAcceptedValidContracts);            
            Block minedBlock = newBlock.mineBlock(this,difficulty,rewardTransaction);   
            
            if(minedBlock != null){
            	getBlockChain().add(newBlock);
                writeChainToDisk(null,Wallet.P2P_FOLDER);
                writeChainToDisk(null,Wallet.LOCAL_STORAGE);
                break;
            }
            else{
            	LOGGER.warning(String.format("Were not able to mine the block!"));
            	returnBool = false;
            }
                
        }
        MiningProgress.setMining(false);
        return returnBool;
    }
    
    public Integer getDifficultyOfBlock(ArrayList<Block> blockChain, int blockDepth){
    	
    	Integer returnDifficulty = Wallet.STARTING_DIFFICULTY;
        
    	int indexToGetDifficulty;
    	if(blockChain != null && blockChain.size() > 0) {
    		indexToGetDifficulty = blockChain.size() - blockDepth;
    	}
    	else {
    		indexToGetDifficulty = -1;
    	}
        
        // begin one block before current block to get its difficulty
        int checkLastIndex = indexToGetDifficulty - 1;
        
        if(checkLastIndex > 0 && blockDepth >= 0) {
        	int nBlocks = 0;
            if(checkLastIndex >= Wallet.DEPTH_FOR_AVERAGE_MINING_SPEED){
            	nBlocks = Wallet.DEPTH_FOR_AVERAGE_MINING_SPEED;            
            }
            else {
            	nBlocks = checkLastIndex;
            }
            
            int checkFirstIndex = checkLastIndex - nBlocks + 1;
            
            Block lastBlock = blockChain.get(checkLastIndex);
            LOGGER.finer(String.format("Hash of last Block: 0x%.6s",lastBlock.getMerkleRoot()));
            Block firstBlock = blockChain.get(checkFirstIndex);
            LOGGER.finer(String.format("Hash of first Block: 0x%.6s",firstBlock.getMerkleRoot()));
            
            int difficultyOfLastBlock = lastBlock.getDifficulty();
            
            long timestampFirstBlock = firstBlock.getTimeStamp();
            long timestampLastBlock = lastBlock.getTimeStamp();
            long timeNeeded = timestampLastBlock - timestampFirstBlock;
            
            double timeNeededS = timeNeeded/1000./60.;
            
            LOGGER.finer(String.format("Average Mining Time: %f",timeNeededS));
            
            if(timeNeededS < Wallet.AVERAGE_MINING_TIME_S) {
            	returnDifficulty = difficultyOfLastBlock + 1;
            }
            else if(timeNeededS > Wallet.AVERAGE_MINING_TIME_S) {
            	returnDifficulty = difficultyOfLastBlock - 1;
            }
            else {
            	returnDifficulty = difficultyOfLastBlock;
            }
            if(returnDifficulty < Wallet.STARTING_DIFFICULTY) {
            	LOGGER.finer(String.format("Difficulty %d is below starting value of %d, so resetting",returnDifficulty,Wallet.STARTING_DIFFICULTY));
            	returnDifficulty = Wallet.STARTING_DIFFICULTY;
            }
        }      
        LOGGER.finer(String.format("Difficulty: %d",returnDifficulty));
        return returnDifficulty;
    }
    
    private void writeUTXOToDisk(String folder){
    	ObjectOutputStream oos = null;
        FileOutputStream fout = null;
        try {
			lock.tryLock(2,TimeUnit.SECONDS);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
			System.exit(1);
		}
        try{
        	LOGGER.fine(String.format("Persist UTXOs to folder %s",folder));
            fout = new FileOutputStream(folder+"/"+this.calculateBlockChainHash()+Wallet.FILEENDING_UTXOS);
            oos = new ObjectOutputStream(fout);
            oos.writeObject(BlockChain.UTXOs);
            LOGGER.fine(String.format("UTXOs persisted to folder %s",folder));
        }
        catch (IOException ex) {
        	ex.printStackTrace();
            LOGGER.fine(String.format("Can not write to folder %s: %s",folder,ex));
        }
        finally {  
        	if(lock.isHeldByCurrentThread()) {
        		lock.unlock();
        	}        	
            try {
				oos.close();
			} catch (Exception e) {
				LOGGER.fine(String.format("Could not close connection: "+e));
			}            
        }
    }
    
    public void writeChainToDisk(ArrayList<Block> blockChain, String folder){
        ObjectOutputStream oos = null;
        FileOutputStream fout = null;
        try {
			lock.tryLock(2,TimeUnit.SECONDS);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
			System.exit(1);
		}
        try{
        	synchronized (lockGetSet) {       
            	if(blockChain == null){
            		blockChain = this.getBlockChain();
                }
            	if(blockChain != null && blockChain.size() > 0) {
            		LOGGER.fine(String.format("Persist Chain to folder %s",folder));
    	            fout = new FileOutputStream(folder+"/"+this.calculateBlockChainHash()+Wallet.FILEENDING_BLOCKCHAIN);
    	            oos = new ObjectOutputStream(fout);
            		oos.writeObject(blockChain);
            	}                
			}            
            LOGGER.fine(String.format("Chain persisted to folder %s",folder));
        }
        catch (IOException ex) {
            LOGGER.config(String.format("Can not write to folder %s: %s",folder,ex));
        }
        finally {  
        	if(lock.isHeldByCurrentThread()) {
        		lock.unlock();
        	}        	
            try {
            	if(oos != null)
				{
            		oos.close();
				}
			} catch (Exception e) {
				LOGGER.config(String.format("Could not close connection: "+e));
			}            
        }
    }      
    
    private ConcurrentHashMap<String,UTXO> readUTXOsFromDisk(String path, String chainHash, boolean onlyDelete){
    	ConcurrentHashMap<String,UTXO> readUTXOs = null;
        ObjectInputStream objectinputstream = null;
        try {
			lock.tryLock(2,TimeUnit.SECONDS);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
			System.exit(1);
		}
        final File folder = new File(path);        
        for (final File fileEntry : folder.listFiles()) {            
            if(fileEntry.isFile() && fileEntry.getName().endsWith(Wallet.FILEENDING_UTXOS)){
                String filename = fileEntry.getName();
                Path pathname = Paths.get(path, filename);
                String filenameChainFile = filename.substring(0, filename.length() - Wallet.FILEENDING_UTXOS.length())+Wallet.FILEENDING_BLOCKCHAIN;
                Path pathnameChainFile = Paths.get(path, filenameChainFile);
                LOGGER.config(String.format("pathnameChainFile %s",pathnameChainFile));
                if(!Files.exists(pathnameChainFile)) {
                	LOGGER.config(String.format("Delete UTXO, because chainfile does not exist: %s",pathnameChainFile));
                	try {                		      		
						Files.delete(pathname);						
					} catch (IOException e) {
						LOGGER.warning(String.format("Could not delete file: %s",pathnameChainFile));
					}
                }
                else if(filename.equals(chainHash+Wallet.FILEENDING_UTXOS) && !onlyDelete){      
                	
                	try {
                		LOGGER.config(String.format("Found UTXOs for Chain 0x%.6s",chainHash));
	                    //System.out.println("Reading BlockChain: "+filename);
	                    FileInputStream streamIn = new FileInputStream(path+"/"+filename);
	                    objectinputstream = new ObjectInputStream(streamIn);
	                    readUTXOs = (ConcurrentHashMap<String,UTXO>) objectinputstream.readObject();
	                    //System.out.println("Reading ready");
	                } catch (IOException e) {
	                	e.printStackTrace();
	                	LOGGER.config(String.format("Could not read UTXOs File"));		                    
	                } catch (ClassNotFoundException e) {
	                	LOGGER.warning(String.format("Problem reading UTXOs. If version changed, delete all files from folder "+Wallet.P2P_FOLDER+"!"));
	                    LOGGER.warning(String.format("Stopping Program execution."));
	                    e.printStackTrace();
	                    System.exit(0);
					} finally {
	                    try {
							objectinputstream.close();
						} catch (Exception e) {
							LOGGER.config(String.format("Could not close connection: "+e));
						}
	                }
                }
            }
        }
        if(lock.isHeldByCurrentThread()) {
        	lock.unlock();
        }
        
        return readUTXOs;
    }
        
    private ArrayList<ArrayList<Block>> readChainsFromDisk(String path, boolean remove){
        ArrayList<ArrayList<Block>> chainsList = new ArrayList<ArrayList<Block>>();
        ObjectInputStream objectinputstream = null;
        try {
			lock.tryLock(2,TimeUnit.SECONDS);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
			System.exit(1);
		}
        final File folder = new File(path);        
        for (final File fileEntry : folder.listFiles()) {            
            if(fileEntry.isFile() && fileEntry.getName().endsWith(Wallet.FILEENDING_BLOCKCHAIN)){            	
                String filename = fileEntry.getName();
                LOGGER.fine(String.format("Reading blockchain file: %s",filename));
                if(remove){
                	if(!fileEntry.delete()){
                		LOGGER.info(String.format("Error, could not delete Chain 0x%.6s from path: %s",fileEntry.getName(),path));
                	}
                }
                else{
                	try {
	                    //System.out.println("Reading BlockChain: "+filename);
	                    FileInputStream streamIn = new FileInputStream(path+"/"+filename);
	                    objectinputstream = new ObjectInputStream(streamIn);
	                    ArrayList<Block> readChain = (ArrayList<Block>) objectinputstream.readObject();
	                    //LOGGER.config(String.format("Read blockchain file Size: %s, Hash: %s",readChain.size(),calculateBlockChainHash(readChain)));
	                    if(isChainValid(readChain,false,false))
	                    {
	                    	chainsList.add(readChain);
	                    }
	                    else {
	                    	LOGGER.warning(String.format("Ignoring invalid blockchain: %s",fileEntry.getName(),path));
	                    }
	                    //System.out.println("Reading ready");
	                } catch (IOException e) {
	                	LOGGER.warning(String.format("Could not read BlockChain File"));		                    
	                } catch (ClassNotFoundException e) {
	                	LOGGER.warning(String.format("Problem reading BlockChain. If version changed, delete all files from folder "+Wallet.P2P_FOLDER+"!"));
	                    LOGGER.warning(String.format("Stopping Program execution."));
	                    e.printStackTrace();
	                    System.exit(0);
					} finally {
	                    try {
							objectinputstream.close();
						} catch (Exception e) {
							LOGGER.fine(String.format("Could not close connection: "+e));
						}
	                }
                }
            }
        }
        if(lock.isHeldByCurrentThread()) {
        	lock.unlock();
        }
        LOGGER.fine(String.format("Size of Chainlist: %s",chainsList.size()));
        return chainsList;
    }
    
    private void delteContractFromDisk(String key) {
    	String paths[] = {Wallet.P2P_FOLDER,Wallet.LOCAL_STORAGE};
    	for(String path : paths){
    		try {
    			lock.tryLock(2,TimeUnit.SECONDS);
    		} catch (InterruptedException e1) {
    			e1.printStackTrace();
    			System.exit(1);
    		}
    		final File folder = new File(path);
    		for (final File fileEntry : folder.listFiles()) {            
                if(fileEntry.isFile() && fileEntry.getName().endsWith(Wallet.FILEENDING_CONTRACT)){
                    String filename = fileEntry.getName();
                    if(filename.equals(key+Wallet.FILEENDING_CONTRACT)){
                    	LOGGER.fine(String.format("Deleting Contract 0x%.6s from path: %s",key,path));                    	
                    	try {                    		
							Files.delete(fileEntry.toPath());
						} catch (IOException e) {
							LOGGER.info(String.format("Error, could not delete Contract 0x%.6s from path: %s",key,path));
							LOGGER.info(e.toString());
						}  
                    }
                }
    		}
    		if(lock.isHeldByCurrentThread()) {
    			lock.unlock();
    		}
    		
    	}  
	}
    
    public LinkedHashMap<String,Contract> getOpenContracts(){
    	LinkedHashMap<String,Contract> returnOpenContractList = new LinkedHashMap<>();
    	LinkedHashMap<String,Contract> contractList = new LinkedHashMap<>();
        chainContractList = getContractsFromChain();
        contractList.putAll(chainContractList);
        contractList.putAll(getNotAcceptedContracts());
        // reset myContractList
        for(Map.Entry<String,Contract> entry : contractList.entrySet()){
            Contract contract = entry.getValue();
            boolean isInBlockChain = (this.chainContractList.get(contract.getHash()) != null) ? true : false;
                        
            boolean show = false;
            
            if(isInBlockChain) {            	
            	if(contract.locked(this)) {
            		show = true;
            	}
            	if(contract instanceof GuessBlockSize) {
            		GuessBlockSize gb = (GuessBlockSize) contract;
            		long timeStampEstimated = gb.getTimeStamp()+(gb.getEstimatedSeconds()*1000);
            		if(timeStampEstimated + 60*10*1000 > new Date().getTime()) {
            			show = true;
            		}            		
            	}
            }
            else {
            	show = true;
            }
            
            
        
            if(show)
            	returnOpenContractList.put(contract.getHash(),contract);                   
            
        }
        return returnOpenContractList;
    }
    
    public int blockSizeAt(long millis){
        int count = 0;
        //System.out.printf("Searching Blocks until %ss %n",millis);
        for(Block block : getBlockChain()){
            if(block.minedAtTimeStamp() <= millis){
                //System.out.printf("block at %s <= %s %n",new SimpleDateFormat("HH:mm:ss").format(new Date(block.minedAtTimeStamp())),new SimpleDateFormat("HH:mm:ss").format(new Date(millis)));
                count++;
            }            
        }
        return count;
    }
    
    public LinkedHashMap<String,Contract> getContractsFromChain(){
        LinkedHashMap<String,Contract> allContracts = new LinkedHashMap<String,Contract>();
        ArrayList<Block> blockChain = getBlockChain();
        for(int i = blockChain.size() - 1; i >= 0; i--){
        	Block block = blockChain.get(i);
            LinkedHashMap<String,Contract> contracts = block.getContractList();
            for(Map.Entry<String,Contract> entry : contracts.entrySet()){
                Contract contract = entry.getValue();
                contract.setDepth(blockChain.size() - i);
                allContracts.put(entry.getKey(),contract);
            }
        }
        return allContracts;
    }
    
    public LinkedHashMap<String,Contract> getAllNotAcceptedContracts(){
    	LinkedHashMap<String,Contract> notAcceptedContracts = new LinkedHashMap<String,Contract>();
    	LinkedHashMap<String,Contract> allContractsFromChains = getContractsFromChain();
        LinkedHashMap<String,Contract> allContractsFromDisk = Wallet.getLocalContracts();
    	for(Map.Entry<String,Contract> entry : allContractsFromDisk.entrySet()){
        	if(!allContractsFromChains.containsKey(entry.getKey())){
        		notAcceptedContracts.put(entry.getKey(),entry.getValue());
        	}
        }
    	return notAcceptedContracts;
    }
    
    public LinkedHashMap<String,Contract> getNotAcceptedContracts(){        
        LinkedHashMap<String,Contract> notAcceptedValidContracts = new LinkedHashMap<String,Contract>();
        LinkedHashMap<String,Contract> allContractsFromChains = getContractsFromChain();
        LinkedHashMap<String,Contract> allContractsFromDisk = new LinkedHashMap<String,Contract>(Wallet.getLocalContracts());
        // first get all Contracts without check      
        
        for(Map.Entry<String,Contract> entry : allContractsFromDisk.entrySet()){
        	String contractHash = entry.getKey();
        	Contract contract = entry.getValue();
        	boolean isInChain = allContractsFromChains.containsKey(contractHash);
        	Contract contractFromChain = null;        	
        	boolean belongsToMe = false;
        	if(isInChain) {
        		String contractSenderHash = "empty";
            	String contractRecipientHash = "empty";
        		contractFromChain = allContractsFromChains.get(contractHash);
        		if(contractFromChain.getSenderPubKey() != null)
        		{
        			contractSenderHash = Hash_it.getHash(contractFromChain.getSenderPubKey());
        		}
        		if(contractFromChain.getRecipientPubKey() != null)
        		{
        			contractRecipientHash = Hash_it.getHash(contractFromChain.getRecipientPubKey());
        		}
        		if(contractSenderHash.equals(wallet.getUserKeyHash()) || contractRecipientHash.equals(wallet.getUserKeyHash())) {
        			belongsToMe = true;
        		}
        	}
        	
            if(!isInChain){
            	boolean isValid = contract.checkContract(this);
            	if(isValid){
            		notAcceptedValidContracts.put(contractHash,contract);
                    LOGGER.fine(String.format("Found valid not accepted Contract: %s",contractHash));
            	}
            	else if(!isValid && contract.locked(this)) {
            		notAcceptedValidContracts.put(contractHash,contract);
            		LOGGER.fine(String.format("Contract: %s is invalid, but waiting until it is approved and unlocked",contractHash));
            	}
            	else{
            		LOGGER.fine(String.format("Delete non valid Contract 0x%.6s from Disk",contractHash));
            		
            		if(contract instanceof Transaction) {
            			Transaction transaction = (Transaction) contract;
            			/* Not correct to remove, because should not be added if not valid
            			 * LOGGER.fine(String.format("Is Transaction so removing from UTXOs",entry.getKey()));
            			if(transaction.getTransactionOutputs() != null){
                			for(TransactionOutput transactionOutput : transaction.getTransactionOutputs()){
                				if(BlockChain.UTXOs.get(transactionOutput.id) != null){
                					BlockChain.UTXOs.remove(transactionOutput.id);
                					LOGGER.fine(String.format("Removing from UTXOs: %.6s", transactionOutput.id));
                				}
                			}
                		}*/
            		}            		
                	delteContractFromDisk(contractHash);
            	}
            }
            else if(!belongsToMe || contractFromChain.getDepth() >= Wallet.MIN_DEPTH_CONTRACT){
            	LOGGER.warning(String.format("Delete already accepted Contract 0x%.6s with depth %d from Disk",contractHash,allContractsFromChains.get(entry.getKey()).getDepth()));
            	delteContractFromDisk(contractHash);
            }
            
        }
        return notAcceptedValidContracts;
    }  

	public ArrayList<Block> readLongestOldestBlockChainFromDisk(boolean onlyLocal){
    	ArrayList<Block> returnChain = null;
        ArrayList<ArrayList<Block>> chainsList = readChainsFromDisk(Wallet.LOCAL_STORAGE, false);
        LOGGER.fine(String.format("Chainlist local size: %s",chainsList.size()));
        if(!onlyLocal) {
        	chainsList.addAll(readChainsFromDisk(Wallet.P2P_FOLDER,false));
        }
        
        LOGGER.fine(String.format("Chainlist sum size: %s",chainsList.size()));
        //LOGGER.config(String.format("First Hash: %s",calculateBlockChainHash(chainsList.get(0))));
        
        ArrayList<Block> longestOldest;
        
        if(this.getBlockChain() != null && this.getBlockChain().size() > 0) {
        	longestOldest = this.getBlockChain();
        }
        else {
        	longestOldest = new ArrayList<Block>();
        }
        
        for (ArrayList<Block> block : chainsList) { 
            ArrayList<Block> current = block;
            if(current.size() > longestOldest.size()){
                longestOldest = current;
            }
            else if(current.size() == longestOldest.size()){
                if(longestOldest.size() < 1 || current.get(current.size()-1).getTimeStamp() < longestOldest.get(longestOldest.size()-1).getTimeStamp()){
                    longestOldest = current;
                }
            }            
        }
        LOGGER.fine(String.format("Longest Oldest Chain found: 0x%.6s",calculateBlockChainHash(longestOldest)));
        
        String longestOldestLastHash = (longestOldest.size() >0) ? longestOldest.get(longestOldest.size()-1).calculateBlockHash() : "0";
        String thisLastHash = (this.getBlockChain().size() >0) ? this.getBlockChain().get(this.getBlockChain().size()-1).calculateBlockHash() : "1";
        if( !longestOldestLastHash.equals(thisLastHash) ){
        	LOGGER.config(String.format("Found new Chain 0x%.6s replacing Chain 0x%.6s",longestOldestLastHash,thisLastHash));
            LOGGER.fine(String.format("Setting new Chain: "));
            LOGGER.fine(String.format("size: "+longestOldest.size()));
            returnChain = longestOldest;
            //BlockChain.UTXOs.clear();                        
        }        
        return returnChain;
    }  
    
    
    public String toString(){
        String outputStr = "";
        for(Block block : getBlockChain()){
            outputStr += block.toString();
        }
        return outputStr;
    }    
    
    public boolean processBlockContracts(Block block, boolean checkContracts, boolean executeSmartContracts) {
    	boolean returnBool = true;
    	
    	LOGGER.finer(String.format("Analyzing Block Contracts %s",block.getMerkleRoot()));
    	
    	for(Map.Entry<String,Contract> entry : block.getContractList().entrySet()) {
    		Contract contract = entry.getValue();
    		LOGGER.finer(String.format("Analyzing Contract %s",contract.getHash()));
    		if(executeSmartContracts) {
    			if(contract instanceof SmartContract) {
    				LOGGER.config(String.format("Executing Smart Contract %s",contract.getHash()));
        			((SmartContract)contract).scriptToExecute(this);
        		}
    		}
    		if(checkContracts){
    			contract.checkContract(this);
    		}
    	}
    	
    	
    	return returnBool;
    }
    
    public boolean isChainValid(ArrayList<Block> blockChain, boolean checkContracts, boolean executeSmartContracts){
    	
    	LOGGER.fine(String.format("Check if chain is Valid"));
    	
        boolean returnCheck = true;
        
        Block currentBlock;
        Block previousBlock;
        
        for (int i = blockChain.size()-1, d = 1; i > 1;i--, d++){
        	
        	int difficultyOfCurrentBlock = this.getDifficultyOfBlock(blockChain,d);
        			
            currentBlock = blockChain.get(i);
            previousBlock = blockChain.get(i-1);
            
            if(checkContracts || executeSmartContracts) {
            	LOGGER.config(String.format("Checking Contract: %s, Execute Smart Contracts: %s",checkContracts,executeSmartContracts));
            	processBlockContracts(currentBlock, checkContracts, executeSmartContracts);
            }
            
            if(difficultyOfCurrentBlock != currentBlock.getDifficulty()) {
            	LOGGER.severe(String.format("Set difficulty of Block 0x%.6s is not in consense!",currentBlock.getMerkleRoot()));
            	returnCheck = false;
            	break;
            }
            
            if(!currentBlock.blockHash.equals(currentBlock.calculateBlockHash()) ){              
              LOGGER.severe(String.format("Hash of Block does not match calculated Hash of Block!"));
              LOGGER.severe(String.format("currentBlock.blockHash \t\t: %s",currentBlock.blockHash));
              LOGGER.severe(String.format("currentBlock.calculateHash()\t: %s",currentBlock.calculateBlockHash()));
              returnCheck = false;
              break;
            }
            
            if(!previousBlock.blockHash.equals(currentBlock.previousHash) ) {
              LOGGER.severe(String.format("Hash of previous Block does not match Hash stored in current Block!"));
              LOGGER.severe(String.format("%n previousBlock.blockHash: %s",previousBlock.blockHash));
              LOGGER.severe(String.format("%n currentBlock.previousHash: %s",currentBlock.previousHash));
              returnCheck = false;
              break;
            }            
        }
        return returnCheck;
    }
    
    private void setBlockChain(ArrayList<Block> blockChain){
        synchronized (lockGetSet) {
        	this.blockChain = blockChain;
		}    	
    }
    
    public ArrayList<Block> getBlockChain(){
    	synchronized (lockGetSet) {
    		return this.blockChain;
    	}        
    }

	public LinkedHashMap<String, Contract> getChainContractList() {
		return chainContractList;
	}

	

	
	
}
