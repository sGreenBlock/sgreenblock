import greenfoot.*;  


public class ButtonAccountBalance extends Actor
{
	private WalletUI walletUI;
	
    public ButtonAccountBalance(WalletUI walletUI) {
    	this.walletUI = walletUI;
		
	}

	
    public void act() 
    {
    	if(Greenfoot.mouseClicked(this)) {
        	WorldAccountBalance accountWorld = new WorldAccountBalance(getWorld(),walletUI);
        	Greenfoot.setWorld(accountWorld);
        }        
    }    
}
