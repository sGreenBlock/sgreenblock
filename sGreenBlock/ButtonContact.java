import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import javax.swing.JOptionPane;


public class ButtonContact extends Actor
{
    
    private WalletUI walletUI;
    String pubKey;

	public ButtonContact(WalletUI walletUI, String pubKey) {
		this.walletUI = walletUI;
		this.pubKey = pubKey;
	}

	public void act() 
    {
    	if(Greenfoot.mouseClicked(this)) {
    		newTransaction();
        }
    }    
    
    private void newTransaction() {
    	String inputValue = JOptionPane.showInputDialog("Input amount");
    	if(inputValue != null) {
    		boolean isParsed = false;
    		float inputFloat = 0;
    		try {
    			inputFloat = Float.parseFloat(inputValue);
    			isParsed = true;
    		}
    		catch(NumberFormatException e) {
    			System.out.println("No valid amount");
    		}
    		
        	if(inputFloat > 0. && isParsed) {
        		if(walletUI != null) {
        			System.out.printf("%n######### New Transaction #######%n");
        	        System.out.printf("##################################%n");
        	        System.out.printf("%n%-15s: %s","To",pubKey);
        	        System.out.printf("%n%-15s: %s","Value",inputFloat);
        	    	if(walletUI.getWallet().newTransaction(pubKey, inputFloat, 0.F)){
        	    		System.out.printf("%n%nTransaction successfully created. Stand by until it is included in a block.");
        	    	}
        	    	else{
        	    		System.out.printf("%nTransaction not created! See logs for details.");
        	    	}
        		}
        		else {
        			System.out.println("No walletUI found");
        		}
        	}
        		
    	}    	
    }
}
