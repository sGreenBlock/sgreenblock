import javax.swing.JOptionPane;

import greenfoot.*;  


public class ButtonGuessBlockChainSize extends Actor
{
	WalletUI walletUI;
    public ButtonGuessBlockChainSize(WalletUI walletUI) {
    	this.walletUI = walletUI;
	}

	
    public void act() 
    {
    	if(Greenfoot.mouseClicked(this)) {
    		String inputMinutes = JOptionPane.showInputDialog("Input Minutes");
    		int inputBlocksInt = 0;
    		int inputMinutesInt = 0;
    		boolean validNumber = false;
    		try {
    			inputMinutesInt = Integer.parseInt(inputMinutes);
    			validNumber = true;
    		}
    		catch(NumberFormatException e) {
    			System.out.println("Value is no valid Integer");
    			validNumber = false;
    		}
    		if(validNumber) {
    			String inputBlocks = JOptionPane.showInputDialog("Input estimated Number of Blocks");
        		try {
        			inputBlocksInt = Integer.parseInt(inputBlocks);
        			validNumber = true;
        		}
        		catch(NumberFormatException e) {
        			System.out.println("Value is no valid Integer");
        			validNumber = false;
        		}
        		
        		if(validNumber) {
        			walletUI.newGuessBlockSize(inputMinutesInt, inputBlocksInt);    	
        		}
        				
    		}
    		
        }
    }    
}
