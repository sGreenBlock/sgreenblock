import java.util.logging.Logger;

import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class ButtonMine extends Actor
{
	private WalletUI walletUI;
	private static int tangleAngle = 2;
	private int time;
	public static final int TANGLE_TIME = 5;
	private boolean reset = false;
	private static final Logger LOGGER = Logger.getLogger(Wallet.class.getName());
	
    public ButtonMine(WalletUI walletUI) {
		this.walletUI = walletUI;
		time = 0;
	}

	
    public void act() 
    {
    	if(Greenfoot.mouseClicked(this) && !MiningProgress.isMining()) {
    		MiningProgress.setMining(true);
    		MiningProgress.setContractsToMine(0);
    		startMining();
    		World world = getWorld();
    		world.addObject(new ActorHashesN(), 400, 330);
    		reset = true;
    	}
    	
    	time = (++time) % TANGLE_TIME;
    		
    	if(MiningProgress.isMining() && time == 0) {
    		walletUI.setRotation(tangleAngle);
    		tangleAngle *= -1;
    	}
    	else if(!MiningProgress.isMining() && reset) {
    		if(MiningProgress.getContractsToMine() == 0) {
    			LOGGER.config(String.format("No contracts found to mine"));
        		System.out.printf("%nNo contracts found to mine");
    		}
    		else {
    			LOGGER.config(String.format("Mining over, redrawing!"));
        		System.out.printf("%nHash found with %d tries", MiningProgress.getHashesCounter());
    		}    		
    		reset = false;
    		walletUI.setRotation(0);
    		walletUI.redrawBlocksAndContracts();
    	}  
    	
    	
    }   
    
    public void startMining() {
    	walletUI.mineNewBlock();
    }
}
