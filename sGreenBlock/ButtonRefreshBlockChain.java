import greenfoot.*;  


public class ButtonRefreshBlockChain extends Actor
{
	private WalletUI walletUI;
	
    public ButtonRefreshBlockChain(WalletUI walletUI) {
    	this.walletUI = walletUI;
	}

	
    public void act() 
    {
    	if(Greenfoot.mouseClicked(this)) {
    		walletUI.redrawBlocksAndContracts();
        }
    }    
}
