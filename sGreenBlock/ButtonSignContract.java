import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import javax.swing.JOptionPane;


public class ButtonSignContract extends Actor
{
    
    private WalletUI walletUI;
    Contract contract;
    World backToWorld;
	public ButtonSignContract(World backToWorld, WalletUI walletUI, Contract contract) {
		this.walletUI = walletUI;
		this.contract = contract;
		this.backToWorld = backToWorld;
	}

	public void act() 
    {
    	if(Greenfoot.mouseClicked(this)) {
    		if(contract instanceof GuessBlockSize) {
    			this.walletUI.getWallet().acceptGuessBlockSizeBet(contract.getHash());
    		}
    		else {
    			this.walletUI.getWallet().signContract(contract.getHash());
    		}    		
    		Greenfoot.setWorld(this.backToWorld);
        }
    }   
    
    
}
