import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)


public class ButtonTransaction extends Actor
{
	private WalletUI walletUI;
    public ButtonTransaction(WalletUI walletUI) {
    	this.walletUI = walletUI;
	}

	
    public void act() 
    {
        if(Greenfoot.mouseClicked(this)) {
        	WorldTransfer transferWorld = new WorldTransfer(getWorld(),walletUI);
        	//transferWorld.setWalletUI(walletUI);
        	//transferWorld.setTestStr("Hello World!");
        	Greenfoot.setWorld(transferWorld);
        }
    }    
    
}
