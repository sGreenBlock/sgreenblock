import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)


public class ButtonWallet extends Actor
{
	WalletUI walletUI;
	World world;
	
    public ButtonWallet(World world, WalletUI walletUI) {
		this.walletUI = walletUI;
		this.world = world;
	}

	
    public void act() 
    {
    	if(Greenfoot.mouseClicked(this)) {
        	Greenfoot.setWorld(this.world);
        }
    }    
}
