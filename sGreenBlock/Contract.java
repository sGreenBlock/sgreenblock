import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.logging.Logger;
import java.text.SimpleDateFormat;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.security.PublicKey;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.ObjectInputStream;
import java.io.File;
import java.io.FileInputStream;

public abstract class Contract implements java.io.Serializable
{
	private int depth;
    private String hash;    
    private long timeStamp;
    private PublicKey senderPubKey; 
	private PublicKey recipientPubKey;    
    private String toTransactionHash;
    private String fromTransactionHash;
	private byte[] signature;
	private PublicKey[] approversPubKey;
	private byte[][] approverSignatures;
	private String[] approverHashes;
    
    private static final Logger LOGGER = Logger.getLogger(Contract.class.getName());
    
    public Contract(String from, PublicKey to) {
    	this.timeStamp = new Date().getTime();
    	this.fromTransactionHash = from;
		this.recipientPubKey = to;
		// first hash belongs to sender
        this.approverHashes = new String[0];
        // first signature belongs to sender
        this.approversPubKey = new PublicKey[0];
        this.approverSignatures = new  byte[0][];
        this.depth = 0;	
    }
    
    public Contract(PublicKey from, String to) {
    	this.timeStamp = new Date().getTime();
    	this.senderPubKey = from;
		this.toTransactionHash = to;
		// first hash belongs to sender
        this.approverHashes = new String[0];
        // first signature belongs to sender
        this.approversPubKey = new PublicKey[0];
        this.approverSignatures = new  byte[0][];
        this.depth = 0;	
    }
    
    public Contract(PublicKey from, PublicKey to){
        this.timeStamp = new Date().getTime();
        this.senderPubKey = from;
        this.recipientPubKey = to;
        // first hash belongs to sender
        this.approverHashes = new String[0];
        // first signature belongs to sender
        this.approversPubKey = new PublicKey[0];
        this.approverSignatures = new  byte[0][];
        this.depth = 0;	
    }
    
    public Contract(PublicKey from, PublicKey to, int approversN){
        this.timeStamp = new Date().getTime();
        this.senderPubKey = from;
        this.recipientPubKey = to;
        // first hash belongs to sender
        this.approverHashes = new String[approversN];
        // first signature belongs to sender
        this.approversPubKey = new PublicKey[approversN];
        this.approverSignatures = new  byte[approversN][];
        this.depth = 0;	
    }
    
    public String getToTransactionHash() {
		return toTransactionHash;
	}
    
    public String getFromTransactionHash() {
		return fromTransactionHash;
	}
    
    public void generateSignature(AsymKrypt asymKrypt) {
        this.signature = asymKrypt.signData(getHash());
    }
    
    public void generateSignature(AsymKrypt asymKrypt, int approverIndex) {
    	if(getApproverHash(approverIndex) != null) {
    		this.approverSignatures[approverIndex] = asymKrypt.signData(getApproverHash(approverIndex));
    		LOGGER.finer(String.format("Signing Contract 0x%.6s on position %d",getHash(),approverIndex));
    	}    	
    	else {
    		LOGGER.warning(String.format("Not signing Contract 0x%.6s, because no Hash on position %d",getHash(),approverIndex));
    	}
    }
    
    public boolean verifiySignature() {    	
    	boolean returnIsValid = AsymKrypt.verify(signature,this.getHash().getBytes(),this.getSenderPubKey());
        return returnIsValid;
    }
    
    public boolean verifiySignature(int approverIndex) {
    	LOGGER.fine(String.format("Checking Signature with %d approvers for Contract 0x%.6s",approversPubKey.length,getHash()));
    	boolean returnIsValid = true;
    	boolean isValid = AsymKrypt.verify(signature,this.getHash().getBytes(),this.getSenderPubKey());
    	if(!isValid) {
    		returnIsValid = false;
    		LOGGER.warning(String.format("Contract 0x%.6s Sender Signature not valid!",getHash()));
    	}
    	else {
    		LOGGER.finer(String.format("Contract 0x%.6s Sender Signature valid",getHash()));
    	}
    	for(int i = 0; i < approverIndex +1; i++) {
    		if(this.getApproverHash(i) != null && this.getApproverSignature(i) != null && this.getApproverPubKey(i) != null) {
    			isValid = AsymKrypt.verify(this.getApproverSignature(i),this.getApproverHash(i).getBytes(),this.getApproverPubKey(i));
    			if(!isValid) {
    	    		returnIsValid = false;
    	    		LOGGER.warning(String.format("Contract 0x%.6s Approver %d Signature not valid!",getHash(),i));
    	    	}
    			else {
    				LOGGER.finer(String.format("Contract 0x%.6s Approver %d Signature valid",getHash(),i));
    			}
    		}
    		else {
    			LOGGER.finer(String.format("Contract 0x%.6s Approver %d no Signature data found!",getHash(),i));
    			returnIsValid = false;
    			break;
    		}    		
    	}
        return returnIsValid;
    }
    
    public PublicKey getApproverPubKey(int approverIndex) {
		return approversPubKey[approverIndex];
	}

	public byte[] getApproverSignature(int approverIndex) {
		return this.approverSignatures[approverIndex];
	}
    
    public abstract boolean locked(BlockChain blockChain);
    
    public abstract String calculateHash();
    
    public String calculateApproverHash(int approverIndex) {
    	// approverIndex starts with 0, but 1 is first approver
    	// append PubKey String to last Hash
    	String lastHash = "";
    	if(approverIndex == 0) {
    		lastHash = getHash();
    		if(this.getRecipientPubKey() != null) {
    			lastHash += Hash_it.getHash(this.getRecipientPubKey());
    		}
    	}
    	else {
    		lastHash = getApproverHash(approverIndex-1);
    	}
    		
    	String returnHash = Hash_it.getHash(lastHash + AsymKrypt.getStringFromKey(this.getApproverPubKey(approverIndex)));
    	return returnHash;
    }   
    
    public abstract boolean checkContract(BlockChain blockChain);    
        
    public int compareTo(Contract contract){
        if(this.getTimeStamp() > contract.getTimeStamp())
            return 1;
        else if(this.getTimeStamp() < contract.getTimeStamp())
            return -1;
        else
            return 0;
    }    
    
    public long getTimeStamp(){
        return timeStamp;
    }

    /*
     * Get Last Hash in Hash Chain
     */
	public String getHash(){		
		return this.hash;
	}	
	
	public void setHash(String hash) {
		this.hash = hash;
	}
	
	public String getApproverHash(int approverIndex) {
		return this.approverHashes[approverIndex];
	}
	
	public int getNumberApproverSigns() {
		int countApprovers = 0;
		for(int i = 0; i < approverSignatures.length; i++) {			
			if(getApproverSignature(i) == null) {
				break;
			}
			countApprovers++;
		}
		return countApprovers;
	}
	
	public int getNumberApproverPubKeys() {
		int countApprovers = 0;
		for(int i = 0; i < approversPubKey.length; i++) {			
			if(getApproverPubKey(i) == null) {
				break;
			}
			countApprovers++;
		}
		return countApprovers;
	}
	
	public int getNumberApproversNeeded() {
		return approverHashes.length;
	}
	
	public int getNumberApproverHashes() {
		int countApprovers = 0;
		for(int i = 0; i < approverHashes.length; i++) {			
			if(getApproverHash(i) == null) {
				break;
			}
			countApprovers++;
		}
		return countApprovers;
	}
	
	public int getDepth() {
		return depth;
	}

	public void setDepth(int depth) {
		this.depth = depth;
	}
		
	/*
	 * First Approver has Index 0
	 */
	public void setApproverHash(String hash, int approverIndex) {
		this.approverHashes[approverIndex] = hash;
	}

	public void setApproverPubKey(int approverIndex, PublicKey pubKey) {
		this.approversPubKey[approverIndex] = pubKey;
	}
	
	public boolean isApproved() {
		if(getNumberApproversNeeded() == getNumberApproverSigns()){
			return true;
		}
		else {
			return false;
		}
	}
	
	public PublicKey getSenderPubKey() {
		return senderPubKey;
	}

	public PublicKey getRecipientPubKey() {
		return recipientPubKey;
	}

	public byte[] getSignature() {
		return signature;
	}

	public void setRecipientPubKey(PublicKey recipientPubKey) {
		this.recipientPubKey = recipientPubKey;
	}
	
	@Override
	public String toString(){
		String returnString = "";
		returnString += String.format("%n# Contract: 0x%s%n", this.getHash());
		if(this.getSenderPubKey()!= null) {
			returnString += String.format("%n* From: 0x%s", Hash_it.getHash(this.getSenderPubKey()));
		}		
		String txHashFrom = "null";
		if(this.getFromTransactionHash() != null) {
			txHashFrom = this.getFromTransactionHash();
		}
		returnString += String.format("%n* From Hash: 0x%s", txHashFrom);
		String to = "null";
        if(this.getRecipientPubKey() != null) {
        	to = Hash_it.getHash(this.getRecipientPubKey());
        }
		returnString += String.format("%n* To: 0x%s", to);
		String txHashTo = "null";
		if(this.getToTransactionHash() != null) {
			txHashTo = this.getToTransactionHash();
		}
		returnString += String.format("%n* To Hash: 0x%s", txHashTo);
		returnString += String.format("%n* Approvers needed: %s", this.getNumberApproversNeeded());
		returnString += String.format("%n* Approvers with PubKeys: ");
		for(int i = 0; i < this.getNumberApproversNeeded(); i++) {
			if(this.getApproverHash(i) != null) {
				returnString += String.format("[%d]0x%.6s ", i, Hash_it.getHash(this.getApproverHash(i)));
			}			
		}
		returnString += String.format("%n* Approver Hashes: ");
		for(int i = 0; i < this.getNumberApproverHashes(); i++) {
			if(this.getApproverPubKey(i) != null) {
				returnString += String.format("[%d]0x%.6s ", i, Hash_it.getHash(this.getApproverPubKey(i)));
			}			
		}
		returnString += String.format("%n* Approvers signed: ");
		for(int i = 0; i < this.getNumberApproversNeeded(); i++) {
			if(this.getApproverSignature(i) != null) {
				returnString += String.format("[%d]0x%.6s ", i, Hash_it.getHash(this.getApproverPubKey(i)));
			}	
		}
		returnString += String.format("%n");
		
		return returnString;
	}
	
}
