import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

import java.security.PublicKey;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;

public class GuessBlockSize extends SmartContract
{
    private int estimatedBlocks;
    private int estimatedSeconds;
    
    private final float MINIMUM_INSET = 20.F;
    
    private static final Logger LOGGER = Logger.getLogger(Transaction.class.getName());
    
    public GuessBlockSize(PublicKey from, PublicKey to, float value, float fee,  ArrayList<TransactionInput> inputs, BlockChain blockChain, int estimatedSeconds, int estimatedBlocks){
        super(from, to, 1);
        //this.blockChain = blockChain;
        this.estimatedBlocks = estimatedBlocks;
        setEstimatedSeconds(estimatedSeconds);
        
    }
    
    @Override
    public String calculateHash(){   
        String strToHash = super.calculateHash() + Long.toString(super.getTimeStamp()) + estimatedBlocks + Integer.toString(estimatedSeconds);
        String returnHash = Hash_it.getHash(strToHash);
        LOGGER.finer(String.format("Calculating Hash: %s",returnHash));
        return Hash_it.getHash(strToHash);
    }
    
    public long getEstimatedSeconds(){
        return estimatedSeconds;
    }
    public void setEstimatedSeconds(int estimatedSeconds){
        if(estimatedSeconds < 60)
            estimatedSeconds = 60;
        this.estimatedSeconds = estimatedSeconds;
    }
    public long getEstimatedBlocks(){
        return estimatedBlocks;
    }

	@Override
	public boolean scriptToExecute(BlockChain blockChain) {
		LOGGER.finer(String.format("Executing Script of Smart Contract 0x%.6s",this.getHash()));
		boolean active = false;
		long contractTime = this.getTimeStamp();
        long timeStampEstimated = contractTime+(this.getEstimatedSeconds()*1000);
        if(timeStampEstimated > new Date().getTime()) {
        	active = false;
        	LOGGER.finer(String.format("Contract 0x%.6s: Stopping Script, starting in %d seconds",this.getHash(),(timeStampEstimated-new Date().getTime())/1000));
        }
        else {
        	active = true;
        }
        
        boolean betWon = false;
		
		if(active && this.getSCBalance(blockChain) >= MINIMUM_INSET) {
            int blocks = blockChain.blockSizeAt(timeStampEstimated);
            if (blocks==this.getEstimatedBlocks()) {
            	betWon = true;
            }
            if(betWon) {
    			newSmartTransaction(blockChain, this.getSenderPubKey(), this.getSCBalance(blockChain), 0.F);
    		}
    		else {
    			newSmartTransaction(blockChain, this.getApproverPubKey(0), this.getSCBalance(blockChain), 0.F);
    		}
    	}	
		
		return true;
	}

	@Override
	public boolean locked(BlockChain blockChain) {
		long contractTime = this.getTimeStamp();
		long timeStampEstimated = contractTime+(this.getEstimatedSeconds()*1000);
		return timeStampEstimated > new Date().getTime();
	}
	
	@Override
	public String toString() {
		String returnString = "";
		returnString = super.toString();
		returnString += String.format("%n## Guess BlockChain size Transaction%n");
		returnString += String.format("%n* Estimated Blocks: %s", this.estimatedBlocks);
		returnString += String.format("%n* At Datetime: %s", new SimpleDateFormat("Y-M-d HH:mm:ss").format(new Date(this.getTimeStamp()+this.getEstimatedSeconds() * 1000)));
		returnString += String.format("%n");
		return returnString;
	}

	@Override
	public boolean checkContract(BlockChain blockChain) {
		boolean returnIsValid = true;
		boolean isSigned = false;
		/*
		 * check if signed with correct private key
		 */
		if(this.getSenderPubKey() != null && verifiySignature(this.getNumberApproversNeeded()-1) == false) {
			LOGGER.fine(String.format("Transaction 0x%.6s Signature failed to verify. Result: %s",this.getHash(),verifiySignature(this.getNumberApproversNeeded())));
			returnIsValid = false;
		}
		else if(this.getSenderPubKey() == null) {
			isSigned = false;
		}
		else {
			isSigned = true;
		}
		return returnIsValid;
	}
}
