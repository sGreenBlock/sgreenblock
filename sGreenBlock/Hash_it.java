import java.security.*;

public class Hash_it
{

  
  public Hash_it()
  {
  }

  public static String getHash(String strToHash){
    try{
        return getHash(strToHash.getBytes("UTF-8"));    
    }
    catch(Exception e) {
      throw new RuntimeException(e);
    }
  }
  
  public static String getHash(PublicKey pubKeyToHash){
	    try{
	        return getHash(pubKeyToHash.getEncoded());    
	    }
	    catch(Exception e) {
	      throw new RuntimeException(e);
	    }
	  }
  
  public static String getHash(byte[] bytes){
    try{
        byte[] bytesOfMessage = bytes;
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] thedigest = md.digest(bytesOfMessage);
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < thedigest.length; i++) {
            String hex = Integer.toHexString(0xff & thedigest[i]);
            if(hex.length() == 1)
                 hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }
    catch(Exception e) {
      throw new RuntimeException(e);
    }

  }
  
  public String myCalculateHash(){
    int result = 17;
    //result = 31 * result + previousHash.hashCode();
    // = 31 * result + (int) timeStamp;
    //result = 31 * result + (int) transactionData.hashCode();
    //result = 31 * result + nonce;
    //result = 31 * result;
    return Integer.toString(result);
  }
}
