import java.util.Date;

public class MiningProgress {
	
	private static long hashesCounter = 0;
	private static boolean isMining = false;
	private static int contractsToMine = 0;
	private static String currentHash = "";
	private static Long startTime;
	
	public static long getHashesCounter() {
		return hashesCounter;
	}
	public static void setHashesCounter(long hashesCounter) {
		MiningProgress.hashesCounter = hashesCounter;
	}
	public static boolean isMining() {
		return isMining;
	}
	public static void setMining(boolean isMining) {
		MiningProgress.isMining = isMining;
	}
	public static String getCurrentHash() {
		return currentHash;
	}
	public static void setCurrentHash(String currentHash) {
		MiningProgress.currentHash = currentHash;
	}
	public static Long getStartTime() {
		return startTime;
	}
	public static void setStartTime(Long startTime) {
		MiningProgress.startTime = startTime;
	}
	public static int getContractsToMine() {
		return contractsToMine;
	}
	public static void setContractsToMine(int contractsToMine) {
		MiningProgress.contractsToMine = contractsToMine;
	}
	
	
}
