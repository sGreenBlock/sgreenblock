import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.file.Paths;

public class P2PClient {

	private Wallet wallet;
	public ArrayList<String> addressArray;
	public ArrayList<String> knownContracts;

	private static final Logger LOGGER = Logger.getLogger(P2PClient.class.getName());
	
	long startingTime;
	int sentN;
	
	public P2PClient(Wallet wallet) {
		this.wallet = wallet;
		addressArray = this.readNodesFromFile((Paths.get(Wallet.LOCAL_STORAGE, Wallet.FILE_NODE_IPS)).toString());
		knownContracts = new ArrayList<String>();
		startingTime = new Date().getTime();
		sentN = 0;
	}

	public void start(P2PSender p2pSender) {		
		LOGGER.config(String.format("Starting P2P Client"));
		boolean runningClient = true;
		while(runningClient) {
			long start_time = new Date().getTime();
			LOGGER.config(String.format("New Polling run"));
			ConcurrentHashMap<String, P2PNodeInfo> nodesAvailable = Wallet.getNodesAvailable();
			Map<String, P2PNodeInfo> sortedNodesAvailable = nodesAvailable.entrySet().stream()
					.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
					.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue,
							LinkedHashMap::new));
			ArrayList<String> addressArrayNodesAvailable = new ArrayList<String>();
			for (Map.Entry<String, P2PNodeInfo> entry : sortedNodesAvailable.entrySet()) {
				addressArrayNodesAvailable.add(entry.getKey());
				LOGGER.config(String.format("Adding to nodes list: %s", entry.getKey()));
			}
			
			for(String address : addressArray) {
				addressArrayNodesAvailable.add(address);
			}

			boolean runningNodeCheck = true;
			int countSuccessfullConnects = 0;
			while(countSuccessfullConnects < Wallet.P2P_POLL_NODES_N && addressArrayNodesAvailable.size() > 0) {
			for(int i = 0; runningNodeCheck
					&& i < addressArrayNodesAvailable.size(); i++) {

				boolean removeHost = true;

				String addrToConnect = addressArrayNodesAvailable.get(i);
				String ipToConnect = ipFromString(addressArrayNodesAvailable.get(i));
				int portToConnect = portFromString(addressArrayNodesAvailable.get(i));

				LOGGER.config(String.format("Connecting to: %s", addrToConnect));
				
				LOGGER.config(String.format("%nStart Client time: %d %n",(new Date().getTime()-start_time)/1000));

				boolean goOn = true;

				if (isNodeIP(ipToConnect, portToConnect)) {
					goOn = true;
				} else {
					LOGGER.config(String.format("My address, so skipping: %s", addrToConnect));
					goOn = false;
					Wallet.removeNodeAvailable(addrToConnect);
					addressArrayNodesAvailable.remove(addrToConnect);
				}

				FileOutputStream fos = null;
				BufferedOutputStream bos = null;
				Socket sock = null;

				if (goOn) {
					try {						
						
						sock = new Socket();
						sock.setSoTimeout(Wallet.P2P_Socket_Timeout);
						sock.setReceiveBufferSize( 128000 );
						sock.setSendBufferSize( 128000 );
						sock.connect(new InetSocketAddress(ipToConnect, portToConnect), 200);
						
						LOGGER.config(String.format("Socket open: %s", addrToConnect));
						
						LOGGER.config(String.format("%nConnection time: %d %n",(new Date().getTime()-start_time)/1000));

						Wallet.addNode(addrToConnect, null);
						Wallet.addNodeAvailable(addrToConnect);
						removeHost = false;

						if (!(countSuccessfullConnects < Wallet.P2P_POLL_NODES_N)) {
							goOn = false;
						}

						if(goOn) {
							if(p2pSender.connect(sock, addrToConnect, start_time) && p2pSender.start()) {								
									countSuccessfullConnects++;		
									addressArrayNodesAvailable.remove(addrToConnect);
									sentN++;
							}		
							else {
								removeHost = true;
							}
						}

					} catch (UnknownHostException e) {
						LOGGER.warning(String.format("Unknown Host: %s", e));
						removeHost = true;
					} catch (SocketTimeoutException e) {
						LOGGER.warning(String.format("%s", e));
					} catch (IOException e) {
						LOGGER.warning(String.format("IO Error: %s", e));
						removeHost = true;
					} finally {

						try {
							if (fos != null)
								fos.close();
							if (bos != null)
								bos.close();
							if (sock != null)
								sock.close();
						} catch (IOException e) {
						}
					}
				}

				try {
					Thread.sleep(Wallet.P2P_POLL_INTERVAL_S * 1000);
				} catch (InterruptedException e) {
					LOGGER.warning(String.format("Thread interrupted, so stopping Client."));
					runningClient = false;
				}
				
				if (removeHost) {
					Wallet.removeNodeAvailable(addrToConnect);
					addressArrayNodesAvailable.remove(addrToConnect);
					LOGGER.config(String.format("Remove from available Nodes: %s", addrToConnect));
				}
			}
			LOGGER.config(String.format("%nClient run time: %d %n",(new Date().getTime()-start_time)/1000));
			}
			if(sentN < 1 && addressArrayNodesAvailable.size() > 0) {
				try {
					Thread.sleep(Wallet.P2P_POLL_INTERVAL_S * 2000);
				} catch (InterruptedException e) {
					LOGGER.warning(String.format("Thread interrupted, so stopping Client."));
					runningClient = false;
				}
			}
			else {
				runningClient = false;
			}
		}
		LOGGER.warning(String.format("Closing P2P Client"));
	}

	/**
	 * Check if IP is valid and not my own
	 * 
	 * @param ipToConnect String with IP
	 * @return true if it is Node IP
	 */
	public static boolean isNodeIP(String ipToConnect, int portToConnect) {
		InetAddress inetAddr = null;
		try {
			inetAddr = InetAddress.getByName(ipToConnect);
		} catch (UnknownHostException e1) {
			return false;
		}

		boolean isMyIP = false;
		// Check if the address is a valid special local or loop back
		if (inetAddr.isAnyLocalAddress() || inetAddr.isLoopbackAddress())
			isMyIP = true;

		if (!isMyIP) {
			// Check if the address is defined on any interface
			try {
				isMyIP = NetworkInterface.getByInetAddress(inetAddr) != null;
			} catch (SocketException e) {
				return false;
			}
		}

		if (isMyIP && portToConnect == Wallet.P2P_MY_PORT) {
			return false;
		} else {
			return true;
		}

	}

	public ArrayList<String> readNodesFromFile(String filename) {
		FileReader fileReader;
		ArrayList<String> lines = new ArrayList<String>();

		try {
			fileReader = new FileReader(filename);
			BufferedReader bufferedReader = new BufferedReader(fileReader);

			String line = null;
			while ((line = bufferedReader.readLine()) != null) {
				lines.add(line);
			}
			bufferedReader.close();
		} catch (IOException e) {
			LOGGER.fine(String.format("Problems reading file with IP List: %s", filename));
			// LOGGER.warning(String.format("Adding: %s","127.0.0.1:43210"));
			// lines.add("127.0.0.1");
		}
		return lines;
	}

	public static String ipFromString(String str) {
		return str.split(":")[0];
	}

	public static int portFromString(String str) {
		return Integer.parseInt(str.split(":")[1]);
	}
}
