import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;



public class P2PCommunication {
	
	private static final Logger LOGGER = Logger.getLogger(P2PCommunication.class.getName());
	
	public static boolean requestP2Pstuff(Wallet wallet, Socket sock, long start_time) {
		
		OutputStream os = null;
		OutputStreamWriter osw = null;
		BufferedWriter bw = null;
		
		boolean returnSucceed = true;
		
		try {
			os = sock.getOutputStream();
			osw = new OutputStreamWriter(os);
			bw = new BufferedWriter(osw);
		} catch (IOException e) {
			LOGGER.fine(String.format("Could not create OutputStream"));
		}	
		
		LOGGER.fine(String.format("%nStream start time: %d %n",(new Date().getTime()-start_time)/1000));
		
		String number;
		String sendMessage;
		
		if(os != null && osw != null && bw != null) {
				
			try {

				LOGGER.fine(String.format("Sending my server port %s ", Wallet.P2P_MY_PORT));
				number = P2PServer.P2P.MY_SERVER_PORT.name();
				sendMessage = number + " " + Wallet.P2P_MY_PORT + "\n";
				bw.write(sendMessage);
				bw.flush();
				LOGGER.fine(String.format("%nSending port time: %d %n",(new Date().getTime()-start_time)/1000));

				LOGGER.fine(String.format("Sending my User Key Hash %s ", wallet.getUserKeyHash()));
				number = P2PServer.P2P.MY_KEY_HASH.name();
				sendMessage = number + " " + wallet.getUserKeyHash() + "\n";
				bw.write(sendMessage);
				bw.flush();
				LOGGER.fine(String.format("%nSending key time: %d %n",(new Date().getTime()-start_time)/1000));

				LOGGER.fine(String.format("Requesting Blockchain Hash ... "));
				number = P2PServer.P2P.GET_CHAIN_HASH.name();
				sendMessage = number + "\n";
				bw.write(sendMessage);
				bw.flush();
				LOGGER.fine(String.format("%nRequesting Blockchain Hash time: %d %n",(new Date().getTime()-start_time)/1000));

				LOGGER.fine(String.format("Reading Blockchain Hash ... "));
				InputStream is = sock.getInputStream();
				InputStreamReader isr = new InputStreamReader(is);
				BufferedReader br = new BufferedReader(isr);
				String blockChainInfoString = br.readLine();
				String[] blockChainInfoArr = new String[0];
				
				if(blockChainInfoString != null) {
					blockChainInfoArr = blockChainInfoString.split(" ");
				}
				
				if(blockChainInfoArr.length == 3) { 
					
					String blockChainHash = blockChainInfoArr[0];
					Integer blockChainLength = Integer.valueOf(blockChainInfoArr[1]);
					Long blockChainAge = Long.valueOf(blockChainInfoArr[2]);
					LOGGER.fine(String.format("Received Blockchain Hash 0x%s", blockChainHash));
					
					ArrayList<Block> blockChain = wallet.getBlockChain().getBlockChain();
					
					boolean isNew = false;
					if (blockChain.size() < blockChainLength) {
						isNew = true;
					}
					else if(blockChain.size() == blockChainLength) {
						if(blockChain.get(blockChain.size()-1).getTimeStamp() > blockChainAge) {
							isNew = true;
						}
					}
					
					if (isNew && !blockChainHash.equals(wallet.getBlockChain().calculateBlockChainHash())) {
						LOGGER.fine(String.format("New Blockchain, so requesting whole blockchain ... 0x%s",
								blockChainHash));
						number = P2PServer.P2P.GET_CHAIN.name();
						sendMessage = number + "\n";
						bw.write(sendMessage);
						bw.flush();
			
						LOGGER.fine(String.format("Receiving Blockchain 0x%s", blockChainHash));
						ObjectInputStream ois = new ObjectInputStream(sock.getInputStream());
						Object receivedObject = ois.readObject();
						ArrayList<Block> receivedBockChain = null;
						if (receivedObject instanceof ArrayList) {
							receivedBockChain = (ArrayList<Block>) receivedObject;
						}
						if (receivedBockChain != null) {
							wallet.getBlockChain().writeChainToDisk(receivedBockChain, Wallet.P2P_FOLDER);
						}
						LOGGER.fine(String.format("%nReading Blockchain time: %d %n",(new Date().getTime()-start_time)/1000));
					}
				}
				

				LOGGER.fine(String.format("Requesting Contract Hashes ... "));
				number = P2PServer.P2P.GET_CONTRACT_HASHES.name();
				sendMessage = number + "\n";
				bw.write(sendMessage);
				bw.flush();
				LOGGER.fine(String.format("%nRequesting Contract Hashes time: %d %n",(new Date().getTime()-start_time)/1000));

				String nodeAddress;
				LOGGER.fine(String.format("Reading Contract Hashes ... "));

				ArrayList<String> contractsToRequest = new ArrayList<String>();
				for (nodeAddress = br.readLine(); nodeAddress != null
						&& !nodeAddress.equals(P2PServer.P2P.ALL_SENT.name()); nodeAddress = br
								.readLine()) {
					LOGGER.fine(String.format("Received Contract Hash ... 0x%s", nodeAddress));
					contractsToRequest.add(nodeAddress);
					LOGGER.fine(String.format("%nReceiving Contract Hash time: %d %n",(new Date().getTime()-start_time)/1000));
				}
				

				if (contractsToRequest.size() > 0) {
					LOGGER.fine(String.format("Requesting missing Contracts ... "));
					LinkedHashMap<String, Contract> contractList = wallet.getBlockChain().getNotAcceptedContracts();
					
					for (String contractHashToRequest : contractsToRequest) {
						if (!contractList.containsKey(contractHashToRequest)) {
							LOGGER.fine(String.format("Requesting Contract 0x%s", contractHashToRequest));
							sendMessage = P2PServer.P2P.GET_CONTRACT + " " + contractHashToRequest + "\n";
							bw.write(sendMessage);
							bw.flush();
							LOGGER.fine(String.format("%nRequesting Contract time: %d %n",(new Date().getTime()-start_time)/1000));

							LOGGER.fine(String.format("Receiving Contract 0x%s", contractHashToRequest));
							ObjectInputStream ois = new ObjectInputStream(sock.getInputStream());
							Contract receivedContract = (Contract) ois.readObject();
							if (receivedContract != null) {
								Wallet.writeContractToDisk(Wallet.P2P_FOLDER,
										receivedContract);
							}
							LOGGER.fine(String.format("%nReceiving Contract time: %d %n",(new Date().getTime()-start_time)/1000));
						}
					}
				}

				LOGGER.fine(String.format("Requesting nodes list ... "));
				number = P2PServer.P2P.GET_NODES.name();
				sendMessage = number + "\n";
				bw.write(sendMessage);
				bw.flush();
				LOGGER.fine(String.format("%nRequesting nodes list time: %d %n",(new Date().getTime()-start_time)/1000));

				LOGGER.fine(String.format("Reading node addresses ... "));

				for (nodeAddress = br.readLine(); nodeAddress != null
						&& !nodeAddress.equals(P2PServer.P2P.ALL_SENT.name()); 
						nodeAddress = br.readLine()) {
					LOGGER.fine(String.format("Received node: %s", nodeAddress));

					String nodeIpToConnect = P2PClient.ipFromString(nodeAddress);
					int nodePortToConnect = P2PClient.portFromString(nodeAddress);

					if (P2PClient.isNodeIP(nodeIpToConnect, nodePortToConnect)) {
						Wallet.addNode(nodeAddress, "n.a.");
					}
					LOGGER.fine(String.format("%nReceiving nodes list time: %d %n",(new Date().getTime()-start_time)/1000));
				}
			}
			catch (SocketTimeoutException e) {
				LOGGER.warning(String.format("Interrupted: %s", e));
				P2PData.requests_interrupted_n++;
				returnSucceed = false;
			} catch (IOException e) {
				LOGGER.warning(String.format("Interrupted: %s", e));
				P2PData.requests_interrupted_n++;
				returnSucceed = false;
			} catch (ClassNotFoundException e) {
				LOGGER.warning(String.format("Interrupted: %s", e));
				P2PData.requests_interrupted_n++;
				returnSucceed = false;
			} finally {
				LOGGER.fine(String.format("Sending exit to close connection ... "));
				number = P2PServer.P2P.EXIT.name();
				sendMessage = number + "\n";
				try {
					bw.write(sendMessage);
					bw.flush();
				} catch (IOException e) {
					LOGGER.warning(String.format("Could send exit to close connection: %s", e));
				}				
			}
		}
			
		return returnSucceed;
		
		
	}
	public static boolean shareP2Pstuff(Wallet wallet, Socket sock) {
		
		boolean returnSucceed = true;
		
		try {
			ObjectOutputStream oos = null;
			
			InputStream is = sock.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			
			String nodeKeyHash = null;
			String nodeIP = null;
			Integer nodePort = null;
			
			OutputStream os = null;
			BufferedWriter bw = null;
			OutputStreamWriter osw = null;
			
			boolean runningConnection = true;
			while (runningConnection) {
				LOGGER.fine(String.format("Waiting for command ... "));
				String receivedMessage = br.readLine();
							
					
				boolean goOn = true;
				
				P2PServer.P2P receivedCommand = null;					
				
				if(receivedMessage != null) {
					String[] receivedMessageArr = receivedMessage.split(" ");
					try {
						receivedCommand = P2PServer.P2P.valueOf(receivedMessageArr[0]);
					}
					catch(IllegalArgumentException e) {
						LOGGER.warning(String.format("Could not read command. Connection will be closed!"));
					}
				}		
				else {
					LOGGER.warning(String.format("No Data sent!"));
					P2PData.shares_interrupted_n++;
					returnSucceed = false;
				}
				
				
				LOGGER.fine(String.format("Message received from client is %s", receivedMessage));
	
				os = sock.getOutputStream();
				osw = new OutputStreamWriter(os);
				bw = new BufferedWriter(osw);
	
				if (receivedCommand == P2PServer.P2P.GET_CHAIN_HASH) {
					ArrayList<Block> blockChain = wallet.getBlockChain().getBlockChain();
					String blockChainHash = "";
					if(blockChain != null && blockChain.size() > 0) {
						blockChainHash = wallet.getBlockChain().calculateBlockChainHash()+" "+blockChain.size()+" "+blockChain.get(blockChain.size()-1).getTimeStamp();
						LOGGER.fine(String.format("Sending Blockchain Hash ... %s", blockChainHash));
					}
					
					bw.write(blockChainHash + "\n");
					bw.flush();
					
				} else if (receivedCommand == P2PServer.P2P.GET_CHAIN) {
					String blockChainHash = wallet.getBlockChain().calculateBlockChainHash();
					LOGGER.fine(String.format("Sending full Blockchain ... %s", blockChainHash));
					oos = new ObjectOutputStream(sock.getOutputStream());
					oos.writeObject(wallet.getBlockChain().getBlockChain());
					oos.flush();
				} else if (receivedCommand == P2PServer.P2P.GET_CONTRACT_HASHES) {
					LOGGER.fine(String.format("Sending Contract Hashes ... "));
					LinkedHashMap<String, Contract> contractList = wallet.getBlockChain()
							.getNotAcceptedContracts();
	
					if (contractList.size() > 0) {
						for (Map.Entry<String, Contract> entry : contractList.entrySet()) {
							LOGGER.fine(String.format("Sending Contract Hash %s", entry.getValue().getHash()));
							bw.write(entry.getValue().getHash() + "\n");
							bw.flush();
						}
					}
	
					bw.write(P2PServer.P2P.ALL_SENT.name() + "\n");
					bw.flush();
	
				} else if (receivedCommand == P2PServer.P2P.GET_NODES) {
					LOGGER.fine(String.format("Sending available nodes list ... "));
					ConcurrentHashMap<String, P2PNodeInfo> nodesAvailable = Wallet.getNodesAvailable();
					ArrayList<String> ipArrayNodesAvailable = new ArrayList<String>();
					for (Map.Entry<String, P2PNodeInfo> entry : nodesAvailable.entrySet()) {
						if (entry.getValue().calcConnectionRating() > 0) {
							LOGGER.fine(String.format("Sending node %s", entry.getKey()));
							bw.write(entry.getKey() + "\n");
							bw.flush();
						}
					}
	
					bw.write(P2PServer.P2P.ALL_SENT.name() + "\n");
					bw.flush();
	
				} else if (receivedCommand == P2PServer.P2P.GET_CONTRACT) {
					String contractHash = receivedMessage.split(" ")[1];
					LOGGER.fine(String.format("Sending Contract 0x%s", contractHash));
	
					oos = new ObjectOutputStream(sock.getOutputStream());
					oos.writeObject(wallet.getBlockChain().getNotAcceptedContracts().get(contractHash));
					oos.flush();
	
				} else if (receivedCommand == P2PServer.P2P.MY_SERVER_PORT) {
					String nodeIPRec = sock.getInetAddress().toString().substring(1);
					int nodePortRec = Integer.parseInt(receivedMessage.split(" ")[1]);
					String nodeAddress = nodeIPRec + ":" + nodePortRec;
					LOGGER.fine(String.format("Address of node: %s", nodeAddress));
					nodeIP = nodeIPRec;
					nodePort = nodePortRec;
	
				} else if (receivedCommand == P2PServer.P2P.MY_KEY_HASH) {
					String nodeKeyHashRec = receivedMessage.split(" ")[1];
					LOGGER.fine(String.format("Key Hash of node: %s", nodeKeyHashRec));
					nodeKeyHash = nodeKeyHashRec;
				} else {
					LOGGER.fine(String.format("Closing connection ... "));
					runningConnection = false;
				}	
	
				if (nodeIP != null && nodePort != null && nodeKeyHash != null) {
					if (P2PClient.isNodeIP(nodeIP, nodePort)) {
						String addressToAdd = nodeIP + ":" + nodePort;
						LOGGER.fine(String.format("Add node %s of user %s", addressToAdd, nodeKeyHash));
						Wallet.addNode(addressToAdd, nodeKeyHash);
					}
				}		
			}
			
		} catch(IOException e) {
			LOGGER.fine(String.format("Interrupted: %s", e));
			P2PData.shares_interrupted_n++;
			returnSucceed = false;
		}
		/*if (osw != null)
			osw.close();
		if(bw != null)
			bw.close();*/
		return true;
	}

}
