import java.util.Date;

public class P2PNodeInfo implements Comparable<P2PNodeInfo> {
	
	public static int BEST_CONNECTION_QUALITY = 5;
	
	private String address;
	private String userKeyHash;
	private int connectionQuality;
	private int goodConnections;
	private int badConnections;
	private long firstConnection;
	private long lastConnectionSucceed;
	private long lastConnectionMissed;
	private int nodeListHash;
	
	public P2PNodeInfo(String address, String userKeyHash) {
		super();
		this.address = address;
		this.userKeyHash = userKeyHash;
		firstConnection = new Date().getTime();
		connectionQuality = 0;
		goodConnections = 0;
		badConnections = 0;
	}
	
	public void storeConnectInfo(boolean succeed) {
		if(succeed) {
			connectionQuality = (connectionQuality < BEST_CONNECTION_QUALITY) ? connectionQuality + 1 : connectionQuality;
			goodConnections = (goodConnections < Integer.MAX_VALUE) ? goodConnections + 1 : goodConnections;
			lastConnectionSucceed = new Date().getTime();
		}
		else {
			connectionQuality = (connectionQuality > 0) ? connectionQuality - 1 : connectionQuality;
			badConnections = (badConnections < Integer.MAX_VALUE) ? badConnections + 1 : badConnections;
			lastConnectionMissed = new Date().getTime();
		}
	}
	
	public int calcConnectionRating() {
		return goodConnections - badConnections;
	}
	
	public int getConnectionQuality() {
		return connectionQuality;
	}	
		
	public int getGoodConnections() {
		return goodConnections;
	}

	public int getBadConnections() {
		return badConnections;
	}		

	public String getAddress() {
		return address;
	}

	public long getFirstConnection() {
		return firstConnection;
	}

	public long getLastConnectionSucceed() {
		return lastConnectionSucceed;
	}

	public long getLastConnectionMissed() {
		return lastConnectionMissed;
	}

	public int compareTo(P2PNodeInfo node){  
		if(connectionQuality==node.connectionQuality)  
			return 0;  
		else if(connectionQuality>node.connectionQuality)  
			return 1;  
		else  
			return -1;  		
	}

	public String getUserKeyHash() {
		return userKeyHash;
	}

	public void setUserKeyHash(String userKeyHash) {
		this.userKeyHash = userKeyHash;
	}

	public int getNodeListHash() {
		return nodeListHash;		
	}  
	
	public void setNodeListHash(int nodeListHash) {
		this.nodeListHash = nodeListHash;
	}
	
	
	
	
}
