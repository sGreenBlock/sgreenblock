import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;

public class P2PReceiver extends P2PSender {
	public P2PReceiver(Wallet wallet) {
		super(wallet);
	}

	@Override
	public boolean start() throws IOException {
		boolean returnSucceed = true;	
		
		P2PSender.LOGGER.config(String.format("Start receiving ... "));
		
		while(returnSucceed) {
		P2PSender.LOGGER.config(String.format("Waiting for offer ... "));
		P2PServer.P2P receivedCommand = receiveCommand();
		P2PSender.LOGGER.config(String.format("Offer received ... "));
		if(receivedCommand != null) {
			switch (receivedCommand) {
			case SEND_CONTRACT_HASHES:
				P2PSender.LOGGER.config(String.format("Contract Hashes offered ... "));
				comSendContractHashes();
				break;
			case MY_ADDRESS:
				P2PSender.LOGGER.config(String.format("Address offered ... "));
				comMyAddress();
				break;
			case SEND_NODES:
				P2PSender.LOGGER.config(String.format("Nodes Addresses offered ... "));
				comSendNodes();
				break;
			case SEND_CHAIN_HASH:
				P2PSender.LOGGER.config(String.format("Chain Hash offered ... "));
				receiveChainHash();
				break;
			case EXIT:
			default:
				returnSucceed = false;
				break;
			}
		}
		else returnSucceed = false;
		}
		
		return returnSucceed;
	}	
	
	private boolean comSendNodes() {
		boolean returnSucceed = true;
		P2PSender.LOGGER.config(String.format("Get Known Nodes"));
		if(sendMessage(P2PServer.P2P.GET_NODES.name())) {
			String receivedMessage = receiveMessage();
			if(receivedMessage != null) {
				P2PSender.LOGGER.config(String.format("Receiving Known Nodes: %s",receivedMessage));
				String[] knownNodes = receivedMessage.split(" ");
				for (int i = 0; i < knownNodes.length; i++) {
					String nodeAddress = knownNodes[i];
					LOGGER.fine(String.format("Received node: %s", nodeAddress));

					String nodeIpToConnect = P2PClient.ipFromString(nodeAddress);
					int nodePortToConnect = P2PClient.portFromString(nodeAddress);

					if (P2PClient.isNodeIP(nodeIpToConnect, nodePortToConnect)) {
						Wallet.addNode(nodeAddress, "n.a.");
					}
				}
			}
		}
		else returnSucceed = false;
		return returnSucceed;
	}
	
		
	private boolean comMyAddress() {
		boolean returnSucceed = true;
		String nodeIP = sock.getInetAddress().toString().substring(1);
		int nodePort = Integer.parseInt(receiveMessage());
		String nodeKeyHash = receiveMessage();
		String nodeAddress = nodeIP + ":" + nodePort;
		LOGGER.fine(String.format("Address of node: %s", nodeAddress));
		if (P2PClient.isNodeIP(nodeIP, nodePort)) {
			String addressToAdd = nodeIP + ":" + nodePort;
			LOGGER.fine(String.format("Add node %s of user %s", addressToAdd, nodeKeyHash));
			Wallet.addNode(addressToAdd, nodeKeyHash);
		}
		return returnSucceed;
	}
	
	private boolean comSendContractHashes() {
		boolean returnSucceed = true;	
		
		if(sendMessage(P2PServer.P2P.GET_CONTRACT_HASHES.name())) {
			String receivedMessage = receiveMessage();
			if(receivedMessage != null) {
				P2PSender.LOGGER.config(String.format("Receiving Contract Hashes: %s",receivedMessage));
				String[] contractHashes = receivedMessage.split(" ");
				
				LinkedHashMap<String, Contract> contractList = wallet.getBlockChain().getNotAcceptedContracts();
				for (int i = 0; i < contractHashes.length; i++) {
					P2PSender.LOGGER.config(String.format("Checking %s",contractHashes[i]));
					if (!contractList.containsKey(contractHashes[i])) {
						P2PSender.LOGGER.config(String.format("Requesting %s",contractHashes[i]));
						if(sendMessage(P2PServer.P2P.GET_CONTRACT.name())) {
							if(sendMessage(contractHashes[i])) {								
								Contract receivedContract = (Contract) receiveObject();
								if (receivedContract != null) {
									Wallet.writeContractToDisk(Wallet.P2P_FOLDER,
											receivedContract);
								}
							}
						}
					}
					
				}
			}
			else {
				returnSucceed = false;	
			}
		}
		else {
			returnSucceed = false;	
		}
		
		sendMessage(P2PServer.P2P.EXIT.name());
		
		return returnSucceed;
	}
	
	private boolean receiveChainHash() {
		boolean returnSucceed = true;	
		P2PSender.LOGGER.fine(String.format("Requesting Chain Hash ... "));
		if(sendMessage(P2PServer.P2P.GET_CHAIN_HASH.name())) {
			P2PSender.LOGGER.fine(String.format("Waiting for Chain Hash ... "));
			
				String receivedMessage = receiveMessage();
				if(receivedMessage != null) {
					String blockChainInfoString = receivedMessage;
					
					P2PSender.LOGGER.fine(String.format("Receiving Chain Hash ... "));
					String[] blockChainInfoArr = new String[0];
					
					if(blockChainInfoString != null) {
						blockChainInfoArr = blockChainInfoString.split(" ");
					}
					
					if(blockChainInfoArr.length == 3) {
					
						String blockChainHash = blockChainInfoArr[0];
						Integer blockChainLength = Integer.valueOf(blockChainInfoArr[1]);
						Long blockChainAge = Long.valueOf(blockChainInfoArr[2]);
						P2PSender.LOGGER.fine(String.format("Received Blockchain Hash 0x%s", blockChainHash));
						
						ArrayList<Block> blockChain = wallet.getBlockChain().getBlockChain();
						
						boolean isNew = false;
						if (blockChain.size() < blockChainLength) {
							isNew = true;
						}
						else if(blockChain.size() == blockChainLength) {
							if(blockChain.get(blockChain.size()-1).getTimeStamp() > blockChainAge) {
								isNew = true;
							}
						}
						
						if (isNew && !blockChainHash.equals(wallet.getBlockChain().calculateBlockChainHash())) {
							P2PSender.LOGGER.fine(String.format("New Blockchain, so requesting whole blockchain ... 0x%s",
									blockChainHash));
							
							if(sendMessage(P2PServer.P2P.GET_CHAIN.name())) {
								if(sendMessage(blockChainHash)) {
									P2PSender.LOGGER.fine(String.format("Receiving Blockchain 0x%s", blockChainHash));
									
									Object receivedObject = receiveObject();
									ArrayList<Block> receivedBockChain = null;
									if (receivedObject instanceof ArrayList) {
										receivedBockChain = (ArrayList<Block>) receivedObject;
									}
									if (receivedBockChain != null && receivedBockChain.size() > 0) {
										wallet.getBlockChain().writeChainToDisk(receivedBockChain, Wallet.P2P_FOLDER);
									}
									P2PSender.LOGGER.fine(String.format("%nReading Blockchain time: %d %n",(new Date().getTime()-start_time)/1000));
								}
							}
						}
						else if(!blockChainHash.equals(wallet.getBlockChain().calculateBlockChainHash())) {
							sendMessage(P2PServer.P2P.EXIT.name());
							// send my chain if newer
							P2PSingleton.instantiateClient(wallet, P2PSingleton.P2P.SHARE_CHAIN);
						}
					}
					else returnSucceed = false;									
					
				}
				else returnSucceed = false;	
			
		}
		else returnSucceed = false;			
		
		return returnSucceed;
	}
	
}
