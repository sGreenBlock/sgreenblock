import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

public abstract class P2PSender {
	
	Wallet wallet;
	Socket sock;
	long start_time;
	BufferedWriter bw;
	BufferedReader br;
	ObjectOutputStream oos;
	ObjectInputStream ois;
	String addr;
	
	public static final Logger LOGGER = Logger.getLogger(P2PSender.class.getName());
	
	public P2PSender(Wallet wallet) {
		this.wallet = wallet;
	}
	
	public boolean connect(Socket sock, String addr, long start_time) {
		boolean returnSucceed = true;
		this.sock = sock;
		this.start_time = start_time;
		this.addr = addr;
		
		OutputStream os = null;
		OutputStreamWriter osw = null;
		
		try {
			os = sock.getOutputStream();
			osw = new OutputStreamWriter(os);
			this.bw = new BufferedWriter(osw);
			oos = new ObjectOutputStream(sock.getOutputStream());
			
			InputStream is = sock.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			this.br = new BufferedReader(isr);
			ois = new ObjectInputStream(sock.getInputStream());			
			
			
		}
		
		catch(IOException e) {
			returnSucceed = false;
			LOGGER.warning(String.format("Could not create Streams: %s",e));
		}
		
		return returnSucceed;
	}
	
	public abstract boolean start() throws IOException;
	
	public boolean sendMyAddress() {
		boolean returnSucceed = true;
		LOGGER.config(String.format("Sending my server port %s ", Wallet.P2P_MY_PORT));
		if(!(
				sendMessage(P2PServer.P2P.MY_ADDRESS.name()) &&
				sendMessage(String.valueOf(Wallet.P2P_MY_PORT)) &&
				sendMessage(wallet.getUserKeyHash())
			)){		
			returnSucceed = false;
		}
		return returnSucceed;
	}
	
	public boolean sendKnownNodes() {		
		boolean returnSucceed = true;
		ConcurrentHashMap<String, P2PNodeInfo> nodesAvailable = Wallet.getNodesAvailable();
		if(!Wallet.hasNodeOthers(addr, nodesAvailable.hashCode())) {
			LOGGER.config(String.format("Sending available nodes list ... "));
			if(
					sendMessage(P2PServer.P2P.SEND_NODES.name()) &&
					receiveCommand() == P2PServer.P2P.GET_NODES
				) {
				
				String nodesString = "";
				for (Map.Entry<String, P2PNodeInfo> entry : nodesAvailable.entrySet()) {
					if (entry.getValue().calcConnectionRating() > 0) {
						LOGGER.fine(String.format("Sending node %s", entry.getKey()));
						nodesString = entry.getKey() + " ";
					}
				}
				if(!(	sendMessage(nodesString) 
					)) {
					returnSucceed = false;
				}
			} else returnSucceed = false;
			if(returnSucceed) {
				Wallet.setNodeOthersHash(addr, nodesAvailable.hashCode());
			}
		}		
		
		return returnSucceed;
	}
	
	public boolean sendMessage(String message) {
		boolean returnSucceed = true;
		LOGGER.config(String.format("Send message: %s",message));
		try {
			String sendMessage = message;
			bw.write(sendMessage.trim() + "\n");
			bw.flush();
		} catch (IOException e) {
			LOGGER.warning(String.format("Could not send message %s: %s",message, e));
			e.printStackTrace();
			returnSucceed = false;
		}
		return returnSucceed;
	}
	
	public Object receiveObject() {
		LOGGER.config(String.format("Receiving Object ... "));
		Object outputObject = null;
		try {			
			outputObject = ois.readObject();
		} catch (ClassNotFoundException e) {
			P2PSender.LOGGER.fine(String.format("Could not read Object: %s",e));
		} catch (IOException e) {
			P2PSender.LOGGER.fine(String.format("Could not read Object: %s",e));
		}
		
		return outputObject;
	}
	
	public boolean sendObject(Object object) {
		LOGGER.config(String.format("Sending Object ... "));
		boolean returnSucceed = true;
		try {
			
			oos.writeObject(object);
			oos.flush();
		} catch (IOException e) {
			P2PSender.LOGGER.fine(String.format("Could not send Object: %s",e));
			returnSucceed = false;
		}
		return returnSucceed;
	}
	
	public String receiveMessage() {
		LOGGER.config(String.format("Waiting for answer ... "));
		String receivedMessage = null;
		try {
			receivedMessage = br.readLine();
			LOGGER.config(String.format("Got answer: %s", receivedMessage));
		} catch (IOException e) {
			LOGGER.warning(String.format("Could not receive message: %s", e));
		}
		return receivedMessage;
	}
	
	public P2PServer.P2P receiveCommand(){
		LOGGER.config(String.format("Waiting for command ... "));
		P2PServer.P2P receivedCommand = null;
		String receivedMessage = receiveMessage();
		if(receivedMessage != null) {
			String[] receivedMessageArr = receivedMessage.split(" ");
			try {
				LOGGER.config(String.format("Got answer: %s", receivedMessageArr[0]));
				receivedCommand = P2PServer.P2P.valueOf(receivedMessageArr[0]);
			}
			catch(IllegalArgumentException e) {
				LOGGER.warning(String.format("Could not read command. Connection will be closed!"));
			}
		}		
		else {
			LOGGER.warning(String.format("No Data received!"));
			P2PData.shares_interrupted_n++;
		}
		return receivedCommand;
	}

	public Wallet getWallet() {
		return wallet;
	}

	public long getStart_time() {
		return start_time;
	}

	public BufferedWriter getBw() {
		return bw;
	}

	public BufferedReader getBr() {
		return br;
	}
	
	
		
}
