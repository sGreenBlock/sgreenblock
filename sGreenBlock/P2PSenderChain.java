import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;


public class P2PSenderChain extends P2PSender{

	public P2PSenderChain(Wallet wallet) {
		super(wallet);
	}

	@Override
	public boolean start() throws IOException {
		boolean returnSucceed = true;	
		
		sendMyAddress();
		
		sendKnownNodes();
		
		P2PSender.LOGGER.fine(String.format("Inform Sending Chain Hash ... "));
		if(sendMessage(P2PServer.P2P.SEND_CHAIN_HASH.name())) {	
			
			if(receiveCommand() == P2PServer.P2P.GET_CHAIN_HASH) {
				
				P2PSender.LOGGER.fine(String.format("Sending Chain Hash ... "));
				
				ArrayList<Block> blockChain = wallet.getBlockChain().getBlockChain();
				String blockChainHash = "";
				String blockChainHashMessage = "";
				if(blockChain != null && blockChain.size() > 0) {
					blockChainHash = wallet.getBlockChain().calculateBlockChainHash();
					blockChainHashMessage = blockChainHash+" "+blockChain.size()+" "+blockChain.get(blockChain.size()-1).getTimeStamp();
					P2PSender.LOGGER.fine(String.format("Sending Blockchain Hash ... %s", blockChainHash));				
				}
				
				if (blockChainHashMessage != null) {					
					if(!sendMessage(blockChainHashMessage)){
						returnSucceed = false;
					}
					if(receiveCommand() == P2PServer.P2P.GET_CHAIN) {
						String chainHash = receiveMessage();
						if(chainHash.equals(blockChainHash)) {
							sendObject(wallet.getBlockChain().getBlockChain());
						}
					}
				}	
			}
			else {
				returnSucceed = false;
				P2PSender.LOGGER.fine(String.format("Node did not want Chain Hash ... "));
			}
		}
		else returnSucceed = false;			
		
		return returnSucceed;
	}

}
