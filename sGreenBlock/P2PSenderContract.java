import java.io.IOException;
import java.net.Socket;
import java.util.LinkedHashMap;
import java.util.Map;


public class P2PSenderContract extends P2PSender{

	public P2PSenderContract(Wallet wallet) {
		super(wallet);
	}

	@Override
	public boolean start() throws IOException {
		boolean returnSucceed = true;	
		
		sendMyAddress();
		
		sendKnownNodes();
		
		if(sendMessage(P2PServer.P2P.SEND_CONTRACT_HASHES.name())) {
			
			P2PServer.P2P receivedCommand = receiveCommand();
			
			if(receivedCommand == P2PServer.P2P.GET_CONTRACT_HASHES) {
				P2PSender.LOGGER.fine(String.format("Sending Contract Hashes ... "));
				
				LinkedHashMap<String, Contract> contractList = wallet.getBlockChain().getNotAcceptedContracts();
				
				if (contractList.size() > 0) {
					String hashes = "";
					for (Map.Entry<String, Contract> entry : contractList.entrySet()) {
						hashes += entry.getValue().getHash() + " ";
						P2PSender.LOGGER.fine(String.format("Sending Contract Hash %s", entry.getValue().getHash()));
					}
					if(!sendMessage(hashes.trim())){
						returnSucceed = false;
					}
					while(receiveCommand() == P2PServer.P2P.GET_CONTRACT) {
						String contractHash = receiveMessage();
						if(contractHash != null) {
							sendObject(wallet.getBlockChain().getNotAcceptedContracts().get(contractHash));
						}
					}
				}					
			}
			else {
				returnSucceed = false;
			}
		}
		else {
			returnSucceed = false;
		}
		
		return returnSucceed;
	}

}
