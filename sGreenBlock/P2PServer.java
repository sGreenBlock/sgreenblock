import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class P2PServer {

	public enum P2P {
		MY_KEY_HASH, MY_ADDRESS, MY_SERVER_PORT, SEND_CHAIN_HASH, GET_CHAIN_HASH, GET_CHAIN, GET_CONTRACT_HASHES, SEND_CONTRACT_HASHES, GET_CONTRACT, SEND_NODES, GET_NODES, ALL_SENT,
		EXIT, OK;
	}

	private ServerSocket servsock;

	public final static int SOCKET_PORT = Wallet.P2P_MY_PORT;

	private volatile Wallet wallet;
	private static final Logger LOGGER = Logger.getLogger(P2PServer.class.getName());

	public P2PServer(Wallet wallet, ServerSocket servsock) {
		this.wallet = wallet;
		this.servsock = servsock;
	}

	public void start() {		
		boolean runningServer = true;
		LOGGER.fine(String.format("Starting Server on Port %s", SOCKET_PORT));
		while (runningServer && !Thread.interrupted()) {
			try {
				
				LOGGER.fine(String.format("Waiting for incoming connection on Port %s", SOCKET_PORT));
				
				while(Wallet.P2P_START_SERVER_N <= P2PData.server_running_n) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						LOGGER.fine(String.format("Sleep interrupted"));
					}
				}
				
				final Socket sock = servsock.accept();
				sock.setSendBufferSize( 128000 );
				sock.setReceiveBufferSize( 128000 );
				sock.setSoTimeout(Wallet.P2P_Socket_Timeout);

				LOGGER.fine(String.format("Incoming connection from: %s", sock.getRemoteSocketAddress()));
				Runnable runnerSocket = () -> { 
					try {
						P2PData.server_n++;P2PData.server_running_n++;
						long start_time = new Date().getTime();
						P2PSender receiver = new P2PReceiver(wallet);
						receiver.connect(sock, null, start_time);
						receiver.start();	
						P2PData.server_running_n--;
						
					} catch(IOException e) {
						LOGGER.fine(String.format("Interrupted: %s", e));
						P2PData.shares_interrupted_n++;
					} finally {
						LOGGER.fine(String.format("Closing sockets ..."));
						try {
							if(servsock.isBound()) {
								if (sock != null)
									sock.close();		
								if(servsock!=null) {
									servsock.setSoTimeout(0);
								}								
							}
							P2PData.server_n--;
							LOGGER.fine(String.format("Open Services: %d / %d, %d shares and %d requests interrupted", P2PData.server_running_n, P2PData.server_n, P2PData.shares_interrupted_n, P2PData.requests_interrupted_n));
						} catch (SocketException e) {
							LOGGER.warning(String.format("SocketException Error: %s", e));
						} catch (IOException e1) {
							LOGGER.warning(String.format("IO Error: %s", e1));
						}
					}
				};
				Thread thread = new Thread(runnerSocket);
				thread.setDaemon(true);
				thread.setName("socketThread");
				thread.start();
				LOGGER.config(String.format("P2P Server thread started"));

			} catch (SocketTimeoutException e) {
				e.printStackTrace();
				LOGGER.warning(String.format("%s", e));
			} catch (SocketException e) {
				LOGGER.warning(String.format("Socket Timeout. Details: %s", e));
			} catch (IOException e) {
				LOGGER.warning(String.format("IO Error: %s", e));
			}
			
		}
		
		if (servsock != null && servsock.isBound()) {
			try {
				LOGGER.warning(String.format("Closing socket on %s of Thread: %s", Wallet.P2P_MY_PORT,
						Thread.currentThread().getId()));
				servsock.close();
			} catch (IOException e) {
				LOGGER.warning(String.format("IO Error: %s", e));
			}
		}
		
	}

	public ServerSocket getServsock() {
		return servsock;
	}

}
