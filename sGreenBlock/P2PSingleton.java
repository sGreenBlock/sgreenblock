import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Logger;

/**
 * Keeps care only one P2P Server and Client Thread exists
 * 
 * @author ststolz
 *
 */
public class P2PSingleton {
	
	public enum P2P {
		SHARE_CONTRACTS, SHARE_CHAIN;
	}
	
	private static ArrayList<Thread> serverThreads = new ArrayList<>();
	private static ArrayList<Thread> clientThreads = new ArrayList<>();
	private static ServerSocket serverSocket;
	
	private static Logger LOGGER = Logger.getLogger(P2PSingleton.class.getName());    	
	
	/**
	 * If old threads exist, sockets are closed and threads are interrupted, 
	 * before new ones are created.
	 * 
	 * @param wallet
	 */
	public static void instantiate(Wallet wallet) {		
		instantiateServer(wallet);
	}
	
	private static void instantiateServer(Wallet wallet) {
		
		if(serverThreads.size() > 0) {
			LOGGER.config(String.format("Interrupting P2P Server Threads"));
			try {
				serverSocket.close();
			} catch (IOException e1) {
				LOGGER.warning(String.format("Could not close P2P Server Socket"));
			}
			for (Thread thread : serverThreads) {
				thread.interrupt();
			}
			try {
				for (Thread thread : serverThreads) {
					thread.join();
				}				
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		LOGGER.config(String.format("Create new socket on Port %s", Wallet.P2P_MY_PORT));		
		try {
			LOGGER.config(String.format("Opening P2P socket"));
			serverSocket = new ServerSocket(Wallet.P2P_MY_PORT);
			
			Runnable runnerSocket = () -> { 					
				P2PServer p2pServer = new P2PServer(wallet,serverSocket);
				p2pServer.start();
			};
			Thread thread = new Thread(runnerSocket);
			thread.setDaemon(true);
			thread.setName("socketThread");
			thread.start();
			LOGGER.config(String.format("P2P Server thread started"));
			serverThreads.add(thread);
			
			
		} catch (IOException e) {
			LOGGER.severe(String.format("IO Error: %s", e));
		} 
		
	}
	
	public static void instantiateClient(Wallet wallet, P2P command) {
		P2PSender sender = null;
		long start_time = new Date().getTime();
		switch (command) {
		case SHARE_CONTRACTS:
			sender = new P2PSenderContract(wallet);
			break;
			
		case SHARE_CHAIN:
			sender = new P2PSenderChain(wallet);
			break;

		default:
			break;
		}
		
		final P2PSender finalSender = sender;
		
		Runnable runnerClient = () -> { 
			LOGGER.config(String.format("Starting P2P Client thread"));			
			P2PClient p2pClient = new P2PClient(wallet);
			p2pClient.start(finalSender);
		};
		Thread clientThread = new Thread(runnerClient);
		clientThread.setDaemon(true);
		clientThread.setName("clientThread");
		clientThread.start();
		//clientThreads.add(clientThread);
		
	}

	public static ArrayList<Thread> getServerThreadInstances(Wallet wallet) {
		if(serverThreads == null || serverSocket == null) {
			instantiateServer(wallet);
		}
		return serverThreads;
	}

	public static ServerSocket getServerSocketInstance(Wallet wallet) {
		if(serverThreads == null || serverSocket == null) {
			instantiateServer(wallet);
		}
		return serverSocket;
	}
	
	
	
}
