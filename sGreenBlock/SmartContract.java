import greenfoot.*;  

import java.security.PublicKey;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;

public abstract class SmartContract extends Contract
{
    private static final Logger LOGGER = Logger.getLogger(Transaction.class.getName());
    
    public SmartContract(PublicKey from, PublicKey to, int approversN){
        super(from, to, approversN);
        
    }    
    
    @Override
    public String calculateHash(){
        String strToHash = "";
        if(this.getSenderPubKey() != null) {
        	strToHash += AsymKrypt.getStringFromKey(this.getSenderPubKey());
        }        
        
        strToHash += Long.toString(this.getTimeStamp());
                
        if(this.getRecipientPubKey() != null) {
        	strToHash += AsymKrypt.getStringFromKey(this.getRecipientPubKey());    
        }
        
        //strToHash += sequence;
        String returnHash = Hash_it.getHash(strToHash);
        LOGGER.config(String.format("Calculating Hash: %s",returnHash));
        return returnHash;        
    }
    
    public ArrayList<TransactionInput> gatherUTXOs(BlockChain blockChain, float value){
    	ArrayList<TransactionInput> txis = new ArrayList<TransactionInput>();
    	for(Map.Entry<String,TransactionOutput> entry : getUTXOs(blockChain).entrySet()){
    		TransactionInput txi = new TransactionInput(entry.getValue().id);
    		txi.transactionOutput = entry.getValue();
    		txis.add(txi);
    		value -= entry.getValue().value;    		
    		if(value <= 0) {
    			break;
    		}
    	}
    	return txis;
    }
    
	public HashMap<String,TransactionOutput> getUTXOs(BlockChain blockChain) {
		LOGGER.config(String.format("Getting UTXOs for Smart Contract 0x.6s",this.getHash()));
		Block block = null;
		HashMap<String,Contract> contractList = null;
		float valueSum = 0;
		HashMap<String,TransactionOutput> txos = new HashMap<String,TransactionOutput>();
		// Searching if ConfirmTransaction is existing
		ArrayList<Block> blockChainList = blockChain.getBlockChain();
		for (int i = 0; i < blockChainList.size(); i++) {
			block = blockChainList.get(i);			
			contractList = block.getContractList();
			for(Map.Entry<String,Contract> entry : contractList.entrySet()){	
				Contract contract = entry.getValue();
				if(contract.getToTransactionHash() != null) {
					if(contract instanceof Transaction) {
						Transaction transaction = (Transaction) contract;
						if(transaction.getToTransactionHash().equals(this.getHash())) {
							for(TransactionOutput txo : transaction.getTransactionOutputs()) {
								if(txo.recipientHash != null) {
									if(txo.recipientHash.equals(this.getHash())) {								
										LOGGER.fine(String.format("%-15s: %s","Adding UTXO: ",txo.id));
						    			txos.put(txo.id, txo);
									}	
								}
														
							}
						}
					}					
				}
			}
		}
		for (int i = 0; i < blockChainList.size(); i++) {
			block = blockChainList.get(i);			
			contractList = block.getContractList();
			for(Map.Entry<String,Contract> entry : contractList.entrySet()){	
				Contract contract = entry.getValue();
				if(contract.getFromTransactionHash() != null) {
					if(contract instanceof Transaction) {
						Transaction transaction = (Transaction) contract;
						if(transaction.getFromTransactionHash().equals(this.getHash())) {						
							for(TransactionInput txi : transaction.getTransactionInputs()) {
								if(txos.get(txi.transactionOutput.id) != null) {								
									LOGGER.fine(String.format("%-15s: %s","Removing UTXO: ",txi.transactionOutput.id));
									txos.remove(txi.transactionOutput.id);					    			
								}							
							}
						}
					}					
				}
			}
		}
		return txos;
	}
	
	public float getSCBalance(BlockChain blockChain) {
		HashMap<String,TransactionOutput> utxos = getUTXOs(blockChain);

		float valueSum = 0;
		
		for(Map.Entry<String, TransactionOutput> entry : utxos.entrySet()) {
			valueSum += entry.getValue().value;
		}
		
		return valueSum;
	}
	
	public boolean newSmartTransaction(BlockChain blockChain, PublicKey toKey, float value, float fee) {

    	boolean allOk = true;    	
    	
    	ArrayList<TransactionInput> inputs = this.gatherUTXOs(blockChain, value+fee);
    	ArrayList<TransactionOutput> outputs = new ArrayList<TransactionOutput>();
    	
    	float inputsSum = 0;
		if(inputs != null){
	        
			for(TransactionInput txi : inputs) {
				inputsSum += txi.transactionOutput.value;
			}
			//check if transaction is valid:
			if(inputsSum < Wallet.MINIMUM_TRANSACTION) {
				LOGGER.config(String.format("#Transaction Inputs to small: " + inputsSum));
				return false;
			}
		}    	
		else if(inputs == null){
    		LOGGER.config(String.format("To less UTXOs. Process aborted."));
    		allOk = false;
    	}
    	
    	/*
		 * generate transaction outputs
		 */
		float leftOver = (inputsSum > value) ? inputsSum - (value+fee) : 0; //get value of inputs then the left over change:
		LOGGER.config(String.format("Leftover: " + leftOver));
		
		if(toKey != null) {
			TransactionOutput to1 = new TransactionOutput(toKey, value);
			outputs.add(to1); //send value to recipient
			LOGGER.config(String.format("Adding Output %.6s for user %.6s with value %.2f", to1.id,Hash_it.getHash(to1.reciepient),value));
		}
		else {
			LOGGER.config(String.format("No Recipient. Aborting"));
    		allOk = false;
		}
		
		if(leftOver > 0){
			TransactionOutput to2 = new TransactionOutput(this.getToTransactionHash(), leftOver);
			outputs.add(to2); //send the left over 'change' back to sender
			LOGGER.config(String.format("Adding LeftOver Output %.6s for user %.6s with value %.2f", to2.id,Hash_it.getHash(to2.reciepient),leftOver));
		}
    	
    	if(toKey != null && allOk){
    		Transaction transaction = new Transaction(this.getHash(), toKey, value, fee, inputs, outputs);   		
    		transaction.setHash(transaction.calculateHash());            
            
    		if(allOk){
    			LOGGER.config(String.format("Transaction processed, adding Transaction %.6s to BlockChain",transaction.getHash()));
    			blockChain.newTransaction(transaction);
    		}
    	}
    	else{
    		LOGGER.config(String.format("Transaction not gerenated. Process aborted."));
    		allOk = false;
    	}
    	return allOk;
	}

	@Override
	public boolean locked(BlockChain blockChain) {
		return false;	
	}
	
	@Override
	public String toString() {
		String returnString = "";
		returnString = super.toString();
		return returnString;
	}

	public abstract boolean scriptToExecute(BlockChain blockChain);
	
	public abstract boolean checkContract(BlockChain blockChain);
}
