import greenfoot.*;  
import java.security.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;


public class Transaction extends Contract
{
    
        
    private float value;        
	
    private ArrayList<TransactionInput> transactionInputs = new ArrayList<TransactionInput>();
    private ArrayList<TransactionOutput> transactionOutputs = new ArrayList<TransactionOutput>();
	
	//private static int sequence = 0;
	
	private static final Logger LOGGER = Logger.getLogger(Transaction.class.getName());
	
	public Transaction(PublicKey from, PublicKey to, float value, float fee, ArrayList<TransactionInput> inputs){
        super(from, to);
                
		this.value = value;
		this.transactionInputs = inputs;			
		
    }
	
	public Transaction(String fromTransactionHash, PublicKey to, float value, float fee, ArrayList<TransactionInput> inputs,  ArrayList<TransactionOutput> outputs){
        super(fromTransactionHash, to);
                
		this.value = value;
		this.transactionInputs = inputs;
		this.transactionOutputs = outputs;
		
    }
	
	public Transaction(PublicKey from, String toTransactionHash, float value, float fee, ArrayList<TransactionInput> inputs){
        super(from, toTransactionHash);
                
		this.value = value;
		this.transactionInputs = inputs;
		
    }
    
    public Transaction(PublicKey from, PublicKey to, int approversN, float value, float fee, ArrayList<TransactionInput> inputs){
        super(from, to, approversN);
                
		this.value = value;
		this.transactionInputs = inputs;
		
    }    
    
	public void act() 
    {
        // Add your action code here.
    }    
	
    @Override
    public String calculateHash(){
        String strToHash = "";
        if(this.getSenderPubKey() != null) {
        	strToHash += AsymKrypt.getStringFromKey(this.getSenderPubKey());
        }        
        
        strToHash += Long.toString(this.getTimeStamp());
        if(this.transactionInputs != null){
        	strToHash += this.transactionInputs.hashCode();
        }        
        
        if(this.getRecipientPubKey() != null) {
        	strToHash += AsymKrypt.getStringFromKey(this.getRecipientPubKey());    
        }
        strToHash += this.transactionOutputs.hashCode();
        strToHash += Float.toString(value);
        //strToHash += sequence;
        String returnHash = Hash_it.getHash(strToHash);
        LOGGER.finer(String.format("Calculating Hash: %s",returnHash));
        return returnHash;        
    }
    
    
    public boolean processTransaction(float fee) {
        		
		if(transactionInputs != null){
	        //gather transaction inputs (Make sure they are unspent):
			for(TransactionInput input : transactionInputs) {
				LOGGER.finer(String.format("%-15s: %s","Processing Input",input.transactionOutputId));
				input.transactionOutput = BlockChain.UTXOs.get(input.transactionOutputId).txo;
				if(input.transactionOutput == null){
					LOGGER.warning(String.format("%-15s: %s","Did not find Output",input.transactionOutputId));
				}
			}
			
			//check if transaction is valid:
			if(getInputsValue() < Wallet.MINIMUM_TRANSACTION) {
				LOGGER.finer(String.format("#Transaction Inputs to small: " + getInputsValue()));
				return false;
			}			
		}
		/*
		 * generate transaction outputs
		 */
		float leftOver = (getInputsValue()>value)?getInputsValue() - (value+fee):0; //get value of inputs then the left over change:
		LOGGER.finer(String.format("Leftover: " + leftOver));
		
		if(this.getRecipientPubKey() != null) {
			TransactionOutput to1 = new TransactionOutput( this.getRecipientPubKey(), value);
			transactionOutputs.add(to1); //send value to recipient
			LOGGER.finer(String.format("Adding Output %.6s for user %.6s with value %.2f", to1.id,Hash_it.getHash(to1.reciepient.getEncoded()),value));
		}
		else if(this.getToTransactionHash() != null) {
			TransactionOutput to1 = new TransactionOutput( this.getToTransactionHash(), value);
			transactionOutputs.add(to1); //send value to recipient
			LOGGER.finer(String.format("Adding Output %.6s for Smart Contract %.6s with value %.2f", to1.id,this.getToTransactionHash(),value));
		}
		
		if(leftOver>0){
			TransactionOutput to2 = new TransactionOutput( this.getSenderPubKey(), leftOver);
			transactionOutputs.add(to2); //send the left over 'change' back to sender
			LOGGER.finer(String.format("Adding LeftOver Output %.6s for user %.6s with value %.2f", to2.id,Hash_it.getHash(to2.reciepient.getEncoded()),leftOver));
		}
		
		// no UTXO Management here, because it happens in BlockChain Class
		
		this.setHash(this.calculateHash());
		LOGGER.finer(String.format("Calculated Hash with Sender: %s", this.getHash()));
		
		return true;
    }
    
    private float getInputsValue() {
		float total = 0;
		if(transactionInputs != null){
			for(TransactionInput i : transactionInputs) {
				if(i.transactionOutput == null){
					LOGGER.finer(String.format("%-15s: %s","Input not valid",i.transactionOutputId));
					continue; //if Transaction can't be found skip it 
				}
				total += i.transactionOutput.value;
			}
		}		
		return total;
	}
	
	public float getOutputsValue() {
		float total = 0;
		for(TransactionOutput o : transactionOutputs) {
			total += o.value;
		}
		return total;
	}
	
	@Override
	public boolean checkContract(BlockChain blockChain) {		
		
		LOGGER.fine(String.format("Checking Contract 0x%.6s",this.getHash()));
		
		boolean returnIsValid = true;
		boolean isSigned = false;
		boolean error = false;
		
		if(returnIsValid) {
		
			LinkedHashMap<String,Contract> allNotAcceptedContracts = blockChain.getAllNotAcceptedContracts();
			LOGGER.finer(String.format("%-15s: %s","Checking Transaction",this.getHash()));
			
			/*
			 * check if signed with correct private key
			 */
			if(this.getSenderPubKey() != null && verifiySignature(this.getNumberApproversNeeded()-1) == false) {
				LOGGER.fine(String.format("Transaction %.6s Signature failed to verify. Result: %s",this.getHash(),verifiySignature(this.getNumberApproversNeeded())));
				returnIsValid = false;
			}
			else if(this.getSenderPubKey() == null) {
				isSigned = false;
			}
			else {
				isSigned = true;
			}
			if(returnIsValid){
			
				if(transactionInputs != null){
					LOGGER.finer(String.format("Found %s inputs",transactionInputs.size()));
					int counter = 0;
					for(TransactionInput input : transactionInputs) {
						counter++;
						LOGGER.finer(String.format("%-15s: %s","Processing TxInput",counter));
						LOGGER.finer(String.format("%-15s: 0x%s","Searching TransactionOutput",input.transactionOutputId));
						int counter2 = 0;
						int foundCounter = 0;
						boolean isExisting = false;
						
						ArrayList<Block> blockChainList = blockChain.getBlockChain();				
						
						for(int i = blockChainList.size()-1; i >= 0; i--){
							counter2++;						
							
							for(Map.Entry<String, Contract> entry : blockChainList.get(i).getContractList().entrySet()) {							
								Contract contract = entry.getValue();
								boolean isThis = false;
								if(contract instanceof Transaction){								
									//Contract contract = block.getContractList().get(input.transactionOutput.id);
									Transaction transaction = (Transaction) contract;
									if(transaction.getHash().equals(this.getHash())){
										this.setDepth(counter2);
										isThis = true;
										foundCounter++;
										LOGGER.finer(String.format("Found this Transaction 0x%.6s",transaction.getHash()));
									}
									if(!isThis && transaction.transactionInputs != null) {
										for(TransactionInput txi : transaction.transactionInputs) {
											if(txi.transactionOutputId.equals(input.transactionOutputId)) {
												LOGGER.warning(String.format("Invalid Transaction! Found TransactionInput with same txo id 0x%.6s in Transaction 0x%.6s of Block 0x%s",txi.transactionOutputId, transaction.getHash(),counter2));
												returnIsValid = false;
												error = true;
											}
										}
									}									
									if(!isThis && returnIsValid) {
										for(TransactionOutput txo : transaction.transactionOutputs){
											if(txo.id.equals(input.transactionOutput.id)){
												isExisting = true;
												LOGGER.finer(String.format("Found TransactionOutput 0x%.6s in Transaction 0x%.6s of Block 0x%s",input.transactionOutput.id, transaction.getHash(),counter2));
												String txoRecipient = null;
												String thisSender = null;
												if(this.getSenderPubKey() != null && txo.reciepient != null) {
													txoRecipient = Hash_it.getHash(txo.reciepient);
													thisSender = Hash_it.getHash(this.getSenderPubKey());												
												}
												else if (txo.recipientHash != null && this.getFromTransactionHash() != null) {
													LOGGER.finer(String.format("Transaction 0x%.6s of Transaction 0x%s is from Smart Contract",input.transactionOutput.id, transaction.getHash(),counter2));
													txoRecipient = txo.recipientHash;
													thisSender = this.getFromTransactionHash();	
												}
												else {
													LOGGER.finer(String.format("Transaction 0x%.6s of Block 0x%s is missing Recipient information",input.transactionOutput.id, transaction.getHash(),counter2));
													returnIsValid = false;
													error = true;
												}
												
												if(txoRecipient != null && thisSender != null) {
													if(!txoRecipient.equals(thisSender)){
														LOGGER.warning(String.format("TransactionOutput 0x%.6s in Transaction 0x%.6s has wrong recipient 0x%.6s for sender 0x%.6s!",input.transactionOutput.id, transaction.getHash(), txoRecipient, thisSender));
														returnIsValid = false;
														error = true;
													}
												}
												else {
													LOGGER.warning(String.format("TransactionOutput 0x%.6s in Transaction 0x%.6s has invalid sender %s or recipient %s!",input.transactionOutput.id, transaction.getHash(), txoRecipient, thisSender));
													returnIsValid = false;
												}								
												
												if(transaction.locked(blockChain)) {
													LOGGER.warning(String.format("TransactionOutput 0x%.6s in Transaction 0x%.6s is locked!",input.transactionOutput.id, transaction.getHash()));
													returnIsValid = false;
												}
												else{		
													LOGGER.finer(String.format("Diving into Transaction 0x%.6s to check txo: ",transaction.getHash(),input.transactionOutput.id));
													returnIsValid = transaction.checkContract(blockChain);
												}										
											}
										}										
									}									
								}
							}							
						}
						if(!isExisting) {
							LOGGER.warning(String.format("TransactionOutput 0x%.6s not found!",input.transactionOutput.id));
							returnIsValid = false;
						}
						if(this.getDepth() == 0 && foundCounter > 0) {
							LOGGER.warning(String.format("Invalid! New TransactionOutput 0x%.6s was already found in Chain",input.transactionOutput.id,this.getDepth()));
							returnIsValid = false;
							error = true;
						}
						else if(foundCounter > 1) {
							LOGGER.warning(String.format("Invalid! TransactionOutput 0x%.6s was found more than two times in Chain",input.transactionOutput.id,this.getDepth()));
							returnIsValid = false;
							error = true;
						}
						LOGGER.finer(String.format("TransactionOutput 0x%.6s has a depth of %d",input.transactionOutput.id,this.getDepth()));
						if(!error && !returnIsValid && this.getDepth() == 0){
							boolean isOk = false;
							LOGGER.warning(String.format("TransactionOutput 0x%.6s not included in any Block.",input.transactionOutput.id));
							for(Map.Entry<String, Contract> entry : allNotAcceptedContracts.entrySet()) {
								Contract contract = entry.getValue();
								if(contract instanceof Transaction){	
									Transaction transaction = (Transaction) contract;
									for(TransactionOutput txo : transaction.transactionOutputs){
										if(txo.id.equals(input.transactionOutput.id)){
											LOGGER.finer(String.format("Found TransactionOutput 0x%.6s in not accepted Transaction %s",input.transactionOutput.id,transaction.getHash()));
											returnIsValid = transaction.checkContract(blockChain);
											isOk = true;
										}
									}
								}
							}
							if(!isOk) {
								LOGGER.warning(String.format("Could not find TransactionOutput 0x%.6s in not accepted Transactions",input.transactionOutput.id));
								returnIsValid = false;
							}
						}
						else if(!returnIsValid){
							LOGGER.warning(String.format("TransactionOutput 0x%.6s not valid",input.transactionOutput.id));
						}
					}
				}
				else{
					LOGGER.fine(String.format("No more Inputs. Checking if reward Block..."));
					int counter = 0;
					ArrayList<Block> blockList = blockChain.getBlockChain();
					boolean isFoundInBlockChain = false;
					for(int i = blockList.size()-1; i >= 0; i--){
						counter++;
						Contract contract = blockList.get(i).getContractList().get(this.getHash());
						if(contract != null){
							LOGGER.finer(String.format("Found Transaction %.6s in Block %s",this.getHash(),counter));
							Iterator<Entry<String,Contract>> iterator = blockList.get(i).getContractList().entrySet().iterator();
							Entry<String,Contract> lastElement = null;
							LOGGER.finer(String.format("Walking to last Element of Block %.6s and calculating txo overheads",counter));
							float txiOverhead = .0F;
							while (iterator.hasNext()) { 
								lastElement = iterator.next(); 
								if(lastElement instanceof Transaction) {
									Transaction thisTransaction = (Transaction) lastElement;
									for(TransactionInput txi : thisTransaction.transactionInputs) {
										txiOverhead += txi.transactionOutput.value;
									}
									for(TransactionOutput txo : thisTransaction.transactionOutputs) {
										txiOverhead -= txo.value;
									}
								}
							}
							LOGGER.finer(String.format("Block %.6s has %f txi overheads",counter,txiOverhead));
							if(lastElement.getValue().getHash().equals(this.getHash())){
								LOGGER.finer(String.format("Transaction %.6s in Block %s is last Element! Checking value %f...",this.getHash(),counter,this.value));
								float allowedValue = txiOverhead + Wallet.MINE_REWARD;
								LOGGER.finer(String.format("%f overhead from txi + mine Reward %f = %f",txiOverhead,Wallet.MINE_REWARD,allowedValue));
								if(this.value == allowedValue) {
									LOGGER.finer(String.format("Value of transaction ok"));
								}
								else {
									LOGGER.warning(String.format("Value check failed"));
									returnIsValid = false;
								}
								this.setDepth(counter);
								isFoundInBlockChain = true;
							}
							else{
								LOGGER.warning(String.format("Transaction %.6s in Block %s is not last Element!",this.getHash(),counter));
								returnIsValid = false;
							}
						}
					}
					if(!isFoundInBlockChain) {
						LOGGER.warning(String.format("Transaction %.6s has no inputs and is not Reward Transaction!",this.getHash()));
						returnIsValid = false;
					}
					if(!returnIsValid){
						LOGGER.warning(String.format("Transaction %.6s has no inputs and is not Reward Transaction!",this.getHash(),counter));
					}
				}
				
			}
		}
		
		if(error) {
			returnIsValid = false;
		}
		
		return returnIsValid;
	}
	@Override
	public String toString(){
		String returnString = "";
		returnString += String.format("%n# Transaction: 0x%s%n", this.getHash());
		String fromHash = null;
		if(this.getFromTransactionHash() != null) {
			fromHash = this.getFromTransactionHash();
		}
		if(this.getSenderPubKey() != null) {
			fromHash = Hash_it.getHash(this.getSenderPubKey());
		}
		if(fromHash != null) {
			returnString += String.format("%n* From: 0x%s", fromHash);
		}
		if(this.getToTransactionHash() != null) {
			fromHash = this.getToTransactionHash();
		}
		if(this.getRecipientPubKey() != null) {
			fromHash = Hash_it.getHash(this.getRecipientPubKey());
		}
		if(fromHash != null) {
			returnString += String.format("%n* To: 0x%s", fromHash);
		}
		returnString += String.format("%n* Depth in Chain: %s", this.getDepth());		
		returnString += String.format("%n* Value: %s", this.value);
		returnString += String.format("%n");
		returnString += String.format("%n## Inputs: %n");
		if(transactionInputs != null){
			for(TransactionInput input : transactionInputs){ 
				String toString = "null";
				if(input.transactionOutput.reciepient != null) {
					toString = Hash_it.getHash(input.transactionOutput.reciepient);
				}
				if(input.transactionOutput.recipientHash != null) {
					toString = input.transactionOutput.recipientHash;
				}
				returnString += String.format("%n* 0x%.6s, Recipient 0x%.6s, Value %.2f", input.transactionOutput.id, toString,input.transactionOutput.value);
			}
			
		}
		else{
			returnString += String.format("has no Inputs");
		}		
		returnString += String.format("%n");
		returnString += String.format("%n## Outputs: %n");	
		if(transactionOutputs.size() > 0) {
			for(TransactionOutput output : transactionOutputs){ 
				String recipientString = "null";
				if(output.reciepient != null)
				{
					recipientString = Hash_it.getHash(output.reciepient);
				}
				else {
					recipientString = output.recipientHash;
				}
				returnString += String.format("%n* 0x%.6s, Recipient 0x%.6s, Value %.2f", output.id, recipientString,output.value);
			}
		}	
		else{
			returnString += String.format("has no Outputs");
		}
		returnString += String.format("%n");
		return returnString;
	}

	@Override
	public boolean locked(BlockChain blockChain) {
		return false;
	}

	public ArrayList<TransactionInput> getTransactionInputs() {
		return transactionInputs;
	}

	public ArrayList<TransactionOutput> getTransactionOutputs() {
		return transactionOutputs;
	}
	
	

	public void setTransactionOutputs(ArrayList<TransactionOutput> transactionOutput) {
		this.transactionOutputs = transactionOutput;
	}

	public float getValue() {
		return value;
	}
	
}
