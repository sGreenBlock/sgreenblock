import java.io.Serializable;

/**
 * Write a description of class TransactionInput here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class TransactionInput implements Serializable
{
    public String transactionOutputId; //Reference to TransactionOutputs -> transactionId
    public TransactionOutput transactionOutput; //Contains the Unspent transaction output
    
    public TransactionInput(String transactionOutputId) {
		this.transactionOutputId = transactionOutputId;
	}

    
}
