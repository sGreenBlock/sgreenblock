import java.security.PublicKey;
import java.util.ArrayList;

import greenfoot.*;  


public class TransactionMultiSig extends Transaction
{
    public TransactionMultiSig(PublicKey from, PublicKey to, int approversN, float value, float fee, ArrayList<TransactionInput> inputs) {
		super(from, to, approversN, value, fee, inputs);
	}
    
	@Override
	public boolean processTransaction(float fee) {
		boolean returnIfSuccess = false;
		returnIfSuccess = super.processTransaction(fee);
		for(int i = 0; i < getNumberApproverPubKeys(); i++) {
			if(this.getApproverHash(i) == null) {
				this.setApproverHash(this.calculateApproverHash(i), i);
			}			
		}
		return returnIfSuccess;
	}
    
}
