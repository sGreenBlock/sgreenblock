import java.io.Serializable;
import java.security.*;
import java.util.Date;

public class TransactionOutput implements Serializable
{
  
    public String id;
	public PublicKey reciepient; // human owner of these coins.
	public String recipientHash; // Smart Contract owner of these coins
	public float value; //the amount of coins
	private long timeStamp;
	
		
	//Constructor
	public TransactionOutput(PublicKey reciepient, float value) {
		this.reciepient = reciepient;
		this.value = value;
		this.timeStamp = new Date().getTime();
		this.id = Hash_it.getHash(AsymKrypt.getStringFromKey(reciepient)+Float.toString(value)+Long.toString(timeStamp));
	}
	
	public TransactionOutput(String recipientHash, float value) {
		this.recipientHash = recipientHash;
		this.value = value;
		this.timeStamp = new Date().getTime();
		this.id = Hash_it.getHash(recipientHash+Float.toString(value)+Long.toString(timeStamp));
	}
	
	//Check if coin belongs to you
	public boolean isMine(PublicKey publicKey) {
		return (publicKey == reciepient);
	}

    
}
