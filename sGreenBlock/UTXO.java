import java.io.Serializable;
import java.security.PublicKey;

public class UTXO implements Serializable {
	public String txid; //the id of the transaction this output was created in
	public TransactionOutput txo;
	public float value; //the amount of coins they own
	public PublicKey reciepient; //also known as the new owner of these coins.
	long timeStamp;
	
	public UTXO(PublicKey reciepient, float value, String txid, TransactionOutput txo, long timeStamp) {
		this.reciepient = reciepient;
		this.value = value;
		this.txid = txid;
		this.txo = txo;
		this.timeStamp = timeStamp;
	}
	
	
}
