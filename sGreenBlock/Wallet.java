import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Wallet here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Wallet 
{
	public transient static ConcurrentHashMap<String,P2PNodeInfo> nodesAvailable = new ConcurrentHashMap<String,P2PNodeInfo>();
	
	public static final int P2P_MY_PORT = 43210;
	public static final int P2P_POLL_NODES_N = 5;
	public static final int P2P_START_SERVER_N = 2;
	public static final int P2P_POLL_INTERVAL_S = 1;
	public static final int P2P_HOLD_AVAILABLE_NODES_N = 5;
	public static final int P2P_Socket_Timeout = 5000;
	
	public static final float INSET_GUESS_BLOCKCHAIN_SIZE = 10.F;
	public static final int MINIMUM_TRANSACTION = 1;
	public static final int MIN_DEPTH_CONTRACT = 4;
	public static final int USER_HASH_LENGTH = 6;
	public static final float MINE_REWARD = 50.F;
	public static final int STARTING_DIFFICULTY = 80; //80
	public static final int DEPTH_FOR_AVERAGE_MINING_SPEED = 5;
	public static final int AVERAGE_MINING_TIME_S = 60;	
	
    public static final String P2P_FOLDER = "blockchain";
    public static final String LOCAL_STORAGE = "local_data";
    public static final String FILE_NODE_IPS = "ips.snodes";
    public static final String FILEENDING_BLOCKCHAIN = ".schain";
    public static final String FILEENDING_CONTRACT = ".scontract";
    public static final String FILEENDING_UTXOS = ".sutxos";
    private final static ReentrantLock lock = new ReentrantLock();
    private transient AsymKrypt walletKey;
    
    private static LinkedHashMap<String,Contract> localContracts;

	private String userName;
    private volatile BlockChain blockChain;
    
    private static final Logger LOGGER = Logger.getLogger(Wallet.class.getName());
    
    public Wallet(String username) {
    	
    	localContracts = new LinkedHashMap<String,Contract>();
    	
    	if(username == null) {
    		this.userName = Hash_it.getHash(Long.toString(new Date().getTime()));
    	}
    	else {
    		this.userName = username;
    	}    	
    	
        walletKey = new AsymKrypt(this.userName, true);

        this.blockChain = new BlockChain(this, lock);
        this.blockChain.updateBlockChain(); 
        
        this.startP2P();
        
    }  
    
    public static ConcurrentHashMap<String, P2PNodeInfo> getNodesAvailable() {
		return nodesAvailable;
	}

	public static void addNode(String addr, String userKeyHash) {
    	if(!nodesAvailable.containsKey(addr)) {
    		nodesAvailable.put(addr, new P2PNodeInfo(addr, userKeyHash));
    	}
    	else if(userKeyHash != null) {
    		nodesAvailable.get(addr).setUserKeyHash(userKeyHash);
    	}
    }
	
	public static void addNodeAvailable(String addr) {
		P2PNodeInfo node = nodesAvailable.get(addr);
		if(node != null)
		{
			node.storeConnectInfo(true);
		}
    }
	
	public static boolean hasNodeOthers(String addr, int nodeListHash) {
		boolean hasHash=false;
    	P2PNodeInfo node = nodesAvailable.get(addr);
    	if(node != null)
    	{
    		int hash = node.getNodeListHash();
    		if(hash == nodeListHash) {
    			hasHash = true;
    		}
    	}
    	return hasHash;
    }
	
	public static boolean setNodeOthersHash(String addr, int nodeListHash) {
		boolean hasHash=false;
    	P2PNodeInfo node = nodesAvailable.get(addr);
    	if(node != null)
    	{
    		node.setNodeListHash(nodeListHash);
    	}
    	return hasHash;
    }
    
    public static void removeNodeAvailable(String addr) {
    	P2PNodeInfo node = nodesAvailable.get(addr);
    	if(node != null)
    	{
    		node.storeConnectInfo(false);
    	}
    }
    
    public static int calcKnownNodes() {     	
    	return nodesAvailable.size();
    }
    
    public static int calcAvailableNodes() { 
    	int returnAvailableNodes = 0;
    	for(Map.Entry<String,P2PNodeInfo> entry : nodesAvailable.entrySet()){
    		if(entry.getValue().getConnectionQuality() > 0) {
    			returnAvailableNodes++;
    		}
    	}
    	return returnAvailableNodes;
    }
    
    public static String toStringNodes() {
    	String strToDraw = "";
    	
    	ConcurrentHashMap<String, P2PNodeInfo> nodesAvailable = Wallet.getNodesAvailable();
    	strToDraw += String.format("%n## Available Nodes List%n");
        Map<String,P2PNodeInfo> sortedNodesAvailable = nodesAvailable.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
        
        strToDraw += String.format("%n | %-23s | %-8s | %-7s | %-6s | %-16s | %-16s |", "User", "Address", "Status", "Rating", "Registered", "Connected");
        strToDraw += String.format("%n | %-23s | %-8s | %-7s | %-6s | %-16s | %-16s |", "--", "--", "--", "--", "--", "--");
		for(Map.Entry<String,P2PNodeInfo> entry : sortedNodesAvailable.entrySet()){    			
			strToDraw += String.format("%n | %-23s | 0x%-6.6s | %2d / %2d | %6d | %tF %tR | %tF %tR |", entry.getKey(), entry.getValue().getUserKeyHash(), entry.getValue().getConnectionQuality(),P2PNodeInfo.BEST_CONNECTION_QUALITY,entry.getValue().calcConnectionRating(),entry.getValue().getFirstConnection(),entry.getValue().getFirstConnection(),entry.getValue().getLastConnectionSucceed(),entry.getValue().getLastConnectionSucceed());
		}
		
		return strToDraw;      
    }
    
    public void getDifficultyOfBlock(int blockDepth) {
    	this.getBlockChain().getDifficultyOfBlock(this.getBlockChain().getBlockChain(),blockDepth);
    }
    
    public static void writeContractToDisk(String folder,Contract contract){
        ObjectOutputStream oos = null;
        FileOutputStream fout = null;
        try {
			lock.tryLock(2,TimeUnit.SECONDS);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
			System.exit(1);
		}
        try{
            fout = new FileOutputStream(folder+"/"+contract.getHash()+Wallet.FILEENDING_CONTRACT);
            oos = new ObjectOutputStream(fout);
            oos.writeObject(contract);
            LOGGER.fine(String.format("Contract written to disk: %s",contract.getHash()));
        }
        catch (FileNotFoundException ex) {
            LOGGER.fine(String.format("Can not write %.6s to disk: %s",contract.getHash(),ex.toString()));
        }
        catch (IOException ex) {
            LOGGER.fine(String.format("IO error: "+ex));
        }finally {
        	if(lock.isHeldByCurrentThread()) {
        		lock.unlock();
        	}
        	
            try {
				oos.close();
			} catch (Exception e) {
				LOGGER.fine(String.format("Could not close connection: "+e));
			}
        }
    } 
    
    public boolean readContractsFromDisk(String path){
    	
    	boolean haveNew = false;
    	
    	LinkedHashMap<String,Contract> localContracts = (LinkedHashMap<String, Contract>) Wallet.localContracts.clone();
    	
    	boolean onlyMy = false;
        
        ObjectInputStream objectinputstream = null;        
        try {
			lock.tryLock(2,TimeUnit.SECONDS);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
			System.exit(1);
		}
        final File folder = new File(path);
        for (final File fileEntry : folder.listFiles()) {
        	
        	String filename = fileEntry.getName();        	
        	
        	if(fileEntry.getName().endsWith(Wallet.FILEENDING_CONTRACT)) {
        		String contractHash = filename.substring(0, filename.length()-Wallet.FILEENDING_CONTRACT.length());            	
        	            
	        	if(fileEntry.isFile() && !localContracts.containsKey(contractHash)){
	        		
	        		LOGGER.fine(String.format("Reading Contract: %s", contractHash));
	                                
	                try {
	                    
	                    FileInputStream streamIn = new FileInputStream(path+"/"+filename);
	                    objectinputstream = new ObjectInputStream(streamIn);
	                    Contract readContract = (Contract) objectinputstream.readObject();
	                    //System.out.println("Reading Contract of user "+readContract.getUser()+": "+readContract.hash);
	                    boolean isMine = false;
	                    if(readContract.getSenderPubKey() != null) {
	                    	isMine = Hash_it.getHash(readContract.getSenderPubKey()).equals(this.getUserKeyHash());
	                    }
	                    boolean isSignedByMe = false;
	                    for(int i = 0; i < readContract.getNumberApproverPubKeys(); i++) {
	                    	if(Hash_it.getHash(readContract.getApproverPubKey(i)).equals(this.getUserKeyHash())) {
	                    		isSignedByMe = true;
	                    	}
	                    }                    
	                    if(!onlyMy || isMine){
	                        localContracts.put(readContract.getHash(),readContract);
	                        haveNew = true;
	                        // Keep care that my contracts are available on P2P _and_ LOCAL                        
	                    }
	                    // If my contract, write it to disk
	                    if(isMine || isSignedByMe){
	                    	writeContractToDisk(Wallet.P2P_FOLDER, readContract);
	                        writeContractToDisk(Wallet.LOCAL_STORAGE, readContract);
	                    }
	                    //System.out.println("Reading ready");
	                } catch (EOFException e) {
	                	LOGGER.fine(String.format("Contract was removed while reading it."));
	                } catch (FileNotFoundException e) {
	                	LOGGER.fine(String.format("Contract was removed before reading it."));
					} catch (IOException e) {
						LOGGER.fine(String.format("Input Output problem while reading Contract."));
					} catch (ClassNotFoundException e) {
						e.printStackTrace();System.exit(1);
					}
	                finally {                	        	
	                    try {
							objectinputstream.close();
						} catch (Exception e) {
							LOGGER.fine(String.format("Could not close connection: "+e));
						}
	                }
	            }
        	}            
        }
        if(lock.isHeldByCurrentThread()) {
    		lock.unlock();
    	}        
        localContracts = sortMap(localContracts);
        Wallet.localContracts = localContracts;
        return haveNew;
    }
    
    public static LinkedHashMap<String, Contract> sortMap(LinkedHashMap<String, Contract> map) {
        List<Map.Entry<String, Contract>> sortedMap = new LinkedList<>(map.entrySet());

        Collections.sort(sortedMap, (o1, o2) -> o1.getValue().compareTo(o2.getValue())); 

        LinkedHashMap<String, Contract> result = new LinkedHashMap<>();
        for (Map.Entry<String, Contract> entry : sortedMap)
        {
            result.put(entry.getKey(), entry.getValue());
        }

        return result;
    }
    
    public boolean newGuessBlockSize(int estimatedMinutes, int estimatedBlocks){
    	boolean allOk = true;
    	ArrayList<TransactionInput> gbInputs = this.blockChain.gatherUTXos(INSET_GUESS_BLOCKCHAIN_SIZE);
    	if(gbInputs == null) {
    		LOGGER.config(String.format("To less UTXOs. Process aborted."));
    		allOk = false;
    	}
    	
    	if(allOk){
    		GuessBlockSize guessBlockSize = new GuessBlockSize(this.walletKey.getPublicKey(), null, 0, 0.0F, null, this.getBlockChain(), estimatedMinutes*60, estimatedBlocks);  
            
    		guessBlockSize.setHash(guessBlockSize.calculateHash());
        	guessBlockSize.generateSignature(walletKey);
            LOGGER.config(String.format("Checking GuessBlockSize Signature"));
    		if(guessBlockSize.verifiySignature()){
    			LOGGER.config(String.format("GuessBlockSize Signature ok"));
    		}
    		else{
    			LOGGER.config(String.format("GuessBlockSize Signature failed!"));
    			allOk = false;
    		}
            
            
    		if(allOk){
    			LOGGER.config(String.format("GuessBlockSize processed, adding Contract %.6s to BlockChain",guessBlockSize.getHash()));
    			blockChain.newTransaction(guessBlockSize);
    		}    		
    		
    		Transaction contract = null;
    		contract = new Transaction(this.walletKey.getPublicKey(), guessBlockSize.getHash(), INSET_GUESS_BLOCKCHAIN_SIZE, 0.F, gbInputs);  
            if(!contract.processTransaction(0.F)){
            	allOk = false;
            	LOGGER.warning(String.format("Processing Transaction failed"));
            }
            else{
            	contract.generateSignature(blockChain.getWallet().getWalletKey());
                LOGGER.config(String.format("Checking Signature"));
        		if(contract.verifiySignature()){
        			LOGGER.config(String.format("Signature ok"));
        		}
        		else{
        			LOGGER.warning(String.format("Signature failed!"));
        			allOk = false;
        		}
            }
            
    		if(allOk){
    			LOGGER.config(String.format("Transaction processed, adding Transaction %.6s to BlockChain",contract.getHash()));
    			blockChain.newTransaction(contract);    			
    		}    		
    		
    	}
    	else{
    		LOGGER.config(String.format("GuessBlockSize not gerenated. Process aborted."));
    		allOk = false;
    	}   
    	
    	return allOk;
    }
    
    public boolean newTransactionMultiSig(String to, float value, float fee, int signersN){
    	boolean allOk = true;
    	PublicKey toKey = AsymKrypt.readPublicKey(to);
    	ArrayList<TransactionInput> inputs = this.blockChain.gatherUTXos(value+fee);
    	
    	if(inputs == null){
    		LOGGER.config(String.format("To less UTXOs. Process aborted."));
    		allOk = false;
    	}
    	
    	if(toKey != null && allOk){
    		Transaction transaction = new TransactionMultiSig(walletKey.getPublicKey(), toKey, signersN, value, fee, inputs);
    		transaction.setApproverPubKey(0, toKey);
    		transaction.setApproverHash(transaction.calculateApproverHash(0), 0);
            if(!transaction.processTransaction(fee)) {
            	allOk = false;
            	LOGGER.config(String.format("Processing Transaction failed"));
            }
            else{
            	transaction.generateSignature(walletKey);
                LOGGER.config(String.format("Checking Signature"));
        		if(transaction.verifiySignature()){
        			LOGGER.config(String.format("Signature ok"));
        		}
        		else{
        			LOGGER.config(String.format("Signature failed!"));
        			allOk = false;
        		}
            }
            
    		if(allOk){
    			LOGGER.config(String.format("Transaction processed, adding Transaction %.6s to BlockChain",transaction.getHash()));
    			blockChain.newTransaction(transaction);    			
    		}    		
    	}
    	else{
    		LOGGER.config(String.format("Transaction not gerenated. Process aborted."));
    		allOk = false;
    	}    
    	return allOk;
    }
    
    public Integer signFindFreePubKeyPosition(Contract contract) {
    	Integer signPosition = null;
    	
    	for(int i = 0; i < contract.getNumberApproverPubKeys(); i++) {
    		String hashOnPos = Hash_it.getHash(contract.getApproverPubKey(i));
    		LOGGER.config(String.format("Comparing PubKey %.6s with my PubKey %.6s", hashOnPos, this.getUserKeyHash()));
    		if(hashOnPos.equals(this.getUserKeyHash())){
    			signPosition = i;
    			LOGGER.config(String.format("Found my PubKey %.6s on position %d", this.getUserKeyHash(),i));
    			break;
    		}
    	}
    	
    	return signPosition;
    }
    
    public Integer signFindFreePosition(Contract contract) {
    	Integer signPosition = null;
    	
    	if(signPosition == null) {
    		for(int i = 0; i < contract.getNumberApproversNeeded(); i++) {
        		if(contract.getApproverPubKey(i) == null){
        			LOGGER.config(String.format("Found free position on position %d", i));
        			signPosition = i;
        			break;
        		}
        	}
    	}
    	
    	return signPosition;
    }
    
    public boolean acceptGuessBlockSizeBet(String hashPrefix) {
    	
    	Contract signedContract = signContract(hashPrefix);
    	GuessBlockSize gbs = null;
    	boolean allOk = true;
    	if(signedContract instanceof GuessBlockSize) {
    		gbs = (GuessBlockSize) signedContract;
    		
    		float valueToHold = gbs.getSCBalance(blockChain);
    	
			ArrayList<TransactionInput> inputs = blockChain.gatherUTXos(valueToHold);
	    	if(inputs == null){
	    		LOGGER.warning(String.format("To less UTXOs. Process aborted."));
	    		allOk = false;
	    	}
	    	
	    	if(allOk){
	    		
	    		Transaction contract = null;
	    		contract = new Transaction(blockChain.getWallet().getWalletKey().getPublicKey(), gbs.getHash(), valueToHold, 0.F, inputs);  
	            if(!contract.processTransaction(0.F)){
	            	allOk = false;
	            	LOGGER.warning(String.format("Processing Transaction failed"));
	            }
	            else{
	            	contract.generateSignature(blockChain.getWallet().getWalletKey());
	                LOGGER.config(String.format("Checking Signature"));
	        		if(contract.verifiySignature()){
	        			LOGGER.config(String.format("Signature ok"));
	        		}
	        		else{
	        			LOGGER.warning(String.format("Signature failed!"));
	        			allOk = false;
	        		}
	            }
	            
	    		if(allOk){
	    			LOGGER.config(String.format("Transaction processed, adding Transaction %.6s to BlockChain",contract.getHash()));
	    			blockChain.newTransaction(contract);
	    		}    		
	    	}
	    	else{
	    		LOGGER.warning(String.format("Transaction not gerenated. Process aborted."));
	    		allOk = false;
	    	}    
    	}
    	
    	
    	return allOk;
    }
    
    public void isChainValid() {
    	if(this.blockChain.isChainValid(this.blockChain.getBlockChain(),true,true)) {
    		LOGGER.fine(String.format("Blockchain is valid"));
    	}
    }
    
    public Contract signContract(String hashPrefix) {
    	this.getBlockChain().updateBlockChain();
        LinkedHashMap<String,Contract> contractList = this.getBlockChain().getOpenContracts();
        if(Wallet.localContracts != null)
        {
        	contractList.putAll(Wallet.localContracts);
        }
        int countContracts = 0;
        Contract contract = null;
        for(Map.Entry<String,Contract> entry : contractList.entrySet()){
        	if(entry.getKey().startsWith(hashPrefix)){        		
        		countContracts++;
        		contract = entry.getValue();
        	}
        }
        
        if(countContracts == 1) {
        	LOGGER.config(String.format("Found Contract 0x%.6s to sign", contract.getHash()));
        	
        	boolean isPubKeyExisting = true;
        	Integer signPosition = signFindFreePubKeyPosition(contract);
        	if(signPosition == null) {
        		signPosition = signFindFreePosition(contract);
        		isPubKeyExisting = false;
        	}
        	
        	if(signPosition == null) {
        		LOGGER.warning(String.format("No position to sign Contract 0x%.6s. Aborting.", hashPrefix));
        	}
        	else if(signPosition >= 0) {
        		int approverIndex = signPosition;        		
        		if(!isPubKeyExisting) {
        			contract.setApproverPubKey(approverIndex, this.walletKey.getPublicKey());
        			contract.setApproverHash(contract.calculateApproverHash(approverIndex),approverIndex);
        			LOGGER.config(String.format("Pubkey not avaiable, setting on Index %d and generating Hash 0x%.6s.", approverIndex, contract.getApproverHash(approverIndex)));
        		}
        		contract.generateSignature(this.walletKey, signPosition);
        		this.writeContractToDisk(Wallet.P2P_FOLDER, contract);
        		this.writeContractToDisk(Wallet.LOCAL_STORAGE, contract);
        		blockChain.newContract(contract);
        		LOGGER.config(String.format("Signing Contract 0x%.6s on Position %d.", hashPrefix, signPosition));
        	}        	
        }
        else if(countContracts > 1){
        	LOGGER.warning(String.format("Found more than one contract with Hash 0x%.6s. Aborting",hashPrefix));
        	contract = null;
        }
        else {
        	LOGGER.warning(String.format("Found no contract with Hash 0x%.6s. Aborting",hashPrefix));
        	contract = null;
        }
    	return contract;
    }
    
    public boolean newTransaction(String to, float value, float fee){
    	
    	boolean allOk = true;
    	PublicKey toKey = AsymKrypt.readPublicKey(to);
    	ArrayList<TransactionInput> inputs = this.blockChain.gatherUTXos(value+fee);
    	
    	
    	if(inputs == null){
    		LOGGER.config(String.format("To less UTXOs. Process aborted."));
    		allOk = false;
    	}
    	
    	if(toKey != null && allOk){
    		Transaction transaction = new Transaction(walletKey.getPublicKey(), toKey, value, fee, inputs);
            if(!transaction.processTransaction(fee)){
            	allOk = false;
            	LOGGER.config(String.format("Processing Transaction failed"));
            }
            else{
            	transaction.generateSignature(walletKey);
                LOGGER.config(String.format("Checking Signature"));
        		if(transaction.verifiySignature()){
        			LOGGER.config(String.format("Signature ok"));
        		}
        		else{
        			LOGGER.config(String.format("Signature failed!"));
        			allOk = false;
        		}
            }
            
    		if(allOk){
    			LOGGER.config(String.format("Transaction processed, adding Transaction %.6s to BlockChain",transaction.getHash()));
    			blockChain.newTransaction(transaction);    			
    		}    		
    	}
    	else{
    		LOGGER.config(String.format("Transaction not gerenated. Process aborted."));
    		allOk = false;
    	}    
    	return allOk;
    }
   
    public String getUserName(){
        return this.userName;
    }
    
    public String getUserKeyHash(){
    	return Hash_it.getHash(walletKey.getPublicKeyEncoded());
    }
    
    public BlockChain getBlockChain(){
    	return this.blockChain;
    }

	public void updateBlocksAndContracts() {
		boolean haveNewContracts = false;
		if(this.readContractsFromDisk(LOCAL_STORAGE)) {
			haveNewContracts = true;
		}
		if(this.readContractsFromDisk(P2P_FOLDER)) {
			haveNewContracts = true;
		}
		if(haveNewContracts) {
			P2PSingleton.instantiateClient(this, P2PSingleton.P2P.SHARE_CONTRACTS);
		}		
		if(this.blockChain.updateBlockChain()) {
			P2PSingleton.instantiateClient(this, P2PSingleton.P2P.SHARE_CHAIN);
		}
	}

	public boolean mineNewBlock() {
		boolean returnBool = this.blockChain.mineNewBlock();    	
		return returnBool;
	}
	
	public void startP2P() {
		
		P2PSingleton.instantiate(this);		
		
	}
	
	public void mineNewBlockThread() {
		
		Runnable runner = () -> { 
			LOGGER.config(String.format("Start mining"));
			BlockChain blockChainToRun = new BlockChain(this,lock);
			blockChainToRun.updateBlockChain(); 
			blockChainToRun.mineNewBlock();
		};
		new Thread(runner).start();
	}

	public TreeMap<Long, UTXO> UTXOsSorted() {
		TreeMap<Long, UTXO> utxoTree = new TreeMap<>();
        for(Map.Entry<String,UTXO> entry : BlockChain.UTXOs.entrySet()){    
        	Long timeStamp = entry.getValue().timeStamp;
        	while(utxoTree.get(timeStamp) != null) {
        		timeStamp += 1;
        	}        	
    		utxoTree.put(timeStamp,entry.getValue());
    	}
		return utxoTree;
	}
	
	public float getAccountBalance(boolean withTmpUTXOs) {
		float accountSum = 0.F;
		for(Map.Entry<String, UTXO> entry : BlockChain.UTXOs.entrySet()){    
    		String recipientHash = Hash_it.getHash(entry.getValue().reciepient.getEncoded());    		
    		if(recipientHash.startsWith(this.getUserKeyHash())){
    			accountSum += entry.getValue().value;
    		}    		
    	}
		if(withTmpUTXOs) {
			for(Map.Entry<String, UTXO> entry : BlockChain.UTXOsTmp.entrySet()){    
	    		String recipientHash = Hash_it.getHash(entry.getValue().reciepient.getEncoded());    		
	    		if(recipientHash.startsWith(this.getUserKeyHash())){
	    			accountSum += entry.getValue().value;
	    		}    		
	    	}
		}
		return accountSum;
	}
	
	public ArrayList<Transaction> getUserContracts(){
		ArrayList<Transaction> returnContractArray = new ArrayList<Transaction>();
		this.getBlockChain().updateBlockChain();
        LinkedHashMap<String,Contract> contractList = this.getBlockChain().getContractsFromChain();
        for(Map.Entry<String,Contract> entry : contractList.entrySet()){
        	if(entry.getValue() instanceof Transaction) {
        		Transaction transaction = (Transaction) entry.getValue();
        		boolean isAdded = false;
        		if(transaction.getSenderPubKey() != null) {
            		if(Hash_it.getHash(transaction.getSenderPubKey()).equals(this.getUserKeyHash())) {
            			returnContractArray.add(transaction);
            			isAdded = true;
            			LOGGER.fine(String.format("Adding send %s",transaction.getHash()));
            		}
            	}   
        		if(!isAdded && transaction.getRecipientPubKey() != null) {
            		if(Hash_it.getHash(transaction.getRecipientPubKey()).equals(this.getUserKeyHash())) {
            			returnContractArray.add(transaction);
            			LOGGER.fine(String.format("Adding rec %s",transaction.getHash()));
            		}
            	}
        	}        	     	
        }
        return returnContractArray;
	}
	
	public Contract getContractById(String transactionIdPrefix) {
		Contract returnContract = null;
		this.getBlockChain().updateBlockChain();
		LinkedHashMap<String,Contract> contractList = this.getBlockChain().getContractsFromChain();
		if(Wallet.localContracts != null)
        {
        	contractList.putAll(Wallet.localContracts);
        }
        for(Map.Entry<String,Contract> entry : contractList.entrySet()){
        	if(entry.getKey().startsWith(transactionIdPrefix)){  
        		returnContract = entry.getValue();
        	}
        }
		return returnContract;
	}
	
	public String toStringContract(String transactionIdPrefix) {
    	String returnContractStr = "";    	
    	Contract contract = getContractById(transactionIdPrefix);
    	    	
		boolean valid = contract.checkContract(this.getBlockChain());
		returnContractStr += String.format(contract.toString());
		returnContractStr += String.format("%n## Checks%n");
		returnContractStr += String.format("%n* Valid: %s",valid);
		returnContractStr += String.format("%n* Locked: %s",contract.locked(this.getBlockChain()));
		returnContractStr += String.format("%n");
		if(contract instanceof SmartContract) {
			SmartContract sc = (SmartContract) contract;
			returnContractStr += String.format("%n## Smart Contract Info%n");
			returnContractStr += String.format("%n* Balance: %s",sc.getSCBalance(this.getBlockChain()));
		}
		returnContractStr += String.format("%n");    	
        
    	return returnContractStr;
    }
	
	public String toStringBlock(String blockMerklePrefix) {
    	String returnBlockStr = "";
    	ArrayList<Block> blockChain = this.getBlockChain().getBlockChain();
        for(Block block : blockChain){
        	if(block.getMerkleRoot().startsWith(blockMerklePrefix)){
        		returnBlockStr += String.format("%nChecking validity: %s",block.checkContracts(this.getBlockChain()));
        		returnBlockStr += String.format("%n%s%n",block.toString());
        	}
        }
    	return returnBlockStr;
    }
	
	public AsymKrypt getWalletKey() {
		return walletKey;
	}

	public static LinkedHashMap<String, Contract> getLocalContracts() {
		return localContracts;
	}
	
	
	
}
