import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import greenfoot.*; // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class WalletUI extends Actor {

	public static final int UPDATE_BLOCKCHAIN_INTERVALL = 300;

	private Wallet wallet;

	public Wallet getWallet() {
		return wallet;
	}

	private static Logger LOGGER = Logger.getLogger(WalletUI.class.getName());
	private int time;

	public WalletUI(String username) {
		System.setProperty("java.util.logging.SimpleFormatter.format", "%1$tF %1$tT %4$s %2$s %5$s%6$s%n");

		Logger rootLogger = LogManager.getLogManager().getLogger("");
		for (Handler h : rootLogger.getHandlers()) {
			h.setLevel(Level.ALL);
		}
		this.wallet = new Wallet(username);

		for (Map.Entry<String, Level> entry : loggingLevels().entrySet()) {
			// get Logger to be sure that it is created (Threads could be back in time)
			Logger.getLogger(entry.getKey());
			Logger logger = LogManager.getLogManager().getLogger(entry.getKey());
			if (logger != null) {
				logger.setLevel(entry.getValue());
			} else {
				LOGGER.warning(String.format("Could not set logger %s : %s", entry.getKey(), entry.getValue()));
			}
		}

		time = 0;
		drawWalletImage();
	}

	private static Map<String, Level> loggingLevels() {
		Map<String, Level> returnMap = new HashMap<String, Level>();
		returnMap.put("", Level.CONFIG);
		returnMap.put(Logger.GLOBAL_LOGGER_NAME, Level.CONFIG);
		returnMap.put(WalletUI.class.getName(), Level.CONFIG);
		returnMap.put(Wallet.class.getName(), Level.CONFIG);
		returnMap.put(BlockChain.class.getName(), Level.CONFIG);
		returnMap.put(Block.class.getName(), Level.CONFIG);
		returnMap.put(Contract.class.getName(), Level.CONFIG);
		returnMap.put(Transaction.class.getName(), Level.CONFIG);
		returnMap.put(P2PServer.class.getName(), Level.FINE);
		returnMap.put(P2PClient.class.getName(), Level.FINE);
		returnMap.put(P2PCommunication.class.getName(), Level.FINE);
		returnMap.put(P2PSingleton.class.getName(), Level.FINE);
		returnMap.put(P2PSender.class.getName(), Level.FINE);
		return returnMap;
	}

	public void act() {
		time = (++time) % UPDATE_BLOCKCHAIN_INTERVALL;
		if (time == 0) {
			redrawBlocksAndContracts();
		}
		if (Greenfoot.mouseClicked(this)) {
			WorldUTXOsMonitor utxoWorld = new WorldUTXOsMonitor(getWorld(), this);
			Greenfoot.setWorld(utxoWorld);
		}
	}

	public void mineNewBlock() {
		System.out.printf("%n########## Start Mining #########%n");
		System.out.printf("##################################%n");
		System.out.printf("%nmining ...%n");
		wallet.mineNewBlockThread();
	}

	public void acceptGuessBlockSizeBet(String hashPrefix) {
		wallet.acceptGuessBlockSizeBet(hashPrefix);
	}

	public void isChainValid() {
		wallet.isChainValid();
	}

	public void getDifficultyOfBlock(int blockDepth) {
		this.wallet.getDifficultyOfBlock(blockDepth);
	}

	public void newGuessBlockSize(int estimatedMinutes, int estimatedBlocks) {
		wallet.newGuessBlockSize(estimatedMinutes, estimatedBlocks);
		redrawBlocksAndContracts();
	}

	public void newTransactionMultiSign(String to, float value) {
		System.out.printf("%n## New Transaction Multi Sign ##%n");
		System.out.printf("##################################%n");
		System.out.printf("%n%-15s: %s", "To", to);
		System.out.printf("%n%-15s: %s", "Value", value);
		if (wallet.newTransactionMultiSig(to, value, 0.F, 1)) {
			System.out.printf("%n%nTransaction successfully created. Stand by until it is included in a block.");
		} else {
			System.out.printf("%nTransaction not created! See logs for details.");
		}
		redrawBlocksAndContracts();
	}

	public void newTransaction(String to, float value) {
		System.out.printf("%n######### New Transaction #######%n");
		System.out.printf("##################################%n");
		System.out.printf("%n%-15s: %s", "To", to);
		System.out.printf("%n%-15s: %s", "Value", value);
		if (wallet.newTransaction(to, value, 0.F)) {
			System.out.printf("%n%nTransaction successfully created. Stand by until it is included in a block.");
		} else {
			System.out.printf("%nTransaction not created! See logs for details.");
		}
		redrawBlocksAndContracts();
	}

	private void drawWalletImage() {
		GreenfootImage thisImage = new GreenfootImage("purse.png");
		thisImage.setColor(new Color(0, 0, 0));
		thisImage.setFont(new Font("Monospaced", false, false, 11));
		thisImage.drawString(String.format("User: %." + Wallet.USER_HASH_LENGTH + "s", wallet.getUserName()), 10, 67);
		thisImage.drawString(String.format("PubK: %." + Wallet.USER_HASH_LENGTH + "s", wallet.getUserKeyHash()), 10,
				77);
		thisImage.drawString(String.format("%.2f ", this.wallet.getAccountBalance(true)), 18, 92);
		thisImage.drawString(String.format("(%.2f) ", this.wallet.getAccountBalance(false)), 11, 102);
		this.setImage(thisImage);
	}

	public void redrawBlocksAndContracts() {
		wallet.updateBlocksAndContracts();
		redrawAllBlocks();
		redrawAllContracts();
		drawWalletImage();
	}

	public void showPubKeyHash() {
		System.out.println(String.format("Hash of your Publik Key:"));
		System.out.println(String.format("%s", wallet.getUserKeyHash()));
	}

	private void redrawAllBlocks() {
		LOGGER.fine(String.format("Redraw Blocks"));
		World world = getWorld();
		List<ActorBlock> allBlocks = world.getObjects(ActorBlock.class);
		// System.out.println("Removing all Blocks");
		for (ActorBlock block : allBlocks) {
			world.removeObject(block);
		}
		// System.out.println("Writing all Blocks to World");
		int blocksN = wallet.getBlockChain().getBlockChain().size();
		for (int i = 0; i < blocksN; i++) {
			// System.out.println("Writing Block: "+(i+1));
			Block block = wallet.getBlockChain().getBlockChain().get(i);
			drawBlockToWorld(new ActorBlock(block, this), i, blocksN);
		}
	}

	private boolean drawBlockToWorld(ActorBlock newUiBlock, final int blockNr, final int blocksN) {
		newUiBlock.drawBlockImage();

		final int BASE_N = 5;
		final int STACK_N = 15;
		final int MAX_ROWS = 2;

		int perviousHeight = 0;
		int perviousWidth = 0;

		int col = 0;
		int row = 0;
		int showFrom = 0;

		if (blocksN > STACK_N + MAX_ROWS * BASE_N) {
			showFrom = blocksN - (STACK_N + MAX_ROWS * BASE_N);
		}

		int thisBlockNr = blockNr - showFrom;
		int thisBlocksN = blocksN - showFrom;

		if (thisBlocksN > STACK_N && thisBlocksN / STACK_N > thisBlockNr / STACK_N) {
			col = thisBlockNr / STACK_N;
			row = thisBlockNr - col * STACK_N;
		} else if (thisBlocksN > STACK_N) {
			int blockNrBig = thisBlockNr - ((thisBlockNr / STACK_N) * STACK_N);
			int colBig = blockNrBig / BASE_N;
			col = colBig + thisBlockNr / STACK_N;
			row = blockNrBig - colBig * BASE_N;
		} else {
			final int colBase = thisBlockNr / BASE_N;
			final int rowBase = thisBlockNr - colBase * BASE_N;
			col = colBase;
			row = rowBase;
		}

		perviousHeight = newUiBlock.getImage().getHeight();
		perviousWidth = newUiBlock.getImage().getWidth();

		if (thisBlocksN > STACK_N && thisBlocksN / STACK_N > thisBlockNr / STACK_N) {
			perviousHeight = perviousHeight / 3;
		}

		World world = getWorld();
		int x = this.getX() + this.getImage().getWidth() + perviousWidth * col;
		int y = this.getY() + ((perviousHeight) * (row));

		if (thisBlockNr > 0 || STACK_N > thisBlocksN) {
			world.addObject(newUiBlock, x, y);
			return true;
		} else if (thisBlockNr == 0) {
			GreenfootImage thisImage = new GreenfootImage("block_60.png");
			int n = showFrom;
			thisImage.drawString(String.format("#%d hidden", n), 3, 15);
			newUiBlock.setImage(thisImage);
			world.addObject(newUiBlock, x, y);
			return true;
		} else {
			return false;
		}
	}

	private void redrawAllContracts() {
		LOGGER.fine(String.format("Redraw Contracts"));
		World world = getWorld();
		List<Object> objectsInWorld = world.getObjects(null);
		for (Object object : objectsInWorld) {
			if (object instanceof ActorContract) {
				Actor actor = (Actor) object;
				world.removeObject(actor);
			}
		}
		int counter = 0;
		String hashLastContract = null;
		Set<Entry<String, Contract>> contractList = wallet.getBlockChain().getOpenContracts().entrySet();
		for (Map.Entry<String, Contract> me : contractList) {
			Contract contract = me.getValue();
			ActorContract uiContract = null;

			if (contract instanceof GuessBlockSize) {
				uiContract = new ActorGuessBlockSize((GuessBlockSize) contract, this);
			} else if (contract instanceof TransactionMultiSig) {
				uiContract = new ActorTransactionMultiSig((TransactionMultiSig) contract, this);
			} else if (contract instanceof Transaction) {
				uiContract = new ActorTransaction((Transaction) contract, this);
			} else {
				uiContract = new ActorContract(contract, this);
			}
			boolean isInBlockChain = (wallet.getBlockChain().getChainContractList()
					.get(uiContract.getContract().getHash()) != null) ? true : false;
			uiContract.drawContractImage(isInBlockChain, false);
			drawContractToWorld(uiContract, hashLastContract, counter, contractList.size());
			hashLastContract = me.getKey();
			counter++;
		}
	}

	private boolean drawContractToWorld(ActorContract uiContract, String hashLastContract, final int blockNr,
			final int contractsN) {

		final int BASE_N = 4;
		final int STACK_N = 12;
		final int MAX_ROWS = 2;

		int perviousHeight = 0;
		int perviousWidth = 0;
		int col = 0;
		int row = 0;
		int showFrom = 0;

		if (contractsN > STACK_N + MAX_ROWS * BASE_N) {
			showFrom = contractsN - (STACK_N + MAX_ROWS * BASE_N);
		}

		int thisBlockNr = blockNr - showFrom;
		int thisContractsN = contractsN - showFrom;

		if (thisContractsN > STACK_N && thisContractsN / STACK_N > thisBlockNr / STACK_N) {

			col = thisBlockNr / STACK_N;
			row = thisBlockNr - col * STACK_N;
		} else if (thisContractsN > STACK_N) {
			int blockNrBig = thisBlockNr - ((thisBlockNr / STACK_N) * STACK_N);
			int colBig = blockNrBig / BASE_N;
			col = colBig + thisBlockNr / STACK_N;
			row = blockNrBig - colBig * BASE_N;
		} else {
			int colBase = blockNr / BASE_N;
			int rowBase = blockNr - colBase * BASE_N;
			col = colBase;
			row = rowBase;
		}

		perviousHeight = uiContract.getImage().getHeight();
		perviousWidth = uiContract.getImage().getWidth();

		if (thisContractsN > STACK_N && thisContractsN / STACK_N > thisBlockNr / STACK_N) {
			perviousHeight = perviousHeight / 3;
		}

		World world = getWorld();
		int x = this.getX() - perviousWidth - 20 - (perviousWidth + 3) * col;
		int y = this.getY() + ((perviousHeight) * (row));

		if (thisBlockNr > 0 || STACK_N > thisContractsN) {
			world.addObject(uiContract, x, y);
			return true;
		} else if (thisBlockNr == 0) {
			GreenfootImage thisImage = new GreenfootImage("contract.png");
			int n = showFrom;
			thisImage.drawString(String.format("#%d hidden", n), 3, 15);
			uiContract.setImage(thisImage);
			world.addObject(uiContract, x, y);
			return true;
		} else {
			return false;
		}

	}

	public void printBlock(String merkleRootPrefix) {
		System.out.printf("%n#### Print Blocks 0x%.6s ####%n", merkleRootPrefix);
		System.out.printf("##################################%n");
		System.out.println(wallet.toStringBlock(merkleRootPrefix));
	}

	public void printContract(String transactionIdPrefix) {
		System.out.printf("%n######## Print Contracts ########%n");
		System.out.printf("##################################%n");
		System.out.printf("%n%-15s: %s%n", "Contracts with prefix", transactionIdPrefix);
		System.out.println(wallet.toStringContract(transactionIdPrefix));
	}

	public void printUserInfo() {
		System.out.printf("%n############ User Info ###########%n");
		System.out.printf("##################################%n");
		System.out.printf("%n%-15s: %s", "Username Hash", wallet.getUserName());
		System.out.printf("%n%-15s: %s", "Public Key Hash", wallet.getUserKeyHash());
	}

	public void printBlockChain() {
		System.out.printf("%s%n", wallet.getBlockChain().toString());
	}

	public void printCoinsFromGuessBlockSize() {

	}

	public void signContract(String hashPrefix) {
		wallet.signContract(hashPrefix);
	}

	public void printUTXOs(String recipientKeyHashPrefix) {
		System.out.printf("%n### UTXO List of User 0x%.6s ###%n", recipientKeyHashPrefix);
		System.out.printf("##################################%n");
		float recipientValueSum = 0;
		String selectedRecipient = null;
		boolean notUnique = false;

		for (Map.Entry<Long, UTXO> entry : wallet.UTXOsSorted().entrySet()) {
			String recipient = Hash_it.getHash(entry.getValue().reciepient.getEncoded());
			String thisUser = wallet.getUserKeyHash();
			if (recipient.startsWith(recipientKeyHashPrefix)) {
				recipientValueSum += entry.getValue().value;
				if (selectedRecipient == null) {
					selectedRecipient = recipient;
				}
				if (selectedRecipient != null && !selectedRecipient.equals(recipient)) {
					notUnique = true;
				}
			} else {
				continue;
			}
			System.out.printf("%n%-15s: %s", "Date",
					new SimpleDateFormat("d.M.y HH:mm:ss").format(new Date(entry.getValue().timeStamp)));
			System.out.printf("%n%-15s: %s", "txid", entry.getValue().txid);
			System.out.printf("%n%-15s: %s", "txoid", entry.getValue().txo.id);
			System.out.printf("%n%-15s: %s", "Recipient", recipient);
			System.out.printf("%n%-15s: %s", "Value", entry.getValue().value);
			System.out.println();
		}
		if (notUnique) {
			System.out.printf(
					"%nPrinting no Credit Sum, because Prefix 0x%.6s is not unique. Type in longer Prefix if you need Credit Sum!%n",
					recipientKeyHashPrefix);
		} else {
			System.out.printf("%n%-15s: %s%n", "Credit Sum", recipientValueSum);
		}
	}

}
