import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class WorldAccountBalance here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class WorldAccountBalance extends WorldMonitor
{

	private WalletUI walletUI;
	
    public WorldAccountBalance(World world, WalletUI walletUI)
    {    
        super(world, walletUI); 
        addObject(new ButtonWallet(world, walletUI),80,40);        
        this.walletUI = walletUI;
        drawUTXOs();
    }
    
    private void drawUTXOs() {
    	String strToDraw = "";
    	int counter = 0;
    	float accountSum = 0;
    	strToDraw = String.format("%n | %-5s | %-20s | %-10s | %-12s | %-12s | %-12s |","Number","DATE","TXID","Sender","Recipient","VALUE");
    	addObject(new ActorAccountBalanceMonitor(strToDraw, getWalletUI()),400,120 + 20*counter);  
    	ArrayList<Transaction> accountContracts = new ArrayList<Transaction>();
    	ArrayList<Transaction> accountContractsFromWallet = walletUI.getWallet().getUserContracts();
    	Collections.reverse(accountContractsFromWallet);
    	accountContracts.addAll(accountContractsFromWallet);
    	for(Transaction transaction : accountContracts){ 
    		
    		String recipientHash = "";
    		if(transaction.getRecipientPubKey() != null) {
    			recipientHash = Hash_it.getHash(transaction.getRecipientPubKey());
    		}
    		else {
    			recipientHash = transaction.getToTransactionHash();
    		}
    		String senderHash = "";
    		if(transaction.getSenderPubKey() != null) {
    			senderHash = Hash_it.getHash(transaction.getSenderPubKey());
    		}	
    		else {
    			senderHash = transaction.getFromTransactionHash();
    		}
    		boolean isCredit = false;
    		if(recipientHash.equals(walletUI.getWallet().getUserKeyHash())) {
    			isCredit = true;
    		}    		
    		
    		
			String dateString = new SimpleDateFormat("d.M.y HH:mm:ss").format(new Date(transaction.getTimeStamp()));
    		String txidString = String.format("%.6s", transaction.getHash());
    		senderHash = String.format("%.6s", senderHash);
    		recipientHash = String.format("%.6s", recipientHash);
    		String valueFormat = (isCredit)?"%.2f sC":"-%.2f sC";
    		String valueString = String.format(valueFormat, transaction.getValue());
    		if(isCredit) {
    			accountSum += transaction.getValue();
    		}
    		else {
    			accountSum -= transaction.getValue();
    		}
    		strToDraw = String.format("%n | %6d | %-20s | %-10s | %-12s | %-12s | %12s |",accountContracts.size()-counter,dateString,txidString,senderHash,recipientHash,valueString);
    		
    		counter ++;
    		if(counter < 25) {
    			addObject(new ActorAccountBalanceMonitor(strToDraw,transaction,getWalletUI()),400,120 + 20*counter);       		
    		}
    		
    	}
    	String sum = String.format("%nYour Credit: %.2f sCoins",accountSum);
    	addObject(new ActorAccountBalanceMonitor(sum, getWalletUI()),400,90);  
    }
}
