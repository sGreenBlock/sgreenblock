import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class WorldBlockMonitor here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class WorldBlockMonitor extends WorldMonitor
{

	private WalletUI walletUI;
	String blockMerklePrefix;
	
    public WorldBlockMonitor(World world, WalletUI walletUI, String blockMerklePrefix)
    {    
        super(world, walletUI); 
        addObject(new ButtonWallet(world, walletUI),80,40);        
        this.walletUI = walletUI;
        this.blockMerklePrefix = blockMerklePrefix;
        String strToDraw = drawUTXOs();
        drawMonitor(strToDraw);
    }
    
    private String drawUTXOs() {
    	String strToDraw = "";
    	
    	strToDraw = walletUI.getWallet().toStringBlock(blockMerklePrefix);
    	
    	return strToDraw;       	
    }
}
