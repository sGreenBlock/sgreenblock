import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class WorldBlockMonitor here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class WorldContractMonitor extends WorldMonitor
{

	private WalletUI walletUI;
	Contract contract;
	
    public WorldContractMonitor(World world, WalletUI walletUI, Contract contract)    
    {    
        super(world, walletUI); 
        if(contract.getNumberApproverSigns() < contract.getNumberApproversNeeded()) {
        	if(walletUI.getWallet().signFindFreePosition(contract)!=null || walletUI.getWallet().signFindFreePubKeyPosition(contract)!=null) {
        		addObject(new ButtonSignContract(world, walletUI, contract),280,40);       
        	}        	 
        }
        
        this.walletUI = walletUI;
        this.contract = contract;
        String strToDraw = drawContracts();
        drawMonitor(strToDraw);
    }
    
    private String drawContracts() {
    	String strToDraw = "";
    	
    	strToDraw = walletUI.getWallet().toStringContract(contract.getHash());
    	
    	return strToDraw;       	
    }
}
