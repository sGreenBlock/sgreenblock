import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class WorldMonitor here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class WorldMonitor extends World
{

	private WalletUI walletUI;
	
    public WorldMonitor(World world, WalletUI walletUI)
    {    
        super(800, 600, 1); 
        addObject(new ButtonWallet(world, walletUI),80,40);        
        this.walletUI = walletUI;
    }
    
    public void drawMonitor(String strToDraw){
        addObject(new ActorMonitor(strToDraw),400,320);   
    }

	public WalletUI getWalletUI() {
		return walletUI;
	}
    
    
}
