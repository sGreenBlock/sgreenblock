import java.net.ServerSocket;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class WorldBlockMonitor here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class WorldNodesMonitor extends WorldMonitor
{

	private WalletUI walletUI;
	private int time;
	public static final int TANGLE_TIME = 50;
		
    public WorldNodesMonitor(World world, WalletUI walletUI)
    {    
        super(world, walletUI);                 
        this.walletUI = walletUI;   
        time = 0;
    }
    
    public void act() 
    {    	
    	if(time == 0) {
    		String strToDraw = drawNodes();
            drawMonitor(strToDraw);
    	}
    	time = (++time) % TANGLE_TIME;
    	
    }    
    
    private String drawNodes() {
    	String strToDraw = "";
    	
    	ServerSocket serverSocket = P2PSingleton.getServerSocketInstance(walletUI.getWallet());
    	boolean isServerAvailable = false;
    	if(serverSocket != null && serverSocket.isBound()) {
    		isServerAvailable = true;
    	}
    	
    	strToDraw += String.format("%n## Server Info %n");    	
    	strToDraw += String.format("%nSocket connected on Port %d: %s", Wallet.P2P_MY_PORT, isServerAvailable);
    	strToDraw += String.format("%n");
    	strToDraw += Wallet.toStringNodes();
    	
    	return strToDraw;       	
    }
}
