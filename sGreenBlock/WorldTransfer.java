import java.io.File;
import java.io.ObjectInputStream;
import java.util.LinkedHashMap;

import greenfoot.*; 




public class WorldTransfer extends World
{
	
	public static final String KEY_PATH = "keys";
	
	private WalletUI walletUI;
    
    public void setWalletUI(WalletUI walletUI) {
		this.walletUI = walletUI;
	}



	public WorldTransfer(World world, WalletUI walletUI)
    {    
        super(800, 600, 1); 
        addObject(new ButtonWallet(world, walletUI),80,40);
        this.walletUI = walletUI;
        readPubKeysFromDisk();        
    }
    
    

	private void readPubKeysFromDisk() {
    	final File folder = new File(KEY_PATH);
        int counter = 0;
        for (final File fileEntry : folder.listFiles()) {            
        	String filename = fileEntry.getName();
            if(fileEntry.isFile() && filename.endsWith(".pub")){
            	GreenfootImage thisImage = new GreenfootImage("contact.png");
            	ButtonContact buttonContact = new ButtonContact(this.walletUI,filename.substring(0,filename.length()-".pub".length()));
            	thisImage.setColor(new Color(255,255,255));
            	thisImage.setFont(new Font("Monospaced", false, false , 12));
            	thisImage.drawString(filename.substring(0, (filename.length() > 12) ? 12 : filename.length()),10,18);
            	String pubKeyHash = Hash_it.getHash(AsymKrypt.readPublicKey(filename.substring(0,filename.length()-".pub".length())));
            	thisImage.drawString("0x"+pubKeyHash.substring(0, 6),10,32);
            	buttonContact.setImage(thisImage);
            	addObject(buttonContact,100,40*counter+90);
            	counter++;
            }
        }
    }
}
