import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class WorldAccountBalance here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class WorldUTXOsMonitor extends WorldMonitor
{

	private WalletUI walletUI;
	
    public WorldUTXOsMonitor(World world, WalletUI walletUI)
    {    
        super(world, walletUI); 
        addObject(new ButtonWallet(world, walletUI),80,40);        
        this.walletUI = walletUI;
        drawUTXOs();
    }
    
    private void drawUTXOs() {
    	String strToDraw = "";
    	int counter = 0;
    	float accountSum = 0;
    	strToDraw = String.format("%n| %-30s | %-10s | %-12s | %-12s |","DATE","TXID","TXOID","VALUE");
    	addObject(new ActorAccountBalanceMonitor(strToDraw,getWalletUI()),400,120 + 20*counter);
    	Map<Long, UTXO> utxMap = new TreeMap<>(Collections.reverseOrder());
    	utxMap.putAll(walletUI.getWallet().UTXOsSorted());
    	for(Map.Entry<Long, UTXO> entry : utxMap.entrySet()){    
    		String recipientHash = Hash_it.getHash(entry.getValue().reciepient.getEncoded());    		
    		
    		if(recipientHash.startsWith(walletUI.getWallet().getUserKeyHash())){
    			String dateString = new SimpleDateFormat("d.M.y HH:mm:ss").format(new Date(entry.getValue().timeStamp));
        		String txidString = String.format("%.6s", entry.getValue().txid);
        		String txoidString = String.format("%.6s", entry.getValue().txo.id);
        		String valueString = String.format("%.2f sC", entry.getValue().value);
        		accountSum += entry.getValue().value;
        		strToDraw = String.format("%n| %-30s | %-10s | %-12s | %-12s |",dateString,txidString,txoidString,valueString);
        		
        		counter ++;
        		if(counter < 25) {        			
        				addObject(new ActorAccountBalanceMonitor(strToDraw,entry.getValue().txid,getWalletUI()),400,120 + 20*counter);       		
        		}
    		}  	    		
    		
    	}
    	String sum = String.format("%nYour Credit: %.2f sCoins",accountSum);
    	addObject(new ActorAccountBalanceMonitor(sum,getWalletUI()),400,90);       	
    }
}
