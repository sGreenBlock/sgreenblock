import java.io.File;

import greenfoot.*; 

public class WorldWallet extends World
{
	public static final String KEY_PATH = "keys";
    
    public WorldWallet()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(800, 600, 1); 
        
        MiningProgress.setHashesCounter(0);
        MiningProgress.setMining(false);
        WalletUI wallet = new WalletUI(getPrivKeyHash());
        
        addObject(new ButtonAccountBalance(wallet),120,40);
        addObject(new ButtonTransaction(wallet),330,41);
        addObject(new ButtonGuessBlockChainSize(wallet),555,40);
        addObject(new ActorMonitorNodes(wallet),750,40);
        
        //addObject(new ButtonRefreshBlockChain(wallet),300,150);
        addObject(new ButtonMine(wallet),500,150);
        
        addObject(wallet,400,240);
        wallet.redrawBlocksAndContracts();
        
        Greenfoot.start();
    }
    
    public String getPrivKeyHash() {
    	String returnString = null;
    	final File folder = new File(KEY_PATH);
    	System.out.printf("%n## Searching Private Keyfile%n");
        for (final File fileEntry : folder.listFiles()) {            
        	String filename = fileEntry.getName();
            if(fileEntry.isFile() && filename.endsWith(".priv")){
            	returnString = filename.substring(0,filename.length()-".priv".length());
            	System.out.printf("%nFound Keyfile '%s'",returnString);
            }
        }
        if(returnString == null) {
        	System.out.printf("%nFound no Keyfile, creating new Keyfile '%s'",AsymKrypt.KEY_NAME);
        }
        return returnString;
    }

   
}
